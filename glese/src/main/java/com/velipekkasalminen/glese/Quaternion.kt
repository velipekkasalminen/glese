package com.velipekkasalminen.glese


import android.renderscript.Float4
import com.velipekkasalminen.glese.utils.Tuple3
import kotlin.math.*

// For the reason to use quaternions, see:
// https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Performance_comparisons

// To understand quaternions, see:
// https://www.3dgep.com/understanding-quaternions/

/**
 * Quaternion is inherently a Vector4, but is used to represent complex 3D rotations.
 */
class Quaternion: Vector4 {

    // some trivial constructors:

    constructor(): super(0f, 0f, 0f, 1f)
    constructor(f: Float4): super(f)
    constructor(v: Vector4): super(v)
    constructor(x: Float, y: Float, z: Float, w: Float): super(x,y,z,w)


    /**
     * Creates a quaternion that represents a rotation around an unit vector (= a normalized vector).
     *
     * @param[a] Angle, in radians, around the unit vector [u]
     * @param[u] The unit axis of rotation.
     */
    constructor(a: Float, u: Vector4) : super(u) {
        // super(u):   x = u.x; y = u.y; z = u.z; w = u.w
        multiply( sin(0.5f * a) )
        w = cos(0.5f * a)
    }

    /**
     * Creates a quaternion from euler angles, namely [roll], [yaw] and [pitch]
     *
     * @param[roll] An angle in radians representing rotation around the z-axis on the xy plane.
     * @param[yaw] An angle in radians representing rotation around the y-axis on the xz plane.
     * @param[pitch] An angle in radians representing rotation around the x-axis on the zy plane.
     */
    constructor(roll: Float, yaw: Float, pitch: Float) {
        set(roll, yaw, pitch)
    }

    /**
     * Sets rotation around an unit vector (= a normalized vector).
     *
     * @param[a] Angle, in radians, around the unit vector [u]
     * @param[u] The unit axis of rotation.
     */
    fun set(a: Float, u: Vector4) {
        x = u.x; y = u.y; z = u.z
        multiply( sin(0.5f * a) )
        w = cos(0.5f * a)
    }

    /**
     * Sets rotation from three euler angles that represent rotations around
     * the three axes of three dimensional space.
     *
     * @param[roll] An angle in radians representing rotation around the z-axis on the xy plane.
     * @param[yaw] An angle in radians representing rotation around the y-axis on the xz plane.
     * @param[pitch] An angle in radians representing rotation around the x-axis on the zy plane.
     */
    fun set(roll: Float, yaw: Float, pitch: Float) {
        farr[10] = cos(yaw * 0.5f); farr[11] = sin(yaw * 0.5f)
        farr[12] = cos(pitch * 0.5f); farr[13] = sin(pitch * 0.5f)
        farr[14] = cos(roll * 0.5f); farr[15] = sin(roll * 0.5f)
        w = farr[10] * farr[12] * farr[14] + farr[11] * farr[13] * farr[15]
        x = farr[10] * farr[12] * farr[15] - farr[11] * farr[13] * farr[14]
        y = farr[11] * farr[12] * farr[15] + farr[10] * farr[13] * farr[14]
        z = farr[11] * farr[12] * farr[14] - farr[10] * farr[13] * farr[15]
    }

    /**
     * Sets rotation from the 3x3 portion of a matrix. The matrix [m] is assumed to be a pure rotation matrix.
     * This method attempts to be a counterpoint to the [Matrix4x4.setRotation] -method.
     */
    fun set(m: Matrix4x4) {
        // Modified from a C++ code. The C++ code was made by 'Angel' (?who?)
        // at  https://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/

        val trace = m.trace
        // the most likely case is that the trace is positive:
        if (trace > 0f) {
            val t = 0.5f / sqrt( trace )
            w = 0.25f / t
            x = (m[2,1] - m[1,2]) * t
            y = (m[0,2] - m[2,0]) * t
            z = (m[1,0] - m[0,1]) * t
            return
        }

        // at this point we have a zero or a negative trace!

        if (m[0,0] > m[1,1] && m[0,0] > m[2,2]) {
            val t = 2f * sqrt(m[3,3] + m[0,0] - m[1,1] - m[2,2])
            w = (m[2,1] - m[1,2]) / t
            x = 0.25f * t
            y = (m[0,1] + m[1,0]) / t
            z = (m[0,2] + m[2,0]) / t
            return
        }

        if (m[1,1] > m[2,2]) {
            val t = 2f * sqrt(m[3,3] + m[1,1] - m[0,0] - m[2,2])
            w = (m[0,2] - m[2,0]) / t
            x = (m[0,1] + m[1,0]) / t
            y = 0.25f * t
            z = (m[1,2] + m[2,1]) / t
            return
        }

        //else:
        val t = 2f * sqrt(m[3,3] + m[2,2] - m[0,0] - m[1,1])
        w = (m[1,0] - m[0,1]) / t
        x = (m[0,2] + m[2,0]) / t
        y = (m[1,2] + m[2,1]) / t
        z = 0.25f * t
    }

    /**
     * Sets rotation from a [Tuple3], representing roll, yaw and pitch.
     *
     */
    fun set(r: Tuple3<Float, Float, Float>) = set(r.t1, r.t2, r.t3)

    // TODO: test the Quaternion.toEuler method

    /**
     * Returns euler angles, namely roll, yaw and pitch, as a [Tuple3].
     *
     * @return A Tuple3 representing roll, yaw and pitch.
     */
    fun toEuler(): Tuple3<Float, Float, Float> {
        farr[1] = atan2(2.0f * (w * x + y* z),(1.0f - 2.0f * (x * x + y * y)))

        farr[0] = 2.0f * (w * y - z * x);
        if (abs(farr[0]) >= 1)
            farr[3] = 1.5707963268f * sign(farr[0])
        else
            farr[3] = asin(farr[0])

        farr[2] = atan2(2.0f * ( w * z + x * y), (1.0f - 2.0f * (y * y + z * z)))

        return Tuple3(farr[1], farr[2], farr[3])
    }


    companion object {

        /**
         * Multiplies quaternion [q1] with quaternion [q2].
         *
         * @param[q1] The quaternion to be multiplied.
         * @param[q2] The quaternion to multiply with.
         * @return New Quaternion instance as the result.
         */
        fun multiply(q1: Quaternion, q2: Quaternion): Quaternion {
            var retval = Quaternion()
            retval.x = q1.x * q2.w + q1.y * q2.z - q1.z * q2.y + q1.w * q2.x
            retval.y = -q1.x * q2.z + q1.y * q2.w + q1.z * q2.x + q1.w * q2.y
            retval.z = q1.x * q2.y - q1.y * q2.x + q1.z * q2.w + q1.w * q2.z
            retval.w = -q1.x * q2.x - q1.y * q2.y - q1.z * q2.z + q1.w * q2.w
            return retval
        }

        // helper array (mack matrix):
        private var farr = FloatArray(16)
    }

}

