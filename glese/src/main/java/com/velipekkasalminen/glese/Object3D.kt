package com.velipekkasalminen.glese


/**
 * Object3D represents a renderable and updatable object in 3D space.
 * Matrix transform calculations and rendering are done automatically when
 * the inheriting instances of this class are set to the rendering queue of the parent scene.
 * Note that inheritors should always override the [update] function.
 */
open class Object3D(val name: String): ITransformable {

    // for the sharedModel:
    private var _model: Model3D? = null

    // customizable props that will not mess with the default props:
    private var _mProps: MaterialProperties? = null

    /**
     * Customizable material properties for this 3D object. Initially set to default material
     * properties, received via the shared model. These custom properties will not affect the
     * material properties of the shared model.
     * This value is null when the shared model is null, or when the model does not have
     * a material.
     */
    val materialProperties get() = _mProps

    /**
     * True if the customized material properties are to be used;
     * the default properties are used on false.
     * This value is set to true by default.
     */
    var customMaterialPropertiesEnabled: Boolean = true

    /**
     * The 3D model shared across the 3D objects.
     * Setting the shared model enables the use of customizable material properties.
     * The model can be set to null.
     */
    var sharedModel: Model3D?
        get() = _model
        set(value) {
            if (value != null) {
                _mProps?.clear()
                _mProps = if (value.sharedMaterial != null) {
                    MaterialProperties(value.sharedMaterial!!.defaultProperties)
                } else null
            } else {
                _mProps?.clear()
                _mProps = null
            }
            _model = value
        }

    /**
     * The local transform matrix.
     */
    override var matrix = Matrix4x4()

    /**
     * The inverted and transposed local transform matrix
     * for lighting calculations.
     */
    var normalMatrix = Matrix4x4()

    /**
     * True, if the normal matrix calculations are enabled.
     * If set to false, the calculations are skipped and may
     * result in odd lighting behaviour of the material.
     * If lighting is not required by the material, this property
     * can be safely set to false.
     */
    var normalMatrixEnabled = true

    /**
     * True, if the matrix of the [sharedModel] is used in the matrix calculations.
     * If set to false, the rotation and position offset of the model are ignored.
     */
    var modelMatrixEnabled = true

    /**
     * The local position of this 3d object in cartesian coordinate system.
     */
    override var position = Position()

    /**
     * The scale of this 3d object.
     */
    override var scale = Vector4(1f, 1f, 1f)

    /**
     * The rotation (orientation) of this 3d object, represented by a quaternion.
     */
    override var rotation = Quaternion()

    // temp holder for the model-view-projection matrix:
    private var mvpMatrix: Matrix4x4 = Matrix4x4()

    // temp holder for the model-view matrix:
    private var mvMatrix: Matrix4x4 = Matrix4x4()

    // same as matrix.row3
    private var _forward = Vector4()

    // same as matrix.row2
    private var _up = Vector4()

    // same as matrix.row1
    private var _right = Vector4()

    private var _isStationary: Boolean = false

    /**
     * The 'forward' direction vector of this 3d object,
     * obtained from the local transform matrix.
     */
    override val forward: Vector4 get() = _forward

    /**
     * The 'up' direction vector of this 3d object,
     * obtained from the local transform matrix.
     */
    override val up: Vector4 get() = _up

    /**
     * The 'right' direction vector of this 3d object,
     * obtained from the local transform matrix.
     */
    override val right: Vector4 get() = _right

    /**
     * Set to true to set this 3D object stationary.
     * This object will still be updated, but manipulating position, rotation or scaling will
     * have no effect as the automatic calls to the [calculateTransformations] -method are disabled.
     */
    override var isStationary: Boolean
        get() = _isStationary
        set(value) {
            // do the calculation one last time before going stationary:
            if (value) calculateTransformations()
            _isStationary = value
        }

    /**
     * Enables or disables the automatic calls to the update method.
     * Update calls are enabled by default.
     */
    var updateEnabled: Boolean = true

    /**
     * Enables or disables the automatic calls to the render method.
     * Render calls are enabled by default.
     */
    var renderEnabled: Boolean = true
    // TODO: the user should set this in the update method: renderEnabled = camera.isInView(this)

    /**
     * Does the necessary matrix calculations.
     * This method is called automatically per frame, unless the [isStationary] is set to true.
     */
    override fun calculateTransformations() {
        // NOTE: isStationary is checked in the Scene.update method

        if (modelMatrixEnabled && _model != null) {

            // WARNING: this copy-from-model functionality and matrix.rotate are untested!

            // copy the model matrix:
            matrix.copyFrom(_model!!.matrix)

            // translate:
            matrix.translate(position)

            // rotate:
            //matrix.rotate(rotation)
            matrix.setRotation(rotation)

        } else {
            // reset the matrix to identity:
            matrix.setIdentity()

            // translate:
            matrix.translate(position)

            // set rotation:
            matrix.setRotation(rotation)
        }

        if (normalMatrixEnabled) {
            normalMatrix.copyFrom(matrix)
            /*if*/ normalMatrix.invert()
            normalMatrix.transpose()
            // NOTE: normalMatrix is now the inverted transposed 'model matrix'
        }

        // get the 'forward', 'up' and 'right' before scaling:
        _forward.x = matrix[0, 2]
        _forward.y = matrix[1, 2]
        _forward.z = matrix[2, 2]
        _up.x = matrix[0, 1]
        _up.y = matrix[1, 1]
        _up.z = matrix[2, 1]
        _right.x = matrix[0, 0]
        _right.y = matrix[1, 0]
        _right.z = matrix[2, 0]

        // scale after normal matrix calc:
        matrix.scale(scale)

    }


    /**
     * Updates the object logic. This is an open function and the default implementation is empty.
     * You do not need to call this function yourself, it is called automatically per frame by the
     * rendering queue unless the [isStationary] is set to true.
     *
     * @param[frameTime] The time (in seconds) it took to complete the previous frame. This depends on the selected/achieved update frame rate.
     */
    open fun update(frameTime: Float) {}


    /**
     * Renders the shared Model with the specified [camera].
     * This method is called automatically per frame with the [Scene.activeCamera].
     */
    fun render(camera: Camera3D) {
        if (_model != null) {

            // prepare the model for rendering:
            if (customMaterialPropertiesEnabled)
                _model!!.onAboutToRender(_mProps)
            else _model!!.onAboutToRender()

            // do we have the uniform locations?
            val mvpMU = _model!!.sharedMaterial?.mvpMatrixUniform ?: -1
            val nrmMU = _model!!.sharedMaterial?.normalMatrixUniform ?: -1

            // set the mvpMatrix for the shader:
            if (mvpMU != -1) {
                // we do the mv- and mvp-matrix multiplication here:
                android.opengl.Matrix.multiplyMM(mvMatrix.array, 0, camera.viewMatrix.array, 0, matrix.array, 0)
                android.opengl.Matrix.multiplyMM(mvpMatrix.array, 0, camera.projectionMatrix.array, 0, mvMatrix.array, 0)
                android.opengl.GLES20.glUniformMatrix4fv(mvpMU, 1, false, mvpMatrix.array, 0)
                // TODO: set the mvMatrix uniform for the shader here, if needed.
            }

            // set the normalMatrix for the shader:
            if (nrmMU != -1)
                android.opengl.GLES20.glUniformMatrix4fv(nrmMU, 1, false, normalMatrix.array, 0)

            // render the model:
            _model!!.render()
        }

    }

    override fun toString(): String = "${name}(stationary:${_isStationary}, model:${_model?.name})"

}


