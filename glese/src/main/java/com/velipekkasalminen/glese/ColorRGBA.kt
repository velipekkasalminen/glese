package com.velipekkasalminen.glese


import android.graphics.Color.parseColor
import android.graphics.ColorMatrix
import android.renderscript.Float3
import android.renderscript.Float4
import androidx.annotation.ColorInt
import androidx.annotation.FloatRange
import androidx.core.graphics.ColorUtils.HSLToColor
import com.velipekkasalminen.glese.utils.Tuple3
import com.velipekkasalminen.glese.utils.Tuple4
import java.text.DecimalFormat
import kotlin.math.abs
import kotlin.math.pow



// NOTE: the newer android.graphics.Color functions requires API Level 26

/**
 * ColorRGBA (GL_RGBA32F) represents a color with four components: 'r' for red, 'g' for green, 'b' for blue and 'a' for alpha.
 * Each component is a float value of normalized intensity that ranges typically from 0.0 to 1.0,
 * values outside of this range are considered being in the high dynamic range and are subject to clamping,
 * if displays, renderers and/or shaders do not support HDR. ColorRGBA works with linear and non-linear scRGB color spaces.
 *
 */
class ColorRGBA {

    var r: Float = 0f
    var g: Float = 0f
    var b: Float = 0f
    var a: Float = 1f // <--- the alpha is set to 1 by default

    // components for destructuring,
    // rgb and rgba setters and getters do about the same...
    operator fun component1() = r
    operator fun component2() = g
    operator fun component3() = b
    operator fun component4() = a

    /**
     * Simple swizzling for the red, green and blue components,
     * the alpha component is ignored.
     * Example usage: var (red, green, blue) = myColor.rgb
     */
    var rgb
        get() = Tuple3(r, g, b)
        set(value) {
            r = value.t1
            g = value.t2
            b = value.t3
            //a = value.t4
        }

    /**
     * Simple swizzling for the red, green, blue and alpha components.
     * Example usage: var (red, green, blue, alpha) = myColor.rgba
     */
    var rgba
        get() = Tuple4(r, g, b, a)
        set(value) {
            r = value.t1
            g = value.t2
            b = value.t3
            a = value.t4
        }


    /**
     * Hexadecimal Int value representing this ColorRGBA.
     * NOTE: conversion errors may occur between floating point and integer values.
     * NOTE: getter will always return a 'ColorInt' in AARRGGBB format,
     * setter supports RRGGBB and AARRGGBB formats.
     */
    var hex : Int
        get() = (((255f * a).toInt() and 0xFF) shl 24) or
                (((255f * r).toInt() and 0xFF) shl 16) or
                (((255f * g).toInt() and 0xFF) shl 8) or
                ((255f * b).toInt() and 0xFF)

        set(value) {
            // detect if hex is an AARRGGBB value:
            a = if (value < 0 || value > 0xFFFFFF) {
                // get the alpha from AARRGGBB value:
                abs( (value shr 24) / 255f )
                // or just:
                // Color.alpha(value) / 255f
            } else 1.0f

            //get red, green and blue from RRGGBB value:
            r = ((value and 0xFF0000) shr 16) / 255f
            g = ((value and 0xFF00) shr 8) / 255f
            b = (value and 0xFF) / 255f
            // or just:
            // r = Color.red(value) / 255f
            // g = Color.green(value) / 255f
            // b = Color.blue(value) / 255f
        }

    /**
     * Creates a new black ColorRGBA instance.
     */
    constructor() // dummy constructor

    /**
     * Creates a new ColorRGBA instance by copying values from another ColorRGBA.
     */
    constructor(c: ColorRGBA) { r = c.r; g = c.g; b = c.b; a = c.a }

    /**
     * Creates a new ColorRGBA instance with specified [red], [green], [blue] and [alpha] values.
     * The supplied values should be in a range from 0.0 to 1.0
     */
    constructor(@FloatRange(from = 0.0, to = 1.0, fromInclusive = true, toInclusive = true) red: Float,
                @FloatRange(from = 0.0, to = 1.0, fromInclusive = true, toInclusive = true) green: Float,
                @FloatRange(from = 0.0, to = 1.0, fromInclusive = true, toInclusive = true) blue: Float,
                @FloatRange(from = 0.0, to = 1.0, fromInclusive = true, toInclusive = true) alpha: Float = 1.0f) {
        r = red; g = green; b = blue; a = alpha
    }

    /**
     * Creates a new instance of ColorRGBA using a hexadecimal color value.
     * The value can be a CSS color value or a color value from the android resources.
     * Both RRGGBB and AARRGGBB formats are supported.
     *
     * @param[hex] Hexadecimal 'ColorInt' value.
     */
    constructor(@ColorInt hex: Int) {
        this.hex = hex
    }

    /**
     * Creates a new instance of ColorRGBA from a string containing a hexadecimal color value.
     * Both RRGGBB and AARRGGBB formats are supported.<br />
     * The following names are also accepted: red, blue, green, black, white, gray, cyan,
     * magenta, yellow, lightgray, darkgray, grey, lightgrey, darkgrey, aqua, fuchsia,
     * lime, maroon, navy, olive, purple, silver, and teal.
     *
     * @see [android.graphics.Color.parseColor]
     */
    constructor(s: String) {
        hex = parseColor(s)
    }

    /**
     * Converts HSL values to a RGB color value, alpha is set to 1.0.<br />
     *
     * @param[hue] The hue component, given in degrees from 0 to 360 (exclusive).
     * @param[saturation] The saturation level of the color, range of the value is from 0 to 1.
     * @param[lightness] The lightness/luminance of the color, ranges from 0 to 1.
     */
    fun hsl(@FloatRange(from = 0.0, to = 360.0, fromInclusive = true, toInclusive = false) hue: Float,
            @FloatRange(from = 0.0, to = 1.0, fromInclusive = true, toInclusive = true) saturation: Float,
            @FloatRange(from = 0.0, to = 1.0, fromInclusive = true, toInclusive = true) lightness: Float) {
        hex = HSLToColor(floatArrayOf(hue, saturation, lightness))
    }

    /**
     * Clamps each color component to a range from [min] to [max].
     */
    fun clamp(min: Float = 0f, max: Float = 1f) {
        // Kotlin nonsense for the ternary operator (a ? b : c)
        r = if (r < min) min else if (r > max) max else r
        g = if (g < min) min else if (g > max) max else g
        b = if (b < min) min else if (b > max) max else b
        a = if (a < min) min else if (a > max) max else a
    }

    /**
     * Inverts the red, green and blue values.
     * Values are assumed to be between 0.0 and 1.0,
     * You may need to call [clamp] to ensure this.
     */
    fun invert() {
        r = 1f - r
        g = 1f - g
        b = 1f - b
    }

    /**
     * Raises the red, green and blue values to the power [p]
     * and multiplies by [n].
     * This function can be used for simple gamma correction, when [p] is
     * understood as the gamma value.
     */
    fun applyGamma(p: Float, n: Float = 1f) {
        r = n * r.pow(p)
        g = n * g.pow(p)
        b = n * b.pow(p)
    }

    /**
     * Creates a new ColorMatrix from this color for image manipulation.
     * NOTE: pure white color with alpha at 1.0 will create a matrix akin to an identity matrix.
     *
     */
    fun toColorMatrix(): ColorMatrix = ColorMatrix(
        floatArrayOf(
            r, 0f, 0f, 0f, 0f,
            0f, g, 0f, 0f, 0f,
            0f, 0f, b, 0f, 0f,
            0f, 0f, 0f, a, 0f)
    )

    /** Returns the values of this ColorRGBA as a FloatArray with four floats */
    fun toFloatArray(): FloatArray = floatArrayOf(r,g,b,a)

    /**
     * Returns the values of this ColorRGBA as a renderscript Float3 structure,
     * the alpha component is ignored
     */
    fun toFloat3(): Float3 = Float3(r,g,b)

    /** Returns the values of this ColorRGBA as a renderscript Float4 structure */
    fun toFloat4(): Float4 = Float4(r,g,b,a)

    companion object {

        /** Creates a new ColorMatrix for image manipulation. */
        fun createColorMatrix(
            redChannel: ColorRGBA,
            greenChannel: ColorRGBA,
            blueChannel: ColorRGBA,
            alphaChannel: ColorRGBA,
            additiveColor: ColorRGBA): ColorMatrix = ColorMatrix(
            floatArrayOf(
                redChannel.r, redChannel.g, redChannel.b, redChannel.a, additiveColor.r,
                greenChannel.r, greenChannel.g, greenChannel.b, greenChannel.a, additiveColor.g,
                blueChannel.r, blueChannel.g, blueChannel.b, blueChannel.a, additiveColor.b,
                alphaChannel.r, alphaChannel.g, alphaChannel.b, alphaChannel.a, additiveColor.a)
        )

        // Some predefined colors:

        val white by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(1f, 1f, 1f) }
        val beige by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.96f, 0.96f, 0.86f) }
        val silver by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.75f, 0.75f, 0.75f) }
        val gold by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(1f, 0.84f, 0f) }
        val red by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(1f, 0f, 0f) }
        val green by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0f, 1f, 0f) }
        val blue by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0f, 0f, 1f) }
        val cyan by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0f, 1f, 1f) }
        val yellow by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(1f, 1f, 0f) }
        val magenta by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(1f, 0f, 1f) }
        val indigo by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.3f, 0f, 0.5f) }
        val maroon by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.5f, 0f, 0f) }
        val brown by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.6f, 0.3f, 0f) }
        val olive by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.5f, 0.5f, 0f) }
        val azure by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0f, 0.5f, 1f) }
        val amber by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(1f, 0.75f, 0f) }
        val crimson by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.86f, 0.1f, 0.25f) }
        val orange by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(1f, 0.5f, 0f) }
        val purple by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.5f, 0f, 0.5f) }
        val teal by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0f, 0.5f, 0.5f) }
        val pink by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(1f, 0f, 0.5f) }
        val turquoise by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.25f, 0.88f, 0.82f) }
        val androidGreen by lazy(LazyThreadSafetyMode.PUBLICATION) { ColorRGBA(0.64f, 0.78f, 0.22f) }

        // The following should be replaced/removed:
        private val _df = DecimalFormat("0.0000")
    }

    override fun toString(): String = "ColorRGBA(${_df.format(r)}, ${_df.format(g)}, ${_df.format(b)}, ${_df.format(a)})"

}


