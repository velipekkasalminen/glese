package com.velipekkasalminen.glese


import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.opengl.GLES20
import android.opengl.GLU
import android.renderscript.Float2
import android.renderscript.Int2
import android.renderscript.Int3
import android.view.KeyEvent
import android.view.MotionEvent
import com.velipekkasalminen.glese.utils.asString
import java.io.InputStream
import kotlin.reflect.KFunction0
import kotlin.reflect.KFunction1
import kotlin.reflect.KFunction2



/**
 * Render queue location types for [Material],
 * used by [Scene] while rendering.
 */
enum class RenderQueueLocation {

    /**
     * Unknown render queue; objects using materials with no rendering queue defined
     * will not be rendered.
     */
    UNKNOWN,

    /**
     * Render queue for nontransparent objects.
     * The depth buffering should be enabled on the materials used with this queue.
     */
    SOLIDGEOMETRY,

    /**
     * Render queue for transparent (possibly additively rendered) objects.
     * The depth buffering may be disabled on the materials used with this queue.
     */
    ADDITIVE,

    /**
     * Render queue for alpha tested transparent objects that
     * are sorted based on the distance to the active camera.
     * The depth buffering may be disabled on the materials used with this queue.
     */
    ALPHATESTED,

    /**
     * Render queue for object intended to be rendered in the screen space.
     * The depth buffering should be disabled on the materials used with this queue.
     */
    SCREENSPACE
}

/**
 * The supported OpenGL state modifying functions for rendering control.
 * Used with Engine.changeState and with Material.addStateChange
 */
enum class OpenGLStateFunction {

    /**
     * Enables or disables blending. Param type is Boolean.
     * Use true to enable, false to disable.
     */
    ENABLE_BLEND,

    /**
     * Enables or disables triangle face culling. Param type is Boolean.
     * Use true to enable, false to disable.
     */
    ENABLE_CULL_FACE,

    /**
     * Enables or disables depth testing. Param type is Boolean.
     * Use true to enable, false to disable.
     */
    ENABLE_DEPTH_TEST,

    /**
     * Enables or disables dithering. Param type is Boolean.
     * Use true to enable, false to disable.
     */
    ENABLE_DITHER,

    /**
     * Enables or disables the stencil test. Param type is Boolean.
     * Use true to enable, false to disable.
     */
    ENABLE_STENCIL_TEST,

    /**
     * Clears buffers. Param type is Int,
     * for example: GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT
     */
    CLEAR,

    /**
     * Sets the clear color. Param type is ColorRGBA,
     * for example: ColorRGBA(0f, 0f, 0f, 0f)
     */
    CLEARCOLOR,

    /**
     * Sets the clear value for the depth buffer. Param type is Float,
     * for example: 1f
     */
    CLEARDEPTHF,

    /**
     * Sets the clear value for the stencil buffer. Param type is Int,
     * for example: 0
     */
    CLEARSTENCIL,

    /**
     * Sets the color channels on or off. Param type is BooleanArray with 4 booleans
     * for red, green, blue and alpha.
     */
    COLORMASK,

    /**
     * Sets the triangle culling. Param type is Int,
     * for example: GL_BACK
     */
    CULLFACE,

    /**
     * Defines front and back -facing triangles. Param type is Int,
     * for example: GL_CCW
     */
    FRONTFACE,

    /**
     * Sets the blend color. Param type is ColorRGBA,
     * for example: ColorRGBA(0f, 0f, 0f, 0f)
     */
    BLENDCOLOR,

    /**
     * Sets the equation used for blending. Param type is Int,
     * for example: GL_FUNC_ADD
     */
    BLENDEQUATION,

    /**
     * Sets the blend function for the fragments/pixels.
     * Param type is renderscript.Int2, for example: Int2(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
     */
    BLENDFUNC,

    /**
     * Sets the depth function for the depth/z -buffer. Param type is Int,
     * for example: GL_LESS
     */
    DEPTHFUNC,

    /**
     * Enables or disables writing into the depth buffer. Param type is Boolean.
     * Use true to enable, false to disable.
     */
    DEPTHMASK,

    /**
     * Sets the mapping of depth values from normalized device coordinates to window coordinates.
     * Param type if renderscript.Float2, for example: Float2(0f, 1f)
     */
    DEPTHRANGEF,

    /**
     * Sets the stencil function, reference value for the stencil test and the ANDed mask value.
     * Param type is renderscript.Int3, for example: Int3(GL_ALWAYS, 0, 0xFF)
     */
    STENCILFUNC,

    /**
     * Sets the stencil mask value. Param type is Int,
     * for example: 0xFF
     */
    STENCILMASK,

    /**
     * Sets the stencil test operations. Param type is renderscript.Int3,
     * for example: Int3(GL_KEEP, GL_KEEP, GL_KEEP)
     */
    STENCILOP

}


/**
 * The Engine is a single point of access to managers, callbacks, the renderer, and the surface view.
 * Note that most the Engine functionality can be only used within the rendering thread; once the
 * [Engine.start] has been called, the [Engine.surfaceView] can be utilized to queue events (runnables)
 * on the rendering thread by using the queueEvent -function of the surface view.
 */
object Engine {

    /**
     * An asset manager (android.content.res.AssetManager)
     * obtained from application context.
     */
    lateinit var assets: AssetManager


    /**
     * Creates and returns a GleseSurfaceView using the provided [context],
     * and starts the engine with the desired starting configuration.
     */
    fun start(context: Context, config: GleseStartConfig): GleseSurfaceView {
        // dump the config to (static) GleseSurfaceView.config -field:
        GleseSurfaceView.config = config

        // FIXME: assets should be accessed through surfaceView.context,
        //        example:  val assets: AssetManager get() = surfaceView.context.assets

        assets = context.assets

        // all the 'starting' is actually done by the surface view:
        return GleseSurfaceView(context)
    }

    // TODO: a counterpart for start ( fun end() ) that deletes all objects from managers (?)
    //       -> gets called when app ends.


    // WARNING: most the Engine functions can be only used within the rendering thread,
    // using the Engine outside the rendering thread may cause problems.
    // TIP: Compare Engine.renderer.threadId with Thread.currentThread().id

    // texture manager class:
    class TextureManager() : BaseResMap<Texture>() {

        private val _tuSize: Int = if ( textureUnitCount >= 8 ) textureUnitCount else 8

        // texture units in use:
        private val _tuUse: BooleanArray

        init {
            if (!Engine::renderer.isInitialized) throw Exception("Can not init \'textures\' TextureManager: Engine.renderer has not been initialized.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not init \'textures\' TextureManager outside the rendering thread.")
            _tuUse = BooleanArray(_tuSize)
        }

        /**
         * Returns the first available texture unit number or -1 if there are no texture units available.
         * Note that the returned integer value corresponds with one of the GL_TEXTURE* values,
         * for example the integer 0 correspond with GL_TEXTURE0.
         */
        fun availableTextureUnit(): Int {
            for ((i, b) in _tuUse.withIndex()) {
                if (!b) return i
            }
            return -1
        }

        /**
         * Returns true if the specified texture unit number is available.
         * @param[i] The texture unit number, starting from 0, not from GL_TEXTURE0
         */
        fun isTextureUnitAvailable(i: Int): Boolean {
            if (i < 0 || i >= _tuSize) return false
            if (_tuUse[i]) return false
            return true
        }

        /**
         * Reserve a texture unit number for your own use. The texture manager will not assign any
         * textures to the reserved unit until released by calling [releaseTextureUnit].
         *
         * @param[i] The texture unit number, starting from 0, not from GL_TEXTURE0
         * @return True if the texture unit was successfully reserved, false if the unit is already reserved.
         */
        fun reserveTextureUnit(i: Int): Boolean {
            if (i < 0 || i >= _tuSize) return false
            if (_tuUse[i]) return false
            _tuUse[i] = true
            return true
        }

        /**
         * Marks a texture unit as free, no matter if the unit is in use or not.
         */
        fun releaseTextureUnit(i: Int) {
            if (i < 0 || i >= _tuSize) return
            _tuUse[i] = false
        }

        /**
         * Creates a Texture2D object from a bitmap, and adds the texture to the
         * [textures] manager, and returns a reference to the created texture.
         * The bitmap can be released and garbage collected after this operation.
         * Note that the texture can also be accessed via the given [name],
         * for example: Engine.textures["myTexture2D"]
         */
        fun createTexture2D(name: String, bitmap: Bitmap,
                generateMipmaps: Boolean = false,
                mipmapFiltering: Int = GLES20.GL_NEAREST_MIPMAP_LINEAR,
                preferredTextureUnit: Int = -1): Texture2D {
            if (name.isBlank()) throw Exception("The name parameter can not be empty or blank.")
            if (contains(name)) throw Exception("A texture named $name already exists.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.meshes.createTexture2D(\"$name\", ... )\' outside the rendering thread.")
            val tu = if (isTextureUnitAvailable(preferredTextureUnit)) preferredTextureUnit else availableTextureUnit()
            if (tu == -1) throw Exception("No texture units available.")

            // construct the texture:
            val tex2D = Texture2D(name)
            val err = tex2D.construct(bitmap, tu, generateMipmaps, mipmapFiltering)
            if (err != 0) {
                if (err != TEXTURE_WARNING_NPOT)
                    throw Exception("CreateTexture2D error: ${getErrorString(err)}")
            }

            // add the tex2D to the map
            _resMap[name.hashCode()] = tex2D

            // Mark the texture unit:
            _tuUse[tu] = true

            return tex2D
        }

        /**
         * Creates a TextureCubemap object from six bitmaps.
         * The bitmaps can be released and garbage collected after this operation.
         */
        fun createCubemap(name: String,
                  front: Bitmap, back: Bitmap, left: Bitmap,
                  right: Bitmap, top: Bitmap, bottom: Bitmap,
                  preferredTextureUnit: Int = -1): TextureCubemap {
            if (name.isBlank()) throw Exception("The name parameter can not be empty or blank.")
            if (contains(name)) throw Exception("A texture named $name already exists.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.meshes.createCubemap(\"$name\", ... )\' outside the rendering thread.")
            val tu = if (isTextureUnitAvailable(preferredTextureUnit)) preferredTextureUnit else availableTextureUnit()
            if (tu == -1) throw Exception("No texture units available.")

            // construct the cubemap:
            val cubemap = TextureCubemap(name)
            val err = cubemap.construct(front, back, left, right, top, bottom, tu)
            if (err != 0) {
                throw Exception("createCubemap error: ${getErrorString(err)}")
            }

            // add the cubemap to the resMap:
            _resMap[name.hashCode()] = cubemap

            _tuUse[tu] = true

            return cubemap
        }

        /**
         * Deletes the named texture allocation from the gpu memory and 'releases' the texture unit.
         */
        override fun delete(name: String): Boolean {
            val h = name.hashCode()
            if(!_resMap.containsKey(h)) return false
            _resMap[h]?.textureUniform?.let { releaseTextureUnit(it) }
            _resMap[h]?.delete()
            _resMap.remove(h)
            return true
        }

        /**
         * Deletes all gpu-allocated textures.
         */
        override fun deleteAll() {
            for (tex in _resMap.values) {
                releaseTextureUnit(tex.textureUniform)
                tex.delete()
            }
            _resMap.clear()
        }

    } // <--- end of 'TextureManager'

    // mesh manager class:
    class MeshManager(): BaseResMap<Mesh>() {

        init {
            if (!Engine::renderer.isInitialized) throw Exception("Can not init \'meshes\' MeshManager: Engine.renderer has not been initialized.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not init \'meshes\' MeshManager outside the rendering thread.")
        }

        /**
         * Creates a Mesh from a Stanford polygon file (PLY), and adds it to the [meshes] manager.
         * Note that this method closes the input stream.
         *
         * @param[name] An unique name for this Mesh object.
         * @param[plyStream] Input stream from a ply -asset file, for example.
         * @return The created Mesh object that was added to the manager.
         */
        fun createFromPly(name: String, plyStream: InputStream): Mesh {
            if (name.isBlank()) throw Exception("The name parameter can not be empty or blank.")
            if (contains(name)) throw Exception("A mesh named $name already exists.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.meshes.createFromPly(\"$name\", ... )\' outside the rendering thread.")

            val mesh = Mesh(name)
            val vdat = VertexData()

            // load vertex data from the ply stream:
            var err = 0
            plyStream.use {
                err = vdat.loadFromPly(it)
            }

            if (err != 0) {
                throw Exception("CreateMeshFromPly vertex data load error: ${getErrorString(err)}")
            }

            // construct the mesh:
            err = mesh.construct(vdat.vertexArray, vdat.indexArray, true,
                    vdat.normalArray, vdat.textureCoordinateArray, vdat.colorArray)
            if (err != 0) {
                throw Exception("CreateMeshFromPly mesh construct error: ${getErrorString(err)}")
            }

            // add the mesh to the map:
            _resMap[name.hashCode()] = mesh

            return mesh
        }

        /**
         * Creates a Mesh object for a cube and adds it to the [meshes] manager.
         * @param[name] An unique name for this Mesh object.
         * @param[size] The width, height and length of the cube. Default size is one (unit of measure).
         * @param[normals] True if each vertex should have a normal vector.
         * @param[textureCoordinates] True if each vertex should have texture coordinates.
         * @param[vertexColors] True if each vertex should have color data.
         * @param[vertexColor] The color of the vertices, if the color data is enabled by setting [vertexColors] to true.
         * @return The created cube Mesh object that was added to the manager.
         */
        fun createCube(name: String, size: Float = 1f, vertexNormals: Boolean = false,
                       textureCoordinates: Boolean = false, vertexColors: Boolean = false,
                       vertexColor: ColorRGBA = ColorRGBA(1f,1f,1f,1f)): Mesh {
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.meshes.createCube(\"$name\", ... )\' outside the rendering thread.")
            val vdat = VertexData.createCubeData(size, vertexNormals, textureCoordinates, vertexColors)
            if (vertexColors)
                vdat.changeVertexColors(vertexColor)
            return create(name, vdat)
        }

        /**
         * Creates a Mesh from a vertex data object and adds it to the [meshes] manager.
         * @param[name] An unique name for this Mesh object.
         * @param[data] Vertex data describing the mesh.
         * @return The created Mesh object that was added to the manager.
         */
        fun create(name: String, data: VertexData): Mesh {
            if (name.isBlank()) throw Exception("The name parameter can not be empty or blank.")
            if (contains(name)) throw Exception("A mesh named $name already exists.")
            if (!data.isReady) throw Exception("The vertex data object is not ready to be used in mesh construction.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.meshes.create(\"$name\", ... )\' outside the rendering thread.")

            val mesh = Mesh(name)

            // construct the mesh:
            // TODO: if later implementations of VertexData contain arrays for tangents, binormals,
            //       or others, add them here!
            val err = mesh.construct(data.vertexArray, data.indexArray, true,
                data.normalArray, data.textureCoordinateArray, data.colorArray)
            if (err != 0) {
                throw Exception("CreateMesh error: ${getErrorString(err)}")
            }

            // add the mesh to the map:
            _resMap[name.hashCode()] = mesh

            return mesh
        }

    } // <-- end of 'MeshManager'

    // shader program manager:
    class ShaderProgramManager(): BaseResMap<ShaderProgram>() {

        init {
            if (!Engine::renderer.isInitialized) throw Exception("Can not init \'shaders\' ShaderProgramManager: Engine.renderer has not been initialized.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not init \'shaders\' ShaderProgramManager outside the rendering thread.")
            if (!shaderCompilerAvailable)
                throw Exception("Can not init \'shaders\' ShaderProgramManager: shader compiler is not available.")
        }

        /*
        An example of the 'attributes' map parameter on create (ShaderProgram) function:
        mapOf(VERTEX_ATTRIBUTE_POSITIONS to "vertexPosition",
              VERTEX_ATTRIBUTE_TEXTURE_COORDS to "textureCoord",
              VERTEX_ATTRIBUTE_NORMALS to "vertexNormal",
              VERTEX_ATTRIBUTE_COLORS to "vertexColor")
        */

        /**
         * Creates a ShaderProgram from [vertexShader] source code, [fragmentShader] source code
         * and vertex [attributes] map. The shader program is then added to the [shaders] manager.
         * Note that this method will close the input streams.
         */
        fun create(name: String, vertexShader: InputStream,
                    fragmentShader: InputStream, attributes: Map<Int, String>? = null): ShaderProgram {
            if (name.isBlank()) throw Exception("The name parameter can not be empty or blank.")
            if (contains(name)) throw Exception("A shader program named $name already exists.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.shaders.create(\"$name\", ... )\' outside the rendering thread.")

            val vsText = asString(vertexShader)
            if (vsText.length < 14) throw Exception("Vertex shader source code is empty.")

            val fsText = asString(fragmentShader)
            if (fsText.length < 14) throw Exception("Fragment shader source code is empty.")

            // construct the shader program:
            val sp = ShaderProgram(name)
            val err = sp.construct(vsText, fsText, attributes)
            if (err != 0) {
                throw Exception("CreateShaderProgram error: ${getErrorString(err)}")
            }

            // add the shader program to the map:
            _resMap[name.hashCode()] = sp

            return sp
        }

    } // <--- end of 'ShaderProgramManager'

    // material manager:
    class MaterialManager(): BaseResMap<Material>() {

        init {
            if (!Engine::renderer.isInitialized) throw Exception("Can not init \'materials\' MaterialManager: Engine.renderer has not been initialized.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not init 'materials' MaterialManager outside the rendering thread.")
        }

        /**
         * Creates a Material using an existing shader program, adds the material to the [materials]
         * manager, and returns a reference to the created material.
         */
        fun create(name: String, shaderProgramName: String, renderQueueLocation: RenderQueueLocation): Material {
            if (name.isBlank()) throw Exception("The name parameter can not be empty or blank.")
            if (shaderProgramName.isBlank()) throw Exception("The shaderProgramName parameter can not be empty or blank.")
            if (contains(name)) throw Exception("A material named $name already exists.")
            if(!shaders.contains(shaderProgramName)) throw Exception("No shader program named $name exist.")
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.materials.create(\"$name\", ... )\' outside the rendering thread.")

            // create the material:
            val material = Material(name, shaders[shaderProgramName])
            material.renderQueueLocation = renderQueueLocation

            // add the material to the map:
            _resMap[name.hashCode()] = material

            return material
        }

    } // <--- end of 'MaterialManager'

    // model manager:
    class ModelManager {

        private val _map = mutableMapOf<Int, Model3D>()

        operator fun get(name: String): Model3D {
            return _map[name.hashCode()] ?: throw IllegalArgumentException("Invalid use of indexed access: $name does not exist.")
        }

        operator fun set(name: String, m: Model3D) {
            val h = name.hashCode()
            if (_map.containsKey(h)) throw IllegalArgumentException("Invalid use of indexed access: $name already exists and can not be changed.")
            _map[h] = m
        }

        operator fun contains(name: String) = _map.contains(name.hashCode())

        val count get() = _map.count()

        /**
         * Creates a model, and adds it to the [models] manager.
         * @param[name] An unique name for this Model3D object.
         * @param[meshName] An existing Mesh object name in the [meshes] manager.
         * @param[materialName] An existing Material object name in the [materials] manager.
         * @return The created Model3D object that was added to the manager.
         */
        fun create(name: String, meshName: String? = null, materialName: String? = null): Model3D {
            if (name.isBlank()) throw Exception("The name parameter can not be empty or blank.")
            if (meshName != null && !meshes.contains(meshName)) throw Exception("No mesh named $name exist.")
            if (materialName != null && !materials.contains(materialName)) throw Exception("No material named $name exist.")

            // create the model and set mesh and material (if not null)
            val model = Model3D(name)
            if (meshName != null) model.sharedMesh = meshes[meshName]
            if (materialName != null) model.sharedMaterial = materials[materialName]

            // add to the map:
            _map[name.hashCode()] = model

            return model
        }

        /**
         * Deletes the named model object from the manager.
         * This does not delete the material or the mesh used by the model.
         */
        fun delete(name: String): Boolean {
            val h = name.hashCode()
            if (!_map.containsKey(h)) return false
            _map.remove(h)
            return true
        }

        /** Removes all models from the manager. */
        fun deleteAll() = _map.clear()

    } // <--- end of 'ModelManager'


    // scene manager:
    class SceneManager {

        private val _map = mutableMapOf<Int, Scene>()

        operator fun get(name: String): Scene {
            return _map[name.hashCode()] ?: throw IllegalArgumentException("Invalid use of indexed access: $name does not exist.")
        }

        // NOTE: 'operator fun set' was removed, use 'add' instead.

        operator fun contains(name: String) = _map.contains(name.hashCode())

        val count get() = _map.count()

        private var _activeScene: Scene? = null

        /** The currently active scene, or null if there is no active scene present. */
        val activeScene: Scene? get() = _activeScene

        /**
         * Adds a new Scene to [scenes] manager.
         * The onCreate method of the scene is called immediately, if the scene is added on the running
         * rendering thread, otherwise the onCreate call is deferred.
         * Note that each scene object must have an unique name.
         *
         * @return False, if a scene with the same name already exists.
         */
        fun add(scene: Scene): Boolean {
            val h = scene.name.hashCode()
            if (_map.containsKey(h)) return false

            // call the 'onCreate' on the scene if we are on the rendering thread:
            if (::renderer.isInitialized && renderer.threadId == Thread.currentThread().id) {
                // add the scene to the map:
                _map[h] = scene
                scene.onCreate()
            } // else we add the scene later, when we are on the running rendering thread:
            else GleseRenderer.addToSceneBouncerQueue(scene)

            return true
        }

        /**
         * Starts ('loads') a scene, and makes it the current [activeScene].
         * The previous active scene will be 'unloaded'.
         * The onUnload method of the previous active scene will be called before onStart on the
         * new active scene.
         * @param[name] The name of the scene to start.
         * @return False, if the scene is not found.
         */
        fun load(name: String): Boolean {
            val h = name.hashCode()
            if (!_map.containsKey(h)) return false

            // we need to make sure we are operating within the rendering thread:
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.scenes.load(\"$name\")\' outside the rendering thread.")

            // if we have a scene already active, we call onUnload on it:
            _activeScene?.onUnload()

            // start the desired scene:
            _map[h]?.onStart()
            _activeScene = _map[h]

            return true
        }

        /**
         * Deletes a scene from the [scenes] manager.
         * The onDelete method of the scene will be called before the scene is removed.
         * @param[name] The name of the scene to remove.
         * @return False, if the scene is not found.
         */
        fun delete(name: String): Boolean {
            val h = name.hashCode()
            if (!_map.containsKey(h)) return false

            // FIXME: can not delete the _activeScene !!!

            // we need to make sure we are operating within the rendering thread:
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.scenes.delete(\"$name\")\' outside the rendering thread.")

            _map[h]?.onDelete()

            // remove the scene from the map:
            _map.remove(h)

            return true
        }

        /** Deletes all of the scenes. */
        fun deleteAll() {
            if (renderer.threadId != Thread.currentThread().id)
                throw Exception("Can not call \'Engine.scenes.deleteAll()\' outside the rendering thread.")

            // FIXME: _activeScene is not set to null anywhere!

            for (scene in _map.values) {
                scene.onDelete()
            }

            _map.clear()
        }

    } // <--- end of 'SceneManager'

    /** Manages allocated textures and keeps a record of texture unit utilization. */
    val textures: TextureManager by lazy { TextureManager() }

    /** Manages allocated meshes. */
    val meshes: MeshManager by lazy { MeshManager() }

    /** Manages allocated shader programs. */
    val shaders: ShaderProgramManager by lazy { ShaderProgramManager() }

    /** Manages materials. */
    val materials: MaterialManager by lazy { MaterialManager() }

    /** Manages models. */
    val models: ModelManager by lazy { ModelManager() }

    /** Manages scenes, and scene lifecycle. */
    val scenes: SceneManager by lazy { SceneManager() }

    /**
     * Called when the GleseSurfaceView is being paused.
     * Remember to call the onPause method of the surface view on the activity's onPause and/or onStop
     * methods.
     */
    var onPauseCallback: KFunction0<Unit>? = null

    /**
     * Called when the GleseSurfaceView is being 'resumed' (usually from a paused state).
     * Remember to call the onResume method of the surface view on the activity's onStart,
     * onResume and/or onRestart methods.
     */
    var onResumeCallback: KFunction0<Unit>? = null

    /**
     * Called when the GleseSurfaceView receives a touch (or trackball) event.
     * The function requires a MotionEvent as parameter and must return a boolean value
     * indicating if the event was consumed (responded to).
     */
    var onMotionEventCallback: KFunction1<in MotionEvent, out Boolean>? = null

    /**
     * Called when a key is released. The function requires an Int (for the keyCode) and
     * a KeyEvent as parameters and must return a boolean value
     * indicating if the event was consumed (responded to).
     */
    var onKeyUpCallback: KFunction2<in Int, in KeyEvent, out Boolean>? = null

    /**
     * Called when a key is pressed. The function requires an Int (for the keyCode) and
     * a KeyEvent as parameters and must return a boolean value
     * indicating if the event was consumed (responded to).
     */
    var onKeyDownCallback: KFunction2<in Int, in KeyEvent, out Boolean>? = null

    /**
     * Called just before the frame is drawn (and the buffers get cleared).
     * This function has no parameters or return values.
     */
    var onDrawFrameCallback: KFunction0<Unit>? = null

    /**
     * Called when the screen dimensions change, for example, when the
     * orientation of the device changes.
     * This callback requires two Int parameters: width and height.
     * If the callback is null the default implementation
     * calls glViewport(0, 0, width, height)
     * Note that this callback may be called several times, and should not be
     * used to handle or allocate resources. If you decide to implement this callback,
     * you should at least set the glViewport (as mentioned) and change the
     * perspective projection of the active camera of the active scene ( accessed via [Engine.scenes] ).
     * See the documentation of [Camera3D.setPerspectiveProjection] on setting the perspective.
     *
     */
    var onSurfaceChangedCallback: KFunction2<in Int, in Int, out Unit>? = null

    /**
     * The renderer working on the surfaceView, initialized and set by the surfaceView.
     * Same as surfaceView.renderer
     */
    lateinit var renderer: GleseRenderer

    /**
     * The GleseSurfaceView object. Provides an access point to the
     * application context via surfaceView.context
     */
    lateinit var surfaceView: GleseSurfaceView

    /**
     * True, if the current implementation has a shader compiler available.
     * Note that this field must be accessed from within the rendering thread.
     */
    val shaderCompilerAvailable: Boolean by lazy {
        var p = intArrayOf(0)
        GLES20.glGetIntegerv(GLES20.GL_SHADER_COMPILER, p, 0)
        (p[0] != GLES20.GL_FALSE)
    }

    /**
     * The number of combined texture units available for all shader stages reported by the current implementation.
     * Note that this field must be accessed from within the rendering thread.
     */
    val textureUnitCount: Int by lazy {
        var p = intArrayOf(0)
        GLES20.glGetIntegerv(GLES20.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, p, 0)
        p[0]
    }

    /**
     * String containing some information about the current GL implementation.
     * Note that this field must be accessed from within the rendering thread.
     */
    val glInfo: String by lazy {
        var p = intArrayOf(0,0,0,0,0,0,0,0)
        GLES20.glGetIntegerv(GLES20.GL_MAX_VERTEX_UNIFORM_VECTORS, p, 0)
        GLES20.glGetIntegerv(GLES20.GL_MAX_FRAGMENT_UNIFORM_VECTORS, p, 1)
        GLES20.glGetIntegerv(GLES20.GL_MAX_VARYING_VECTORS, p, 2)
        GLES20.glGetIntegerv(GLES20.GL_MAX_VERTEX_ATTRIBS, p, 3)
        GLES20.glGetIntegerv(GLES20.GL_MAX_TEXTURE_SIZE, p, 4)
        GLES20.glGetIntegerv(GLES20.GL_MAX_CUBE_MAP_TEXTURE_SIZE, p, 5)
        GLES20.glGetIntegerv(GLES20.GL_MAX_RENDERBUFFER_SIZE, p, 6)
        GLES20.glGetIntegerv(GLES20.GL_MAX_TEXTURE_IMAGE_UNITS, p, 7)
        """
        GL_VERSION=${GLES20.glGetString(GLES20.GL_VERSION)}
        GL_RENDERER=${GLES20.glGetString(GLES20.GL_RENDERER)}
        GL_VENDOR=${GLES20.glGetString(GLES20.GL_VENDOR)}
        GL_SHADING_LANGUAGE_VERSION=${GLES20.glGetString(GLES20.GL_SHADING_LANGUAGE_VERSION)}
        GL_SHADER_COMPILER=${shaderCompilerAvailable}
        GL_MAX_TEXTURE_IMAGE_UNITS=${p[7]}
        GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS=${textureUnitCount}
        GL_MAX_VERTEX_UNIFORM_VECTORS=${p[0]}
        GL_MAX_FRAGMENT_UNIFORM_VECTORS=${p[1]}
        GL_MAX_VARYING_VECTORS=${p[2]}
        GL_MAX_VERTEX_ATTRIBS=${p[3]}
        GL_MAX_TEXTURE_SIZE=${p[4]}
        GL_MAX_CUBE_MAP_TEXTURE_SIZE=${p[5]}
        GL_MAX_RENDERBUFFER_SIZE=${p[6]}
        """.trimIndent()
    }

    /**
     * String containing a space-separated list of supported extensions or "None".
     * Note that this field must be accessed from within the rendering thread.
     */
    val glExtensions: String by lazy {
        GLES20.glGetString(GLES20.GL_EXTENSIONS) ?: "None"
    }


    /** Provides a hint for the implementation that there are no more shaders to compile. */
    fun releaseShaderCompiler() = GLES20.glReleaseShaderCompiler()

    /**
     * Makes a request to the gpu to change the state of one of the (supported) modifiable states.
     * Note that no sanity checks are made for the contents of [param], only the type is checked.
     *
     * @param[func] The OpenGL ES function to call.
     * @param[param] The parameter object for the function (see the [OpenGLStateFunction] documentation for the parameter object types).
     * @return True if the function call was made, false if the [param] type was invalid for the [func].
     */
    fun changeState(func: OpenGLStateFunction, param: Any): Boolean {
        when(func) {
            OpenGLStateFunction.ENABLE_DEPTH_TEST -> if (param is Boolean) {
                if (param) GLES20.glEnable(GLES20.GL_DEPTH_TEST) else GLES20.glDisable(GLES20.GL_DEPTH_TEST)
                return true }
            OpenGLStateFunction.ENABLE_CULL_FACE -> if (param is Boolean) {
                if (param) GLES20.glEnable(GLES20.GL_CULL_FACE) else GLES20.glDisable(GLES20.GL_CULL_FACE)
                return true }
            OpenGLStateFunction.ENABLE_BLEND -> if (param is Boolean) {
                if (param) GLES20.glEnable(GLES20.GL_BLEND) else GLES20.glDisable(GLES20.GL_BLEND)
                return true }
            OpenGLStateFunction.ENABLE_DITHER -> if (param is Boolean) {
                if (param) GLES20.glEnable(GLES20.GL_DITHER) else GLES20.glDisable(GLES20.GL_DITHER)
                return true }
            OpenGLStateFunction.ENABLE_STENCIL_TEST -> if (param is Boolean) {
                if (param) GLES20.glEnable(GLES20.GL_STENCIL_TEST) else GLES20.glDisable(GLES20.GL_STENCIL_TEST)
                return true }
            OpenGLStateFunction.CLEARCOLOR -> if (param is ColorRGBA) {
                GLES20.glClearColor(param.r, param.g, param.b, param.a); return true }
            OpenGLStateFunction.CLEAR -> if (param is Int) {
                GLES20.glClear(param); return true }
            OpenGLStateFunction.BLENDEQUATION -> if (param is Int) {
                GLES20.glBlendEquation(param); return true }
            OpenGLStateFunction.BLENDFUNC -> if (param is Int2) {
                GLES20.glBlendFunc(param.x, param.y); return true }
            OpenGLStateFunction.COLORMASK -> if (param is BooleanArray && param.size == 4) {
                GLES20.glColorMask(param[0], param[1], param[2], param[3]); return true }
            OpenGLStateFunction.CULLFACE -> if (param is Int) {
                GLES20.glCullFace(param); return true }
            OpenGLStateFunction.DEPTHFUNC -> if (param is Int) {
                GLES20.glDepthFunc(param); return true }
            OpenGLStateFunction.CLEARDEPTHF -> if (param is Float) {
                GLES20.glClearDepthf(param); return true }
            OpenGLStateFunction.CLEARSTENCIL -> if (param is Int) {
                GLES20.glClearStencil(param); return true }
            OpenGLStateFunction.FRONTFACE -> if (param is Int) {
                GLES20.glFrontFace(param); return true }
            OpenGLStateFunction.BLENDCOLOR -> if (param is ColorRGBA) {
                GLES20.glBlendColor(param.r, param.g, param.b, param.a); return true }
            OpenGLStateFunction.DEPTHMASK -> if (param is Boolean) {
                GLES20.glDepthMask(param); return true }
            OpenGLStateFunction.DEPTHRANGEF -> if (param is Float2) {
                GLES20.glDepthRangef(param.x, param.y); return true }
            OpenGLStateFunction.STENCILFUNC -> if (param is Int3) {
                GLES20.glStencilFunc(param.x, param.y, param.z); return true }
            OpenGLStateFunction.STENCILMASK -> if (param is Int) {
                GLES20.glStencilMask(param); return true }
            OpenGLStateFunction.STENCILOP -> if (param is Int3) {
                GLES20.glStencilOp(param.x, param.y, param.z); return true }
        }
        return false
    }

    fun resetState(func: OpenGLStateFunction) {
        when(func) {
            OpenGLStateFunction.CLEARCOLOR -> {
                // get the clear color set to the renderer:
                val c = renderer.clearColor
                GLES20.glClearColor(c.r, c.g, c.b, c.a)
                }
            OpenGLStateFunction.ENABLE_DEPTH_TEST -> GLES20.glEnable(GLES20.GL_DEPTH_TEST)
            OpenGLStateFunction.ENABLE_CULL_FACE -> GLES20.glEnable(GLES20.GL_CULL_FACE)
            OpenGLStateFunction.ENABLE_BLEND -> GLES20.glEnable(GLES20.GL_BLEND)
            OpenGLStateFunction.ENABLE_DITHER -> GLES20.glEnable(GLES20.GL_DITHER)
            OpenGLStateFunction.ENABLE_STENCIL_TEST -> GLES20.glDisable(GLES20.GL_STENCIL_TEST)
            OpenGLStateFunction.BLENDEQUATION -> GLES20.glBlendEquation(GLES20.GL_FUNC_ADD)
            OpenGLStateFunction.BLENDFUNC -> GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
            OpenGLStateFunction.CULLFACE -> GLES20.glCullFace(GLES20.GL_BACK)
            OpenGLStateFunction.DEPTHFUNC -> GLES20.glDepthFunc(GLES20.GL_LESS)
            OpenGLStateFunction.COLORMASK -> GLES20.glColorMask(true, true, true, true)
            OpenGLStateFunction.CLEARDEPTHF -> GLES20.glClearDepthf(1f)
            OpenGLStateFunction.CLEARSTENCIL -> GLES20.glClearStencil(0)
            OpenGLStateFunction.FRONTFACE -> GLES20.glFrontFace(GLES20.GL_CCW)
            OpenGLStateFunction.BLENDCOLOR -> GLES20.glBlendColor(0f, 0f, 0f, 0f)
            OpenGLStateFunction.DEPTHMASK -> GLES20.glDepthMask(true)
            OpenGLStateFunction.DEPTHRANGEF ->  GLES20.glDepthRangef(0f, 1f)

            // NOTE: I'm assuming the stencil buffer is an 8-bit buffer on the client implementation.
            //       The EGLConfigChooser should be used to set this. Use GL_STENCIL_BITS to check.
            OpenGLStateFunction.STENCILFUNC -> GLES20.glStencilFunc(GLES20.GL_ALWAYS, 0, 0xFF)
            OpenGLStateFunction.STENCILMASK -> GLES20.glStencilMask(0xFF)
            OpenGLStateFunction.STENCILOP -> GLES20.glStencilOp(GLES20.GL_KEEP, GLES20.GL_KEEP, GLES20.GL_KEEP)

            // NOTE: calling glClear would clear the current color and depth buffers AND
            //       end the current frame, thus we will not 'reset' the glClear here since it
            //       will be done at the end of the frame.
            //OpenGLStateFunction.CLEAR -> GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
            else -> { /* Unit */ }
        }
    }


    /**
     * Returns a string representation of an error code.
     */
    fun getErrorString(errorCode: Int): String = when (errorCode) {
        0 -> "None"
        MATERIAL_PROPERTY_WARNING_MAP_EMPTY -> "Empty material property map."
        MATERIAL_PROPERTY_WARNING_UNSUPPORTED_TYPE -> "Unsupported material property type."
        MATERIAL_NOT_INITIALIZED -> "Material not initialized."
        MESH_ALREADY_INITIALIZED -> "Mesh already initialized."
        MESH_INVALID_VERTEX_ARRAY_SIZE -> "Invalid vertex array size."
        MESH_INVALID_INDEX_ARRAY_SIZE -> "Invalid index array size."
        MESH_INVALID_NORMAL_ARRAY_SIZE -> "Invalid normal array size."
        MESH_INVALID_TEXCOORD_ARRAY_SIZE -> "Invalid texture coordinate array size."
        MESH_INVALID_COLOR_ARRAY_SIZE -> "Invalid color array size."
        MESH_ERRONEOUS_BUFFER_HANDLE -> "Erroneous buffer handle."
        SHADER_ALREADY_INITIALIZED -> "ShaderProgram already initialized."
        SHADER_COMPILER_NOT_AVAILABLE -> "Shader source code compiler is not available."
        SHADER_FS_HANDLE_NULL -> "Fragment shader handle is null."
        SHADER_VS_HANDLE_NULL -> "Vertex shader handle is null."
        SHADER_FS_COMPILE_FAILED -> "Fragment shader compilation failed."
        SHADER_VS_COMPILE_FAILED -> "Vertex shader compilation failed."
        SHADER_PROG_LINK_FAILED -> "Failed to link vertex and/or fragment shaders to a shader program."
        SHADER_PROG_HANDLE_NULL -> "Shader program handle is null."
        TEXTURE_ALREADY_INITIALIZED -> "Texture already initialized."
        TEXTURE_INVALID_TEXTURE_UNIT -> "Invalid texture unit number."
        TEXTURE_WARNING_NPOT -> "Non power-of-two texture."
        TEXTURE_NOT_INITIALIZED -> "Texture not initialized."
        PLY_HEADER_START_NOT_FOUND -> "Start of ply header was not found."
        PLY_HEADER_END_NOT_FOUND -> "End of ply header was not found."
        PLY_UNSUPPORTED_FORMAT -> "Unsupported ply format."
        PLY_ELEMENT_VERTEX_COUNT_ERROR -> "Invalid vertex count entry."
        PLY_ELEMENT_FACE_COUNT_ERROR -> "Invalid face count entry."
        PLY_PROPERTY_PARSE_ERROR -> "Property entry parse error."
        PLY_PROPERTIES_MISSING -> "Required properties missing."
        PLY_PROPERTY_DATA_TYPE_ERROR -> "Invalid property data type."
        PLY_WARNING_FACES_UNTRIANGULATED -> "Untriangulated faces encountered. Only triangles are supported."
        PLY_WARNING_VERTEX_INDEX_LIST_DEFINITION_MISSING -> "Definition on a list entry is missing: 'vertex_index' or 'vertex_indices' required."
        else -> GLU.gluErrorString(errorCode) ?: "Unknown error."
    }


}



// ________________________________________________________________________________________
//
// Constant integer values for OpenGL style error (and warning) codes, and other constants:
// ________________________________________________________________________________________


/** The property map of a MaterialProperties object is empty. */
const val MATERIAL_PROPERTY_WARNING_MAP_EMPTY = -91

/** An unsupported property type was encountered within the property map of a MaterialProperties object. */
const val MATERIAL_PROPERTY_WARNING_UNSUPPORTED_TYPE = -92

/** The material is not initialized properly. */
const val MATERIAL_NOT_INITIALIZED = -93

/** The Mesh object has already been initialized. */
const val MESH_ALREADY_INITIALIZED = -101

/** The size of the vertex array is not suitable. */
const val MESH_INVALID_VERTEX_ARRAY_SIZE = -102

/** The size of the index array is not suitable. */
const val MESH_INVALID_INDEX_ARRAY_SIZE = -103

/** The size of the normal array is not suitable. */
const val MESH_INVALID_NORMAL_ARRAY_SIZE = -104

/** The size of the texture coordinate array is not suitable. */
const val MESH_INVALID_TEXCOORD_ARRAY_SIZE = -105

/** The size of the color array is not suitable. */
const val MESH_INVALID_COLOR_ARRAY_SIZE = -106

/** The buffer handle points to nowhere (is zero). */
const val MESH_ERRONEOUS_BUFFER_HANDLE = -107

/** The ShaderProgram object has already been initialized. */
const val SHADER_ALREADY_INITIALIZED = -201

/** Engine.shaderCompilerAvailable returns false; The shader compiler is not available on this implementation. */
const val SHADER_COMPILER_NOT_AVAILABLE = -202

/** The fragment shader handle points to nowhere.  */
const val SHADER_FS_HANDLE_NULL = -203

/** The vertex shader handle points to nowhere.  */
const val SHADER_VS_HANDLE_NULL = -204

/** The fragment shader code compilation failed, possibly due to an error in the source code. */
const val SHADER_FS_COMPILE_FAILED = -205

/** The vertex shader code compilation failed, possibly due to an error in the source code. */
const val SHADER_VS_COMPILE_FAILED = -206

/** Linking the vertex and fragment shaders to the shader program has failed. */
const val SHADER_PROG_LINK_FAILED = -207

/** The shader program handle points to nowhere. */
const val SHADER_PROG_HANDLE_NULL = -208

/** The Texture object has already been initialized. */
const val TEXTURE_ALREADY_INITIALIZED = -301

/** The desired texture unit is not available on this implementation. */
const val TEXTURE_INVALID_TEXTURE_UNIT = -302

/** The texture dimensions do not conform to the power-of-two rule; for example mipmapping might not be available. */
const val TEXTURE_WARNING_NPOT = -303

/** The texture is uninitialized. */
const val TEXTURE_NOT_INITIALIZED = -304

/** The 'ply' entry, that marks the start of the header, was not found. */
const val PLY_HEADER_START_NOT_FOUND = -401

/** The 'end_header' entry, that marks the end of the header, was not found. */
const val PLY_HEADER_END_NOT_FOUND = -402

/**  The 'format ascii 1.0' entry was not found. */
const val PLY_UNSUPPORTED_FORMAT = -403

/** The count of vertices is erroneous after 'element vertex' entry. */
const val PLY_ELEMENT_VERTEX_COUNT_ERROR = -404

/** The count of faces is erroneous after 'element face' entry. */
const val PLY_ELEMENT_FACE_COUNT_ERROR = -405

/** There is a problem with one of the 'property' entries. */
const val PLY_PROPERTY_PARSE_ERROR = -406

/** Required properties for a mesh are missing. */
const val PLY_PROPERTIES_MISSING = -407

/** Bad/incorrect data type for a property entry. */
const val PLY_PROPERTY_DATA_TYPE_ERROR = -408

/** Non-triangle face encountered, only triangle faces can be used. */
const val PLY_WARNING_FACES_UNTRIANGULATED = -409

/** The required 'vertex_index' or 'vertex_indices' definition of a 'list' entry is missing. */
const val PLY_WARNING_VERTEX_INDEX_LIST_DEFINITION_MISSING = -410



/** Coordinate components per vertex (x, y and z). */
const val MESH_COORDS_PER_VERTEX = 3

/** Texture coordinate components per vertex (s and t / u and v). */
const val MESH_TEX_COORDS_PER_VERTEX = 2

/** Color channels per vertex (r, g, b and a). */
const val MESH_COLOR_CHANNELS_PER_VERTEX = 4

// The size of the vertex coordinate data in bytes. Each coordinate component is a float value and uses four bytes.
//const val MESH_VERTEX_STRIDE = MESH_COORDS_PER_VERTEX * 4


// NOTE: The following use the 'standard' vertex attribute indices, this is second-hand knowledge of Nvidia's old vertex attribute usage.
const val VERTEX_ATTRIBUTE_POSITIONS = 0
const val VERTEX_ATTRIBUTE_TEXTURE_COORDS = 1 // <- I'm assuming this
const val VERTEX_ATTRIBUTE_NORMALS = 2
const val VERTEX_ATTRIBUTE_COLORS = 3
// const val VERTEX_ATTRIBUTE_SECONDARY_COLORS = 4
// const val VERTEX_ATTRIBUTE_FOG_COORDS = 5
// const val VERTEX_ATTRIBUTE_MULTITEXTURE_COORDS0 = 8
// const val VERTEX_ATTRIBUTE_MULTITEXTURE_COORDS1 = 9
// const val VERTEX_ATTRIBUTE_MULTITEXTURE_COORDS2 = 10
// ... etc.
// On ATI's implementation these all are likely to be independent (use whatever index you want)






