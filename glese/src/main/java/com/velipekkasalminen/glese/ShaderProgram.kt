package com.velipekkasalminen.glese


import android.opengl.GLES20



// NOTE: GLSL/ESSL shaders have nothing to do with android.graphics.Shader

/**
 * ShaderProgram contains the handles to the compiled vertex and fragment shaders and to the
 * shader program object living on the gpu.
 * Note that these GLSL/ESSL shaders have nothing to do with [android.graphics.Shader]
 *
 */
class ShaderProgram(val name: String): IDeletable {

    // have both shaders been compiled, is the program ready to be used?
    private var _shaderInitialized: Boolean = false

    // shader program object handle:
    @GLESIntHandle private var _spHandle: Int = 0
    // vertex shader handle:
    @GLESIntHandle private var _vsHandle: Int = 0
    // fragment shader handle:
    @GLESIntHandle private var _fsHandle: Int = 0


    /** True, if the shader program has been initialized and is ready to be used. */
    override val isInitialized: Boolean get() = _shaderInitialized

    @GLESIntHandle val fragmentShaderHandle: Int get() = _fsHandle
    @GLESIntHandle val vertexShaderHandle: Int get() = _vsHandle
    @GLESIntHandle val programHandle: Int get() = _spHandle


    /**
     * Constructs a shader program from pre-existing vertex and fragment shader handles.
     * WARNING: if these handles are shared across multiple shader programs, do not
     * call [delete] unless you are sure that the shaders are no longer in use.
     *
     * @param[vertexShader] Handle to the vertex shader.
     * @param[fragmentShader] Handle to the fragment shader.
     * @param[attributes] A map with a vertex attribute location index as the key and the name of the attribute as value.
     *
     * @return Zero on success, otherwise returns one of the SHADER_* error codes or GL_* error codes.
     */
    fun construct(@GLESIntHandle vertexShader: Int, @GLESIntHandle fragmentShader: Int, attributes: Map<Int, String>? = null): Int {
        if (!Engine.shaderCompilerAvailable) return SHADER_COMPILER_NOT_AVAILABLE
        if (_shaderInitialized) return SHADER_ALREADY_INITIALIZED

        if (vertexShader != 0) _vsHandle = vertexShader
        else return SHADER_VS_HANDLE_NULL

        if (fragmentShader != 0) _fsHandle = fragmentShader
        else return SHADER_FS_HANDLE_NULL

        // TODO: maybe use glIsShader to check the vertexShader and fragmentShader

        // Create a (shader) program object:
        _spHandle = GLES20.glCreateProgram()

        if (_spHandle == 0) return SHADER_PROG_HANDLE_NULL

        // attach the shaders to the program:
        GLES20.glAttachShader(_spHandle, _vsHandle)
        GLES20.glAttachShader(_spHandle, _fsHandle)

        if (attributes != null) {
            // bind the (vertex) attributes, this must be done before linking.
            // attribute format: <locationIndex, attributeName>
            val attrList = attributes.toList()
            for (i in attrList.indices) {
                //     bind attribute to this program,  at location index,  attribute name string
                GLES20.glBindAttribLocation(_spHandle, attrList[i].first, attrList[i].second)
            }
            // TODO: Check for vertex attribute errors
            // (bad indices or non-existing names)
            // NOTE: attribute errors won't stop us from using the shader program
        }

        var status = intArrayOf(0)

        // finally link the program:
        GLES20.glLinkProgram(_spHandle)

        // was the linking successful?
        GLES20.glGetProgramiv(_spHandle, GLES20.GL_LINK_STATUS, status, 0)

        if (status[0] == GLES20.GL_FALSE) {
            // linking failed, delete the program
            GLES20.glDeleteProgram(_spHandle)
            _spHandle = 0
            return SHADER_PROG_LINK_FAILED
        }

        // TODO: use GLES20.glValidateProgram() here?

        _shaderInitialized = true

        // the program is now 'initialized' but we may still return an error code:
        val glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError

        return 0
    }

    /**
     * Constructs a shader program from shader sources.
     *
     * @param[vertexShaderCode] Source code for the vertex shader.
     * @param[fragmentShaderCode] Source code for the fragment shader.
     * @param[attributes] A map with a vertex attribute location index as the key and the name of the attribute as value.
     *
     * @return Zero on success, otherwise returns one of the SHADER_* error codes or GL_* error codes.
     */
    fun construct(vertexShaderCode: String, fragmentShaderCode: String, attributes: Map<Int, String>? = null): Int {
        if (!Engine.shaderCompilerAvailable) return SHADER_COMPILER_NOT_AVAILABLE
        if (_shaderInitialized) return SHADER_ALREADY_INITIALIZED

        // Create a shader object handle for the vertex shader:
        _vsHandle = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER)

        if (_vsHandle == 0) return SHADER_VS_HANDLE_NULL // <-- error: we have a null shader object!

        var status = intArrayOf(0)

        // set the vertex shader code and compile:
        GLES20.glShaderSource(_vsHandle, vertexShaderCode)
        GLES20.glCompileShader(_vsHandle)

        GLES20.glGetShaderiv(_vsHandle, GLES20.GL_COMPILE_STATUS, status, 0)

        if (status[0] == GLES20.GL_FALSE) {
            // compilation failed, delete the shader:
            GLES20.glDeleteShader(_vsHandle)
            _vsHandle = 0
            return SHADER_VS_COMPILE_FAILED
        }

        // Create a shader object handle for the fragment shader:
        _fsHandle = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER)

        if (_fsHandle == 0) {
            // error: we have a null shader object!
            // Now we must kill the perfectly good vertex shader :(
            GLES20.glDeleteShader(_vsHandle)
            _vsHandle = 0
            return SHADER_FS_HANDLE_NULL
        }

        // set the fragment shader code and compile:
        GLES20.glShaderSource(_fsHandle, fragmentShaderCode)
        GLES20.glCompileShader(_fsHandle)

        GLES20.glGetShaderiv(_fsHandle, GLES20.GL_COMPILE_STATUS, status, 0)

        if (status[0] == GLES20.GL_FALSE) {
            // compilation failed, delete both shaders :(

            delete()
            return SHADER_FS_COMPILE_FAILED
        }

        // Create a (shader) program object:
        _spHandle = GLES20.glCreateProgram()

        if (_spHandle == 0) {
            // error: we have a null program handle!
            // delete both shaders :(

            delete()
            return SHADER_PROG_HANDLE_NULL
        }

        // attach the shaders to the program:
        GLES20.glAttachShader(_spHandle, _vsHandle)
        GLES20.glAttachShader(_spHandle, _fsHandle)

        if (attributes != null) {
            // bind the (vertex) attributes, this must be done before linking.
            // attribute format: <locationIndex, attributeName>
            val attrList = attributes.toList()
            for (i in attrList.indices) {
                //     bind attribute to this program,  at location index,  attribute name string
                GLES20.glBindAttribLocation(_spHandle, attrList[i].first, attrList[i].second)
            }
            // TODO: Check for vertex attribute errors
            // (bad indices or non-existing names)
            // NOTE: attribute errors won't stop us from using the shader program
        }

        // finally link the program:
        GLES20.glLinkProgram(_spHandle)

        // was the linking successful?
        GLES20.glGetProgramiv(_spHandle, GLES20.GL_LINK_STATUS, status, 0)

        if (status[0] == GLES20.GL_FALSE) {
            // linking failed, delete EVERYTHING :(

            delete()
            return SHADER_PROG_LINK_FAILED
        }

        // TODO: use GLES20.glValidateProgram() here?

        _shaderInitialized = true

        // the shader is now 'initialized' but we may still return an error code:
        val glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError

        // no errors? awesome!
        return 0
    }

    /**
     * Sets this as the active shader program for rendering,
     * assuming that the program is initialized.
     */
    fun setActive() = GLES20.glUseProgram(_spHandle)

    /** Deletes the program object itself and the attached shaders. */
    override fun delete() {
        if (_spHandle != 0) GLES20.glDeleteProgram(_spHandle)
        if (_vsHandle != 0) GLES20.glDeleteShader(_vsHandle)
        if (_fsHandle != 0) GLES20.glDeleteShader(_fsHandle)
        _vsHandle = 0
        _fsHandle = 0
        _spHandle = 0
        _shaderInitialized = false
    }


    override fun toString(): String = "${name}(fragmentShader:${_fsHandle!=0}, vertexShader:${_vsHandle!=0})"

}

