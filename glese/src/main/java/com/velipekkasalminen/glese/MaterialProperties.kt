package com.velipekkasalminen.glese


import android.opengl.GLES20
import android.renderscript.Float2
import android.renderscript.Float3
import android.renderscript.Float4
import com.velipekkasalminen.glese.utils.Tuple3
import com.velipekkasalminen.glese.utils.Tuple4
import org.jetbrains.annotations.NotNull



/**
 * The material property types for [MaterialProperties]
 */
enum class MaterialPropertyType {
    NULL, COLORRGBA, FLOAT, FLOAT2, FLOAT3, FLOAT4, TUPLE3, TUPLE4, VECTOR4, TEXTURE2D, TEXTURECUBEMAP, UNSUPPORTED
}


/**
 * MaterialProperties object holds the desired mutable properties and attributes of an individualized
 * attribute map for renderable objects.
 */
class MaterialProperties(shader: ShaderProgram) {

    /*
     * UniformBindPoint is a single bind point for an attribute/property of a Material,
     * accessed via an integer uniform location handle.
     */
    private data class UniformBindPoint(@GLESIntHandle val uniformLocation: Int, val value: Any)

    // we need the shader program to check the uniforms:
    private val _shader: ShaderProgram = shader

    // attribute map of (nameHash, UniformBindPoint(uniformLocation, value))
    private var _attr = mutableMapOf<Int, @NotNull UniformBindPoint>()

    /**
     * The count of properties contained within the property map.
     */
    val count get() = _attr.count()

    fun clear() = _attr.clear()


    constructor(mp: MaterialProperties) : this(mp._shader) {
        // deep copy map data:
        // NOTE: would  _attr.putAll(mp._attr)  deep copy the map?
        mp._attr.forEach { (i, ubp) -> _attr[i] = UniformBindPoint(ubp.uniformLocation, ubp.value) }
    }


    // MaterialAttribute.value allowed as:
    // ColorRGBA
    // Vector4 (includes Quaternion)
    // Matrix4x4
    // Texture (Texture2D and TextureCubemap)
    // Float
    // Float2
    // Float3
    // Float4
    // FloatArray
    // NOTE: ColorRGBA and Vector4 classes can be converted to Float4 and
    //       Matrix4x4 can be converted to FloatArray(16) -type.

    // Accepted GLSL types -> to Kotlin types:
    // float, int, bool    ->   Float, Int, Boolean
    // vec2, vec3, vec4    ->   Float2, Float3, Float4 OR Vector4 OR ColorRGBA
    // mat2, mat3, mat4    ->   FloatArray(4), FloatArray(9), FloatArray(16) OR Matrix4x4
    // sampler2D           ->   Texture2D
    // samplerCube         ->   TextureCubemap

    /**
     * Adds a new attribute that can be manipulated.
     *
     * @param[name] The name of the new property, this can be anything except blank.
     * @param[uniformName] The name of the uniform on the shader program that the property name reflects.
     * @param[value] The value for the property. You must verify yourself that the data type is compatible with the GLSL data type of the shader uniform.
     * @return True if the new attribute was successfully added, false otherwise.
     */
    fun add(name: String, uniformName: String, value: Any): Boolean {
        when {
            name.isBlank() -> return false
            !_shader.isInitialized -> return false
            !supports(value) -> return false
        }

        val h = name.hashCode()
        if (_attr.containsKey(h)) return false

        // check the uniform location:
        val loc = GLES20.glGetUniformLocation(_shader.programHandle, uniformName)

        // does the uniform name exist on the shader?
        if (loc == -1) return false

        _attr[h] = UniformBindPoint(loc, value)

        return true
    }

    /**
     * Returns true if the property [name] exists.
     */
    operator fun contains(name: String) = _attr.containsKey(name.hashCode())

    /**
     * Returns the type of the given property as [MaterialPropertyType] enum.
     * [MaterialPropertyType.NULL] is returned if the property [name] does not exist,
     * [MaterialPropertyType.UNSUPPORTED] is returned if the property type is not supported.
     *
     */
    fun getTypeEnum(name: String): MaterialPropertyType {
        val h = name.hashCode()
        if (_attr.containsKey(h)){
            val attrval = _attr[h]!!
            when (attrval.value) {
                is ColorRGBA -> return MaterialPropertyType.COLORRGBA
                is Float -> return MaterialPropertyType.FLOAT
                is Float2 -> return MaterialPropertyType.FLOAT2
                is Float3 -> return MaterialPropertyType.FLOAT3
                // Tuple3 has been already checked to contain only floats (see: supports -function)
                is Tuple3<*, *, *> ->  return MaterialPropertyType.TUPLE3
                is Float4 -> return MaterialPropertyType.FLOAT4
                // Tuple4 has been already checked to contain only floats (see: supports -function)
                is Tuple4<*, *, *, *> -> return MaterialPropertyType.TUPLE4
                is Vector4 -> return MaterialPropertyType.VECTOR4
                is Texture2D -> return MaterialPropertyType.TEXTURE2D
                is TextureCubemap -> return MaterialPropertyType.TEXTURECUBEMAP
                // TODO: add more property types as they are implemented
            }
            // type is not found within the above list of acceptable types:
            return MaterialPropertyType.UNSUPPORTED
        }
        // the name does not exist:
        return MaterialPropertyType.NULL
    }

    /**
     * Sets a new value for an existing property.
     *
     * @param[name] The name of the existing property, created with the [add] -method.
     * @param[value] The new value for the attribute.
     * @return True if the new [value] was set, false if the [name] does not exist or the type of the new [value] is invalid.
     */
    fun setValue(name: String, value: Any): Boolean {
        val h = name.hashCode()

        // does the name exist?
        if (!_attr.containsKey(h)) return false

        // value and the _attr[h].value must have the same type:
        val attrval = _attr[h]!!
        if (value::class != attrval.value::class) return false

        // set the value:
        _attr[h] = UniformBindPoint(attrval.uniformLocation, value)
        return true
    }

    operator fun set(name: String, value: Any) =
        when(setValue(name, value)) {
            true -> { /* Unit */ }
            false -> throw IllegalArgumentException("Invalid use of indexed access: $name does not exist or type is invalid.")
        }

    /**
     * Returns the value of the property (as Any? -type), or
     * null if the property [name] does not exist.
     */
    fun getValue(name: String): Any? {
        val h = name.hashCode()
        if (!_attr.containsKey(h)) return null
        return _attr[h]!!.value
    }

    operator fun get(name: String): Any =
        getValue(name) ?: throw IllegalArgumentException("Invalid use of indexed access: $name does not exist.")


    /**
     * Sets uniform data for the currently used shader, this is done before rendering with the material.
     * @return Zero on success, otherwise one of the MATERIAL_PROPERTY_WARNING_* codes or one of the GL_* error codes is returned.
     */
    fun sendUniformDataToShader(): Int {
        if (_attr.isEmpty()) return MATERIAL_PROPERTY_WARNING_MAP_EMPTY

        // NOTE: _shader.setActive() is assumed to have been called (done with the Material)

        var err = 0

        _attr.forEach {
            val a = it.value.value
            when (a) {
                is Float -> GLES20.glUniform1f(it.value.uniformLocation, a)
                is Float2 -> GLES20.glUniform2f(it.value.uniformLocation, a.x, a.y)
                is Float3 -> GLES20.glUniform3f(it.value.uniformLocation, a.x, a.y, a.z)
                // Tuple3 has been already checked to contain only floats (see: supports -function)
                is Tuple3<*, *, *> -> GLES20.glUniform3f(it.value.uniformLocation, a.t1 as Float, a.t2 as Float, a.t3 as Float)
                is Float4 -> GLES20.glUniform4f(it.value.uniformLocation, a.x, a.y, a.z, a.w)
                // Tuple4 has been already checked to contain only floats (see: supports -function)
                is Tuple4<*, *, *, *> -> GLES20.glUniform4f(it.value.uniformLocation, a.t1 as Float, a.t2 as Float, a.t3 as Float, a.t4 as Float)
                is Vector4 -> GLES20.glUniform4f(it.value.uniformLocation, a.x, a.y, a.z, a.w)
                is ColorRGBA -> GLES20.glUniform4f(it.value.uniformLocation, a.r, a.g, a.b, a.a)
                is Texture2D -> {
                    GLES20.glActiveTexture(a.textureUnit)
                    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, a.textureHandle)
                    GLES20.glUniform1i(it.value.uniformLocation, a.textureUniform)
                }
                is TextureCubemap -> {
                    GLES20.glActiveTexture(a.textureUnit)
                    GLES20.glBindTexture(GLES20.GL_TEXTURE_CUBE_MAP, a.textureHandle)
                    GLES20.glUniform1i(it.value.uniformLocation, a.textureUniform)
                }

                // TODO: more uniforms for Matrix4x4, FloatArrays, etc.

                // unsupported type found (this should never happen):
                else -> err = MATERIAL_PROPERTY_WARNING_UNSUPPORTED_TYPE
            }
        }

        val glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError
        return err
    }

    override fun toString(): String = "MaterialProperties(propertyMap:${_attr})"

    companion object{

        /**
         * Test whether the type of the [obj] is supported for the material properties or not.
         */
        fun supports(obj: Any): Boolean {
            when (obj) {
                is ColorRGBA -> return true
                is Float -> return true
                is Float2 -> return true
                is Float3 -> return true
                is Tuple3<*, *, *> -> if (obj.t1 is Float && obj.t2 is Float && obj.t3 is Float) return true
                is Float4 -> return true
                is Tuple4<*, *, *, *> -> if (obj.t1 is Float && obj.t2 is Float && obj.t3 is Float && obj.t4 is Float) return true
                is Vector4 -> return true
                is Texture2D -> return true
                is TextureCubemap -> return true
                // TODO: add more property types as they are implemented
            }
            return false
        }
    }

}
