package com.velipekkasalminen.glese


/**
 * Model3D consists of a mesh and a material,
 * both of which can be shared across multiple Model3D objects.
 * Model3D is mainly used by [Object3D]
 */
class Model3D(val name: String) {

    /** The shared Mesh object across models. */
    var sharedMesh: Mesh? = null

    /** The shared Material object across models. */
    var sharedMaterial: Material? = null

    /** Transformation matrix. */
    var matrix = Matrix4x4()


    /**
     * Sets the rotation and position offset of the model.
     *
     */
    fun setTransformations(rotation: Quaternion, position: Position) {
        matrix.setIdentity()
        matrix.setRotation(rotation)
        matrix.translate(position)
    }


    fun onAboutToRender(customizedMaterialProperties: MaterialProperties? = null) {
        sharedMaterial?.onAboutToDraw(customizedMaterialProperties)
    }

    fun render() {
        // NOTE: a mesh and a material are needed to render.

        // do not put the following to the onAboutToRender method:
        sharedMesh?.onAboutToRender()

        // we could put fancy stuff here, between sharedMesh.onAboutToRender and sharedMesh.render...

        sharedMesh?.render()
    }

    override fun toString(): String = "${name}(mesh:${sharedMesh?.name}, material:${sharedMaterial?.name})"

}

