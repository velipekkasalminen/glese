package com.velipekkasalminen.glese


import android.opengl.Matrix.*


/**
 * Matrix4x4 is a 4 by 4 square matrix with 16 float elements in column-major order.
 * Matrices are used to represent transformations, mostly translations and rotations, of 3D objects.
 * The first three row vectors usually represent transformations such as rotation and scaling, and
 * the last row vector is usually used to represent a position translation.<br />
 * NOTE: Matrix4x4 is not the same as [android.graphics.Matrix], which is used to transform 2D points.
 */
class Matrix4x4 {

    // NOTE: Matrices are 4 x 4 column-vector matrices stored in column-major order.
    // For more info, see:  https://developer.android.com/reference/android/opengl/Matrix
    private var backingArray = FloatArray(16)

    // Basically this class is just a wrapper for the static OpenGL Matrix functions

    val array get() = backingArray

    /**
     * Gets the matrix element, note that
     * [column] and [row] index range from 0 to 3.<br />
     * TIP: you can use the indexing operator:
     * matrix[column, row]
     */
    operator fun get(column: Int, row: Int): Float {
        return backingArray[4 * column + row]
    }

    /**
     * Sets the matrix element to value [n], note that
     * [column] and [row] index range from 0 to 3.<br />
     * TIP: you can use the indexing operator:
     * matrix[column, row] = n
     */
    operator fun set(column: Int, row: Int, n: Float) {
        backingArray[4 * column + row] = n
    }

    /**
     * Gets the (diagonal) trace of the matrix.
     * An identity matrix has a trace of 4.0
     */
    val trace get() = backingArray[0] + backingArray[5] + backingArray[10] + backingArray[15]

    /**
     * First column vector
     */
    var column1: Vector4
        get() { return Vector4(backingArray[0], backingArray[1], backingArray[2], backingArray[3]) }
        set(value) {
            backingArray[0] = value.x
            backingArray[1] = value.y
            backingArray[2] = value.z
            backingArray[3] = value.w
        }

    /**
     * Second column vector
     */
    var column2: Vector4
        get() { return Vector4(backingArray[4], backingArray[5], backingArray[6], backingArray[7]) }
        set(value) {
            backingArray[4] = value.x
            backingArray[5] = value.y
            backingArray[6] = value.z
            backingArray[7] = value.w
        }

    /**
     * Third column vector
     */
    var column3: Vector4
        get() { return Vector4(backingArray[8], backingArray[9], backingArray[10], backingArray[11]) }
        set(value) {
            backingArray[8] = value.x
            backingArray[9] = value.y
            backingArray[10] = value.z
            backingArray[11] = value.w
        }

    /**
     * Fourth column vector
     */
    var column4: Vector4
        get() { return Vector4(backingArray[12], backingArray[13], backingArray[14], backingArray[15]) }
        set(value) {
            backingArray[12] = value.x
            backingArray[13] = value.y
            backingArray[14] = value.z
            backingArray[15] = value.w
        }

    /**
     * First row vector, usually represents the
     * transformations of the X axis.
     */
    var row1: Vector4
        get() { return Vector4(backingArray[0], backingArray[4], backingArray[8], backingArray[12]) }
        set(value) {
            backingArray[0] = value.x
            backingArray[4] = value.y
            backingArray[8] = value.z
            backingArray[12] = value.w
        }

    /**
     * Second row vector, usually represents the
     * transformations of the Y axis.
     */
    var row2: Vector4
        get() { return Vector4(backingArray[1], backingArray[5], backingArray[9], backingArray[13]) }
        set(value) {
            backingArray[1] = value.x
            backingArray[5] = value.y
            backingArray[9] = value.z
            backingArray[13] = value.w
        }

    /**
     * Third row vector, usually represents the
     * transformations of the Z axis.
     */
    var row3: Vector4
        get() { return Vector4(backingArray[2], backingArray[6], backingArray[10], backingArray[14]) }
        set(value) {
            backingArray[2] = value.x
            backingArray[6] = value.y
            backingArray[10] = value.z
            backingArray[14] = value.w
        }

    /**
     * Fourth row vector, usually stores the
     * translation (=position)
     */
    var row4: Vector4
        get() { return Vector4(backingArray[3], backingArray[7], backingArray[11], backingArray[15]) }
        set(value) {
            backingArray[3] = value.x
            backingArray[7] = value.y
            backingArray[11] = value.z
            backingArray[15] = value.w
        }

    /**
     * Creates a new Matrix4x4 and sets it as an identity matrix.
     */
    constructor() {
        // setup an identity matrix:
        setIdentityM(backingArray, 0)
    }

    /**
     * Creates a new Matrix4x4 instance by copying values from another matrix.
     */
    constructor(m: Matrix4x4) {
        // copy backingArray from another matrix:
        m.backingArray.copyInto(this.backingArray)
    }

    /**
     * Copies the data from another matrix into this matrix.
     */
    fun copyFrom(m: Matrix4x4) {
        // copy backingArray from another matrix:
        m.backingArray.copyInto(this.backingArray)
    }

    /**
     * Sets this matrix to an identity matrix.
     */
    fun setIdentity() {
        setIdentityM(backingArray, 0)
    }

    /**
     * Sets this matrix as a projection matrix in terms of six clip planes.
     */
    fun setProjection(left: Float, right: Float, bottom: Float, top: Float, near: Float, far: Float) {
        frustumM(backingArray, 0, left, right, bottom, top, near, far)
    }

    /**
     * Sets this matrix as a projection matrix by using field of view [fov] angle
     * in degrees, [aspect] ratio of the viewport, and the [near] and [far] clip planes.
     *
     */
    fun setProjection(fov: Float, aspect: Float, near: Float, far: Float) {
        perspectiveM(backingArray, 0, fov, aspect, near, far)
    }

    /**
     * Sets this matrix as an orthographic projection matrix in terms of six clip planes.
     */
    fun setOrthographic(left: Float, right: Float, bottom: Float, top: Float, near: Float, far: Float) {
        orthoM(backingArray, 0, left, right, bottom, top, near, far)
    }

    /**
     * Inverts this matrix.
     * Returns true if the matrix is inverted, false otherwise.
     *
     */
    fun invert(): Boolean {
        // 'mInv' and 'm' must not overlap, thus we make a copy:
        backingArray.copyInto(farr1)
        return invertM(backingArray, 0, farr1, 0)
    }

    /**
     * Transposes this matrix.
     *
     */
    fun transpose() {
        // 'mTrans' and 'm' must not overlap, thus we make a copy:
        backingArray.copyInto(farr1)
        transposeM(backingArray, 0, farr1, 0)
    }

    /**
     * Translates this matrix by using cartesian coordinates [x], [y] and [z].
     *
     */
    fun translate(x: Float, y: Float, z: Float) {
        // Do not use: translateM(backingArray, 0, x, y, z)

        setIdentityM(farr2, 0)
        translateM(farr2, 0, x, y, z)

        backingArray.copyInto(farr1)
        multiplyMM(backingArray, 0, farr1, 0, farr2, 0)

        // Create translation martix from vector(x,y,z), then
        // multiply this matrix with the traslation matrix,
        // the result is effectively:
        /*backingArray[0] += backingArray[12] * x
        backingArray[1] += backingArray[13] * x
        backingArray[2] += backingArray[14] * x
        backingArray[3] += backingArray[15] * x
        backingArray[4] += backingArray[12] * y
        backingArray[5] += backingArray[13] * y
        backingArray[6] += backingArray[14] * y
        backingArray[7] += backingArray[15] * y
        backingArray[8] += backingArray[12] * z
        backingArray[9] += backingArray[13] * z
        backingArray[10] += backingArray[14] * z
        backingArray[11] += backingArray[15] * z*/
    }

    /**
     * Translates this matrix by using vector [v] in the cartesian coordinate
     * system.
     */
    fun translate(v: Vector4) = translate(v.x, v.y, v.z)


    /**
     * Scales this matrix on every axis individually.
     */
    fun scale(x: Float, y: Float, z: Float) {
        setIdentityM(farr2, 0)
        scaleM(farr2, 0, x, y, z)
        backingArray.copyInto(farr1)
        multiplyMM(backingArray, 0, farr1, 0, farr2, 0)

        /*backingArray[0] *= x
        backingArray[1] *= x
        backingArray[2] *= x
        backingArray[3] *= x
        backingArray[4] *= y
        backingArray[5] *= y
        backingArray[6] *= y
        backingArray[7] *= y
        backingArray[8] *= z
        backingArray[9] *= z
        backingArray[10] *= z
        backingArray[11] *= z*/
        //backingArray[12] *= 1
        //backingArray[13] *= 1
        //backingArray[14] *= 1
        //backingArray[15] *= 1
    }

    /**
     * Scales this matrix by a cartesian vector,
     * the w component of the vector is ignored.
     */
    fun scale(v: Vector4) = scale(v.x, v.y, v.z)

    /**
     * Multiplies this matrix with another matrix.
     */
    fun multiply(m: Matrix4x4) {
        // matrix backingArrays must not overlap, thus we make a copy:
        backingArray.copyInto(farr1)
        multiplyMM(backingArray, 0, farr1, 0, m.backingArray, 0)
    }


    /**
     * Applies a rotation to this matrix.
     */
    fun rotate(q: Quaternion) {
        backingArray.copyInto(farr2)
        farr1[0] = q.w*q.w+q.x*q.x-q.y*q.y-q.z*q.z
        farr1[1] = 2f*q.x*q.y+2f*q.w*q.z
        farr1[2] = 2f*q.x*q.z-2f*q.w*q.y
        farr1[3] = 0f
        farr1[4] = 2f*q.x*q.y-2f*q.w*q.z
        farr1[5] = q.w*q.w-q.x*q.x+q.y*q.y-q.z*q.z
        farr1[6] = 2f*q.y*q.z+2f*q.w*q.x
        farr1[7] = 0f
        farr1[8] = 2f*q.x*q.z+2f*q.w*q.y
        farr1[9] = 2f*q.y*q.z-2f*q.w*q.x
        farr1[10]= q.w*q.w-q.x*q.x-q.y*q.y+q.z*q.z
        farr1[11] = 0f
        farr1[12] = 0f
        farr1[13] = 0f
        farr1[14] = 0f
        farr1[15] = 0f
        multiplyMM(backingArray, 0, farr2, 0, farr1, 0)
    }

    /**
     * Sets this matrix to represent a rotation, note that the
     * beginning 3x3 portion of the matrix is overridden.
     *
     */
    fun setRotation(q: Quaternion) {
        // sets the 3x3 portion like so:
        // (q.w*q.w+q.x*q.x-q.y*q.y-q.z*q.z) , (2*q.x*q.y-2*q.w*q.z) , (2*q.x*q.z+2*q.w*q.y)
        // (2*q.x*q.y+2*q.w*q.z) , (q.w*q.w-q.x*q.x+q.y*q.y-q.z*q.z) , (2*q.y*q.z-2*q.w*q.x)
        // (2*q.x*q.z-2*q.w*q.y) , (2*q.y*q.z+2*q.w*q.x) , (q.w*q.w-q.x*q.x-q.y*q.y+q.z*q.z)

        backingArray[0] = q.w*q.w+q.x*q.x-q.y*q.y-q.z*q.z
        backingArray[1] = 2f*q.x*q.y+2f*q.w*q.z
        backingArray[2] = 2f*q.x*q.z-2f*q.w*q.y
        backingArray[4] = 2f*q.x*q.y-2f*q.w*q.z
        backingArray[5] = q.w*q.w-q.x*q.x+q.y*q.y-q.z*q.z
        backingArray[6] = 2f*q.y*q.z+2f*q.w*q.x
        backingArray[8] = 2f*q.x*q.z+2f*q.w*q.y
        backingArray[9] = 2f*q.y*q.z-2f*q.w*q.x
        backingArray[10]= q.w*q.w-q.x*q.x-q.y*q.y+q.z*q.z
    }


    fun setLookAt(position: Vector4, target: Vector4) {
        setLookAtM(backingArray, 0,
            position.x, position.y, position.z,
            target.x, target.y, target.z,
            0f, 1f, 0f)
    }

    fun setLookAt(position: Vector4, target: Vector4, up: Vector4) {
        setLookAtM(backingArray, 0,
            position.x, position.y, position.z,
            target.x, target.y, target.z,
            up.x, up.y, up.z)
    }

    operator fun times(v: Vector4) = multiply(this, v)

    companion object {

        /**
         * Multiplies a Matrix4x4 by a Vector4.
         * A new Vector4 object is returned as the result.
         */
        fun multiply(m: Matrix4x4, v: Vector4): Vector4 {
            var arr = FloatArray(4)
            multiplyMV(arr, 0, m.backingArray, 0, v.toFloatArray(), 0)
            return Vector4(arr[0], arr[1], arr[2], arr[3])
        }


        // some helper arrays:
        private var farr1 = FloatArray(16)
        private var farr2 = FloatArray(16)

    }

}


