package com.velipekkasalminen.glese


import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*


/** The possible data types in a (standard) Stanford triangle/polygon file (.PLY), excluding 'VOID' */
enum class PlyVertexAttributeDataType {
    VOID, CHAR, UCHAR, SHORT, USHORT, INT, UINT, FLOAT, DOUBLE
}

/** The possible attribute names in a (standard) Stanford triangle/polygon file (.PLY), excluding 'NONE' */
enum class PlyVertexAttributeDataName {
    NONE, X, Y, Z, NX, NY, NZ, U, V, S, T, RED, GREEN, BLUE, ALPHA
}

// NOTE: the alpha component is not within the 'standards' (?)
//       of the PLY -format but supported by Blender and others.
//       Also, the U and V coordinates are not standard PLY, but let's support them anyway.

/** Describes a vertex attribute in a PLY file. */
data class PlyVertexAttribute(val dataType: PlyVertexAttributeDataType = PlyVertexAttributeDataType.VOID,
                              val dataName: PlyVertexAttributeDataName = PlyVertexAttributeDataName.NONE)



/**
 * VertexData is used to create a [Mesh]. After the mesh has been created, the vertex data object
 * can be garbage collected.
 */
class VertexData(vertexCount: Int = 3, triangleCount: Int = 1) {
    // NOTE: Using the VertexData() -constructor will create a 'dummy'
    //       vertex data object, so that the loadFromPly -function can be used.

    // NOTE: To construct a Mesh, we require at least a vertex array and an index array.

    // private fields:

    private var _vertexArr: FloatArray = FloatArray(vertexCount * MESH_COORDS_PER_VERTEX)
    private var _indexArr: IntArray = IntArray(triangleCount * 3)
    private var _vertices: Int = vertexCount
    private var _triangles: Int = triangleCount
    private var _vertexArrIsSet = false
    private var _indexArrIsSet = false

    // All of the following arrays are optional:
    private var _normalArr: FloatArray? = null
    private var _mainTexCoordArr: FloatArray? = null
    //private var _ext1TexCoordArr: FloatArray? = null
    //private var _ext2TexCoordArr: FloatArray? = null
    private var _mainColorArr: FloatArray? = null
    //private var _ext1ColorArr: FloatArray? = null
    //private var _ext2ColorArr: FloatArray? = null

    // TODO: tangentArray: FloatArray? = null
    // TODO: binormalArray: FloatArray? = null

    // TODO: calculateTangents function
    // TODO: calculateBinormals function



    // public fields:

    /** True if this vertex data object can be used to construct a [Mesh] */
    val isReady: Boolean get() = _vertexArrIsSet && _indexArrIsSet && _vertices > 2 && _triangles > 0

    /** The count of allocated vertices. */
    val vertices: Int get() = _vertices

    /** The count of allocated triangles (vertex index triplets). */
    val triangles: Int get() = _triangles

    /** An array of x, y and z position coordinates. */
    val vertexArray: FloatArray get() = _vertexArr

    /** An array of vertex indices of triangles. */
    val indexArray: IntArray get() = _indexArr

    /** An array of vertex normals with x, y and z coordinates. */
    val normalArray: FloatArray? get() = _normalArr

    /** An array of texture coordinates (UV or ST) of vertices. */
    val textureCoordinateArray: FloatArray? get() = _mainTexCoordArr

    /** An array of colors (r, g, b and a) of vertices. */
    val colorArray: FloatArray? get() = _mainColorArr

    /**
     * Sets the position data for the vertices. Every vertex has three position coordinates (x, y and z),
     * thus the [verts] array size must be three times the vertex count.
     *
     * @return True when successful, false if the array did not have the correct size.
     */
    fun setVertices(verts: FloatArray): Boolean {
        if (verts.size != MESH_COORDS_PER_VERTEX * _vertices) return false
        verts.copyInto(_vertexArr)
        _vertexArrIsSet = true
        return true
    }

    /**
     * Sets the vertex index data for the triangles. Every triangle has three vertices, thus
     * the [indices] array size must be three times the triangle count.
     *
     * @return True when successful, false if the array did not have the correct size.
     */
    fun setIndices(indices: IntArray): Boolean {
        if (indices.size != 3 * _triangles) return false
        indices.copyInto(_indexArr)
        _indexArrIsSet = true
        return true
    }

    /**
     * Sets the data for the vertex normals. Every vertex normal has three coordinates (x, y and z),
     * thus the [normals] array size must be three times the vertex count.
     *
     * @return True when successful, false if the array did not have the correct size.
     */
    fun setNormals(normals: FloatArray): Boolean {
        if (normals.size != MESH_COORDS_PER_VERTEX * _vertices) return false
        if (_normalArr == null) {
            _normalArr = FloatArray(MESH_COORDS_PER_VERTEX * _vertices)
        }
        normals.copyInto(_normalArr!!)
        return true
    }

    /**
     * Sets the (primary) texture coordinate data for the vertices. Every vertex has two texture coordinates
     * (u and v OR s and t), thus the [texCoords] array size must be two times the vertex count.
     *
     * @return True when successful, false if the array did not have the correct size.
     */
    fun setTextureCoordinates(texCoords: FloatArray): Boolean {
        if (texCoords.size != MESH_TEX_COORDS_PER_VERTEX * _vertices) return false
        if (_mainTexCoordArr == null) {
            _mainTexCoordArr = FloatArray(MESH_TEX_COORDS_PER_VERTEX * _vertices)
        }
        texCoords.copyInto(_mainTexCoordArr!!)
        return true
    }

    /**
     * Converts the (primary) texture coordinates from UV to ST and vice versa.
     * Call this function if the texture appears to be 'upside down'
     */
    fun convertTextureCoordinates() {
        if (_mainTexCoordArr == null) return
        val s: Int = _mainTexCoordArr!!.size
        var i = 1
        while (i < s) {
            _mainTexCoordArr!![i] = 1f - _mainTexCoordArr!![i]
            i += 2
        }
    }


    /**
     * Sets the primary color data for the vertices. Every vertex color has four channels
     * (r, g, b and a), thus the [colors] array size must be four times the vertex count.
     *
     * @return True when successful, false if the array did not have the correct size.
     */
    fun setVertexColors(colors: FloatArray): Boolean {
        if (colors.size != MESH_COLOR_CHANNELS_PER_VERTEX * _vertices) return false
        if (_mainColorArr == null) {
            _mainColorArr = FloatArray(MESH_COLOR_CHANNELS_PER_VERTEX * _vertices)
        }
        colors.copyInto(_mainColorArr!!)
        return true
    }

    /**
     * Changes all of the vertex colors to [newColor] value.
     * False is returned if the [colorArray] does not exist.
     */
    fun changeVertexColors(newColor: ColorRGBA): Boolean {
        if (_mainColorArr == null) return false
        val size = _mainColorArr!!.count()
        var i = 0
        while (i < size) {
            _mainColorArr!![i]   = newColor.r
            _mainColorArr!![i+1] = newColor.g
            _mainColorArr!![i+2] = newColor.b
            _mainColorArr!![i+3] = newColor.a
            i += 4
        }
        return true
    }



    // inputStream is from your asset file or something
    fun loadFromPly(inputStream: InputStream): Int {

        // PLY "format ascii 1.0" assumes ANSI/ASCII character set, but we just use utf-8:
        val buff = BufferedReader( InputStreamReader(inputStream, "UTF-8") )

        // The first line must be "ply"
        if (buff.readLine() != "ply") return PLY_HEADER_START_NOT_FOUND

        val plyDataTypes =
            enumValues<PlyVertexAttributeDataType>()
            .joinToString(" ")
            .toLowerCase(Locale.ROOT)

        //plyDataTypes = "void uchar int char uint ushort short float double"

        val plyDataNames =
            enumValues<PlyVertexAttributeDataName>()
                .joinToString(" ")
                .toLowerCase(Locale.ROOT)

        //plyDataNames = "none x y z nx ny nz u v s t red green blue alpha"

        // 'property modes'
        // 0= no mode selected
        // 1= vertex property mode after "element vertex" -line
        // 2= index list mode after "element face" -line
        // 3= unsupported property mode (user-defined elements, or such as "element edge" for example)
        var propertyMode = 0

        // NOTE: 'erret' returns mostly just the warnings
        var erret = 0

        var indexListDataType = PlyVertexAttributeDataType.VOID

        var loopy = true // <--- we all go a bit loopy here  :)
        var asciiFormat = false
        var verts: Int? = null
        var tris: Int? = null
        var propertyOrder = mutableListOf<PlyVertexAttribute>()

        headerLoop@ while (loopy) {
            val line = buff.readLine()
                ?: // line should never be null within the 'headerLoop'
                return PLY_HEADER_END_NOT_FOUND

            // Skip all empty lines (here should not be any)
            if (line.isBlank()) continue@headerLoop

            if (line.startsWith("format")) {
                if (line != "format ascii 1.0") return PLY_UNSUPPORTED_FORMAT
                asciiFormat = true
                continue@headerLoop
            }

            if (line.startsWith("comment")) continue@headerLoop

            if (line.startsWith("element vertex")) {
                verts = line.substring(15).toIntOrNull()
                if (verts == null) return PLY_ELEMENT_VERTEX_COUNT_ERROR
                propertyMode = 1
                continue@headerLoop
            }

            if (line.startsWith("element face")) {
                tris = line.substring(13).toIntOrNull()
                if (tris == null) return PLY_ELEMENT_FACE_COUNT_ERROR
                propertyMode = 2
                continue@headerLoop
            }

            // The other possible 'element' declarations are ignored:
            if (line.startsWith("element")) {
                propertyMode = 3
                continue@headerLoop
            }

            // parse the properties of 'propertyMode'
            if (line.startsWith("property")) {
                // Do we have a proper propertyMode set? If not, then ignore the property.
                if (propertyMode == 3 || propertyMode == 0) continue@headerLoop

                // Split the line into parts after the "property " (empty space intentional):
                val parts = (line.substring(9)).split(' ')

                if (parts.size < 2) return PLY_PROPERTY_PARSE_ERROR

                // Parse the vertex property type and name:
                if (plyDataTypes.contains(parts[0]) &&
                    plyDataNames.contains(parts[1])) {
                    // Accept only if the 'vertex property mode' is the current mode:
                    if (propertyMode != 1) return PLY_PROPERTY_PARSE_ERROR

                    // Add this attribute to the 'propertyOrder'
                    // vertex attribute type = parts[0]
                    // vertex attribute name = parts[1]
                    propertyOrder.add(
                        PlyVertexAttribute(
                            PlyVertexAttributeDataType
                                .valueOf(parts[0].toUpperCase(Locale.ROOT)),
                            PlyVertexAttributeDataName
                                .valueOf(parts[1].toUpperCase(Locale.ROOT)))
                    )
                    continue@headerLoop
                }

                // Parse the vertex index list:
                if (parts[0] == "list") {
                    // Accept only the 'index list mode' and parts size must be exactly 5:
                    if (!(propertyMode == 2 && parts.size == 4)) return PLY_PROPERTY_PARSE_ERROR
                    // The next three parts (1, 2 and 3) are usually: "uchar", "int" and "vertex_index"
                    // parts[1] is the data type for the vertex count per face per entry (that we ignore)
                    // parts[2] is the data type for the vertex index per entry, we
                    // set the 'indexListDataType' as the type indicated by parts[2]:
                    indexListDataType = PlyVertexAttributeDataType.valueOf(parts[2].toUpperCase(Locale.ROOT))
                    // parts[3] is just fluff, but required to be "vertex_index" or "vertex_indices" (as set by Blender?) for the index list:
                    if (parts[3] != "vertex_indices" && parts[3] != "vertex_index") erret = PLY_WARNING_VERTEX_INDEX_LIST_DEFINITION_MISSING
                    continue@headerLoop
                }

                // User defined (and unsupported) vertex properties are all ignored:
                if (propertyMode == 1) {
                    // Just add an 'empty' entry that we can later safely ignore:
                    // vertex attribute type = void
                    // vertex attribute name = none
                    propertyOrder.add(
                        PlyVertexAttribute(/* VertexAttributeDataType.VOID,
						                      VertexAttributeDataName.NONE */)
                    )
                    continue@headerLoop
                }

                // if we have reached this point, we may have an issue...
                return PLY_PROPERTY_PARSE_ERROR

            } // <--- end of 'if (line.startsWith("property"))'

            if (line == "end_header") loopy = false

        } // <--- end of the 'headerLoop'


        // Some simple error checking:
        if (!asciiFormat) return PLY_UNSUPPORTED_FORMAT
        if (verts == null) return PLY_ELEMENT_VERTEX_COUNT_ERROR
        if (tris == null) return PLY_ELEMENT_FACE_COUNT_ERROR
        if (verts < 3) return PLY_ELEMENT_VERTEX_COUNT_ERROR
        if (tris < 1) return PLY_ELEMENT_FACE_COUNT_ERROR
        if (propertyOrder.isNullOrEmpty()) return PLY_PROPERTIES_MISSING

        // The 'propertyOrder' must have at least attribute 'x' and attribute 'y' as floats or doubles:
        if (!((propertyOrder.contains(
                PlyVertexAttribute(PlyVertexAttributeDataType.FLOAT, PlyVertexAttributeDataName.X)
                ) ||
                propertyOrder.contains(
                PlyVertexAttribute(PlyVertexAttributeDataType.DOUBLE, PlyVertexAttributeDataName.X)
              )) &&
               (propertyOrder.contains(
                PlyVertexAttribute(PlyVertexAttributeDataType.FLOAT, PlyVertexAttributeDataName.Y)
                ) ||
                propertyOrder.contains(
                PlyVertexAttribute(PlyVertexAttributeDataType.DOUBLE, PlyVertexAttributeDataName.Y)
                ))
            )) return PLY_PROPERTIES_MISSING


        // Init (reset by overriding) the VertexData object:
        _vertices = verts
        _triangles = tris
        _vertexArr = FloatArray(verts * MESH_COORDS_PER_VERTEX)
        _indexArr = IntArray(tris * 3)
        _vertexArrIsSet = false
        _indexArrIsSet = false


        // if we have at least 'nx' within the 'propertyOrder'
        // then we must reallocate the '_normalArray'
        _normalArr = if (propertyOrder.indexOfFirst { it.dataName == PlyVertexAttributeDataName.NX }
            != -1) FloatArray(MESH_COORDS_PER_VERTEX * verts) else null

        // if we have 'u' or 's' within the 'propertyOrder'
        // then we must reallocate a '_mainTexCoordArr'
        _mainTexCoordArr = if (propertyOrder.indexOfFirst {
            it.dataName == PlyVertexAttributeDataName.S || it.dataName == PlyVertexAttributeDataName.U }
            != -1) FloatArray(MESH_TEX_COORDS_PER_VERTEX * verts) else null


        // if we have "red" within the 'propertyOrder' (assuming we also have "green" and "blue")
        // then we must reallocate the '_mainColorArr'
        _mainColorArr = if (propertyOrder.indexOfFirst { it.dataName == PlyVertexAttributeDataName.RED }
            != -1) FloatArray(MESH_COLOR_CHANNELS_PER_VERTEX * verts) { 1f } else null
        // All of the colors are set to opaque white by default


        // Some index helpers:
        var h = 0 // <--- only for tracking the indexArray index
        var i = 0 // <--- vertexArray index
        var j = 0 // <--- current triangle face entry (after all of the vertices are processed)
        var k = 0 // <--- current propertyOrder entry

        loopy = true

        // A huge while loop to parse the vertex and index data:
        dataLoop@ while (loopy) {
            val line = buff.readLine()

            if (line == null) {
                // This should not happen, since there should be at least one
                // empty line at the end of the file (we stop reading before that).
                loopy = false
                break@dataLoop
            }
            if (line.isBlank()) continue@dataLoop
            k = 0

            if (i < verts) {
                // Parse vertex data:
                val parts = line.split(' ')
                for (part in parts) {
                    // TODO: get val f AND val c here beforehand!

                    // start an ugly loop:
                    when (propertyOrder[k].dataName) {
                        PlyVertexAttributeDataName.X -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _vertexArr[3 * i] = f
                            } else {
                                // TODO: debug log here!
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        PlyVertexAttributeDataName.Y -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _vertexArr[3 * i + 1] = f
                            } else {
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        PlyVertexAttributeDataName.Z -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _vertexArr[3 * i + 2] = f
                            } else {
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        PlyVertexAttributeDataName.NX -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                //normalArray.set(3 * i, f)
                                _normalArr?.set(3 * i, f)
                            } else {
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        PlyVertexAttributeDataName.NY -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _normalArr?.set(3 * i + 1, f)
                            } else {
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        PlyVertexAttributeDataName.NZ -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _normalArr?.set(3 * i + 2, f)
                            } else {
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        PlyVertexAttributeDataName.S, PlyVertexAttributeDataName.U -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _mainTexCoordArr?.set(2 * i, f)
                            } else {
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        PlyVertexAttributeDataName.T -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _mainTexCoordArr?.set(2 * i + 1, f)
                            } else {
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        PlyVertexAttributeDataName.V -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _mainTexCoordArr?.set(2 * i + 1, 1f - f)
                            } else {
                                return PLY_PROPERTY_DATA_TYPE_ERROR
                            }	}
                        // NOTE: color components should be integer types (ply standard)
                        PlyVertexAttributeDataName.RED -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _mainColorArr?.set(4 * i, f)
                            } else {
                                val c = part.toIntOrNull() ?: 0
                                _mainColorArr?.set(4 * i, c / 255f)
                            }	}
                        PlyVertexAttributeDataName.GREEN -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _mainColorArr?.set(4 * i + 1, f)
                            } else {
                                val c = part.toIntOrNull() ?: 0
                                _mainColorArr?.set(4 * i + 1, c / 255f)
                            }	}
                        PlyVertexAttributeDataName.BLUE -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _mainColorArr?.set(4 * i + 2, f)
                            } else {
                                val c = part.toIntOrNull() ?: 0
                                _mainColorArr?.set(4 * i + 2, c / 255f)
                            }	}
                        PlyVertexAttributeDataName.ALPHA -> {
                            if (propertyOrder[k].dataType == PlyVertexAttributeDataType.FLOAT ||
                                propertyOrder[k].dataType == PlyVertexAttributeDataType.DOUBLE) {
                                val f = part.toFloatOrNull() ?: 0f
                                _mainColorArr?.set(4 * i + 3, f)
                            } else {
                                val c = part.toIntOrNull() ?: 0
                                _mainColorArr?.set(4 * i + 3, c / 255f)
                            }	}
                        else -> { /* do nothing */ }
                    } // <--- end of 'when (propertyOrder[k].dataName)'

                    // Goto next property in the 'propertyOrder'
                    k++

                    // TODO: if k >= propertyOrder.size then error?
                } // <--- end of 'for(part in parts)'

                // Goto next vertex:
                i++

                continue@dataLoop
            } // <--- end of 'if (i < vertices)'

            if (j < tris) {
                // Parse index data:
                val parts = line.split(' ')
                for (part in parts) {
                    when (k) {
                        0 -> { // first part tells us the vertex count of this face entry,
                            // it must be 3 for a triangle (we are also assuming integer data type)
                            val c = part.toIntOrNull() ?: 0
                            if (c != 3) {
                                // Warn about untriangulated polygons:
                                erret = PLY_WARNING_FACES_UNTRIANGULATED
                            }
                        }
                        1, 2, 3 -> {  _indexArr[h] = part.toIntOrNull() ?: 0; h++ }
                        // 4 -> quadrilaterals and other faces are not accepted!
                        //      only the first three vertices of a face are considered (-> forced triangle)
                    }
                    // Goto next property in the 'propertyOrder'
                    k++
                }
                // Goto next face entry (triangle):
                j++
                continue@dataLoop
            }

            // Skip all the other lines (the unsupported stuff?):
            loopy = false

        } // <--- end of 'dataLoop'

        // All done (errors be damned!)
        _vertexArrIsSet = true
        _indexArrIsSet = true
        return erret
    }

    // hardcoded functions that create vertex data:
    companion object {

        /**
         * Creates vertex data for a cube.
         *
         * @param[size] The width, height and length of the cube. Default size is one (unit of measure).
         * @param[normals] True if each vertex should have a normal vector.
         * @param[textureCoordinates] True if each vertex should have texture coordinates.
         * @param[vertexColors] True if each vertex should have color data.
         */
        fun createCubeData(size: Float = 1f, normals: Boolean = false,
                           textureCoordinates: Boolean = false,
                           vertexColors: Boolean = false): VertexData {

            if (size < 0.001f) throw IllegalArgumentException("Can not create a cube with such a small size.")

            val retval = VertexData(24,12)

            // required arrays for the cube:
            // (index array and vertex positions array)

            val indexArr = intArrayOf(
                0,  1,  2,   0,  2,  3,  // <--- Front
                4,  5,  6,   4,  6,  7,  // <--- Back
                8,  9,  10,  8,  10, 11, // <--- Top
                12, 13, 14,  12, 14, 15, // <--- Bottom
                16, 17, 18,  16, 18, 19, // <--- Right
                20, 21, 22,  20, 22, 23  // <--- Left
            )

            // size to vertex positions:
            val s = 0.5f * size

            val vertexArr = floatArrayOf(
                // Front
                -s, -s,  s,    s, -s,  s,
                 s,  s,  s,   -s,  s,  s,
                // Back
                -s, -s, -s,   -s,  s, -s,
                 s,  s, -s,    s, -s, -s,
                // Top
                -s,  s, -s,   -s,  s,  s,
                 s,  s,  s,    s,  s, -s,
                // Bottom
                -s, -s, -s,    s, -s, -s,
                 s, -s,  s,   -s, -s,  s,
                // Right
                 s, -s, -s,    s,  s, -s,
                 s,  s,  s,    s, -s,  s,
                // Left
                -s, -s, -s,   -s, -s,  s,
                -s,  s,  s,   -s,  s, -s)

            retval.setVertices(vertexArr)
            retval.setIndices(indexArr)

            // optional arrays:
            // (normals, texture coordinates, and colors )

            if (normals) {
                val vertexNormals = floatArrayOf(
                    // Front
                    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,
                    // Back
                    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,
                    // Top
                    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,
                    // Bottom
                    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
                    // Right
                    1.0f, 0.0f, 0.0f,    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,    1.0f, 0.0f, 0.0f,
                    // Left
                    -1.0f, 0.0f, 0.0f,    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,    -1.0f, 0.0f, 0.0f)
                retval.setNormals(vertexNormals)
            }

            if (textureCoordinates) {
                val textureCoords = floatArrayOf(
                    // Front
                    0.0f, 0.0f, 1.0f, 0.0f,
                    1.0f, 1.0f, 0.0f, 1.0f,
                    // Back
                    0.0f, 0.0f, 1.0f, 0.0f,
                    1.0f, 1.0f, 0.0f, 1.0f,
                    // Top
                    0.0f, 0.0f, 1.0f, 0.0f,
                    1.0f, 1.0f, 0.0f, 1.0f,
                    // Bottom
                    0.0f, 0.0f, 1.0f, 0.0f,
                    1.0f, 1.0f, 0.0f, 1.0f,
                    // Right
                    0.0f, 0.0f, 1.0f, 0.0f,
                    1.0f, 1.0f, 0.0f, 1.0f,
                    // Left
                    0.0f, 0.0f, 1.0f, 0.0f,
                    1.0f, 1.0f, 0.0f, 1.0f)
                retval.setTextureCoordinates(textureCoords)
            }

            if (vertexColors) {
                val vertexColorsArr = FloatArray(96) { 1.0f }
                retval.setVertexColors(vertexColorsArr)
            }

            return retval
        } // <--- end of 'createCubeData'
    }

}

