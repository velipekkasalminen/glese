package com.velipekkasalminen.glese


import android.graphics.Bitmap
import android.opengl.GLES20
import android.opengl.GLES30
import android.opengl.GLUtils
import android.os.Build


/**
 * TextureCubemap represents a cube map texture living on the gpu.
 * Cube maps are constructed from six bitmaps, and are usually used with sky boxes and
 * shaders utilizing environment reflections.
 */
class TextureCubemap(name: String) : Texture(name) {

    /**
     * Constructs a cube map from the supplied bitmaps.
     * The bitmaps can be released and garbage collected after this operation.
     *
     * @return Zero on success, otherwise returns one of the TEXTURE_* error codes or GL_* error codes.
     */
    fun construct(
        front: Bitmap,
        back: Bitmap,
        left: Bitmap,
        right: Bitmap,
        top: Bitmap,
        bottom: Bitmap,
        @GLESIntHandle textureUnit: Int = 0
    ): Int {
        // TODO: call GLES30.glEnable(GLES30.GL_TEXTURE_CUBE_MAP_SEAMLESS) here? ARB_texture_cube_map is required.

        if (_textureInitialized) return TEXTURE_ALREADY_INITIALIZED

        when {
            textureUnit < 0 -> return TEXTURE_INVALID_TEXTURE_UNIT
            textureUnit >= 8 && textureUnit >= Engine.textureUnitCount -> return TEXTURE_INVALID_TEXTURE_UNIT
        }

        val usage = GLES20.GL_TEXTURE_CUBE_MAP

        // generate texture object name (=handle):
        var tmpArr = intArrayOf(0)
        GLES20.glGenTextures(1, tmpArr, 0)
        _txHandle = tmpArr[0]

        // set the active texture unit to modify:
        _txUnit = GLES20.GL_TEXTURE0 + textureUnit
        GLES20.glActiveTexture(_txUnit)
        _txUnif = textureUnit

        // bind texture to active texture unit:
        GLES20.glBindTexture(usage, _txHandle)

        // did the binding cause errors?
        var glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError

        GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            // GLES30 on API level 18 (JELLY_BEAN_MR2) and onwards
            GLES30.glTexParameteri(usage, GLES30.GL_TEXTURE_WRAP_R, GLES30.GL_CLAMP_TO_EDGE)
            GLES30.glTexParameteri(usage, GLES30.GL_TEXTURE_BASE_LEVEL, 0)
            GLES30.glTexParameteri(usage, GLES30.GL_TEXTURE_MAX_LEVEL, 0)
        }

        GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR)
        GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)

        // NOTE: GL_TEXTURE_CUBE_MAP_NEGATIVE_Z represents the front face of the cube map,
        //       since in OpenGL the negative z -direction points forward (the z-axis is 'backwards')

        // using GLUtils for convenience:
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, front, 0)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, back, 0)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, left, 0)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, right, 0)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, top, 0)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, bottom, 0)

        // did the texImage2D from the GLUtils cause errors?
        glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError


        _textureInitialized = true

        return 0
    }

}

