package com.velipekkasalminen.glese


import android.opengl.GLES20



/**
 * Texture is a base class for [Texture2D] and [TextureCubemap]
 */
open class Texture(val name: String): IDeletable {

    protected var _textureInitialized: Boolean = false
    protected var _mipmapLevels = 0

    // texture handle:
    // NOTE: "The value zero is reserved to represent the default texture for each texture target."
    @GLESIntHandle protected var _txHandle = 0

    // texture unit 'slot'
    @GLESIntHandle protected var _txUnit = 0
    @GLESIntHandle protected var _txUnif = 0


    /**
     * The number of mipmap levels.
     * Zero mipmap levels indicate an error,
     * one level indicates that there are no mipmapping available.
     */
    val mipmapLevels get() = _mipmapLevels
    override val isInitialized: Boolean get() = _textureInitialized

    /**
     * The OpenGL 'name' of this texture.
     */
    @GLESIntHandle val textureHandle get() = _txHandle

    /**
     * One of the GL_TEXTURE* texture units slots.
     */
    @GLESIntHandle val textureUnit get() = _txUnit

    /**
     * Represents texture sampler uniforms's value, that is the texture unit number
     * (starting from 0) that the texture is assigned to by default.
     * For example the value 0 corresponds with GL_TEXTURE0 texture unit.
     */
    @GLESIntHandle val textureUniform get() = _txUnif


    /**
     * Tells the OpenGL ES to delete the associated texture buffer.
     *
     */
    override fun delete() {
        GLES20.glDeleteTextures(1, intArrayOf(_txHandle), 0)
        _txUnit = 0
        _txHandle = 0
        _textureInitialized = false
    }

    override fun toString(): String = "${name}(initialized:${_textureInitialized}, mipmapLevels:${_mipmapLevels}, textureUnit:${_txUnit})"

}

