package com.velipekkasalminen.glese


import android.annotation.SuppressLint
import android.content.Context
import android.opengl.GLSurfaceView
import android.view.KeyEvent
import android.view.MotionEvent


class GleseSurfaceView(context: Context) : GLSurfaceView(context) {

    private val _renderer: GleseRenderer
    private var _rendererIsSet = false

    val renderer get() = _renderer

    init {
        // super(context) called at construction

        if (config.eglContextClientVersion == EGLContextVersion.OPENGLES3CONTEXT) {
            // Create an OpenGL 3.0 compatible context:
            setEGLContextClientVersion(3)
        } else {
            // Create an OpenGL 2.0 compatible context:
            setEGLContextClientVersion(2)
        }

        // TODO: use setEGLConfigChooser here:
        //       setEGLConfigChooser(redSize, greenSize, blueSize, alphaSize, depthSize, stencilSize)

        /*
        NOTE: "If no setEGLConfigChooser method is called, then by default the view will
              choose an RGB_888 surface with a depth buffer depth of at least 16 bits."
        */

        _renderer = GleseRenderer()

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(_renderer)
        _rendererIsSet = true

        // setRenderMode(RENDERMODE_WHEN_DIRTY) makes the renderer render when
        // requestRender is called and the buffer has changed (=dirty),
        // the other mode is RENDERMODE_CONTINUOUSLY which we will use.

        // NOTE: setRenderMode can only be called after calling setRenderer
        setRenderMode(RENDERMODE_CONTINUOUSLY)

        // set some common access points:
        Engine.renderer = _renderer
        Engine.surfaceView = this

        // do we (attempt to) preserve the context?
        setPreserveEGLContextOnPause(config.eglContextPreservedOnPause)

        // set the renderer things:
        _renderer.targetFPS = config.targetFPS
        _renderer.clearColor = config.clearColor

        //_renderer.runSceneDeque()

        // load the staring scene:
        //if (config.startingSceneName != null) {
        //    if (!Engine.scenes.load(config.startingSceneName!!))
        //        throw IllegalArgumentException("Attempted to start scene \"${config.startingSceneName}\" but it was not found on the Engine.scenes")
        //} // else ?? Warning: no starting scene name set ??

    }

    /*
    The following functions will use the 'queueEvent' to run callbacks
    on the rendering thread.
    */

    override fun onPause() {
        if (!_rendererIsSet) { super.onPause(); return }

        // call onPause on the engine and on the active scene:
        if (Engine.onPauseCallback != null || Engine.scenes.activeScene != null)
            queueEvent {
                Engine.onPauseCallback?.let { it() }
                Engine.scenes.activeScene?.onPause()
            }

        // lastly call:
        super.onPause()
    }

    override fun onResume() {
        if (!_rendererIsSet) { super.onResume(); return }

        // call onResume on the engine and on the active scene:
        if (Engine.onResumeCallback != null || Engine.scenes.activeScene != null)
            queueEvent {
                Engine.onResumeCallback?.let { it() }
                Engine.scenes.activeScene?.onResume()
            }

        // lastly call:
        super.onResume()
    }

    // TODO: performCLick call in onTouchEvent for the GLSurfaceView?

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!_rendererIsSet) return false

        var retval = false
        if (Engine.onMotionEventCallback != null)
            queueEvent {
                Engine.onMotionEventCallback?.let { retval = it(event) }
            }
        //retval = super.onTouchEvent(event)
        return retval
    }

    override fun onTrackballEvent(event: MotionEvent): Boolean {
        if (!_rendererIsSet) return false

        var retval = false
        if (Engine.onMotionEventCallback != null)
            queueEvent {
                Engine.onMotionEventCallback?.let { retval = it(event) }
            }
        //retval = super.onTrackballEvent(event)
        return retval
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (!_rendererIsSet) return false

        var retval = false
        if (Engine.onKeyDownCallback != null)
            queueEvent {
                Engine.onKeyDownCallback?.let { retval = it(keyCode, event) }
            }
        //retval = super.onKeyDown(keyCode, event)
        return retval
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        if (!_rendererIsSet) return false

        var retval = false
        if (Engine.onKeyUpCallback != null)
            queueEvent {
                Engine.onKeyUpCallback?.let { retval = it(keyCode, event) }
            }
        //retval = super.onKeyUp(keyCode, event)
        return retval
    }

    companion object {

        // config can be set, even before the object is instantiated:
        var config = GleseStartConfig(null)

    }

}
