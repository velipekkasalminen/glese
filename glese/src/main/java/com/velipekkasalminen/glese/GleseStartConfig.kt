package com.velipekkasalminen.glese


enum class EGLContextVersion { OPENGLES2CONTEXT, OPENGLES3CONTEXT }

/** Simple data class for start configuration. */
data class GleseStartConfig(var startingSceneName: String?) {
    var targetFPS: Float = 60f
    var eglContextPreservedOnPause: Boolean = false
    var eglContextClientVersion: EGLContextVersion = EGLContextVersion.OPENGLES2CONTEXT
    var clearColor: ColorRGBA = ColorRGBA(0f,0f,0f,1f)

    // nothing else here? what about the color, depth and stencil buffers?

}
