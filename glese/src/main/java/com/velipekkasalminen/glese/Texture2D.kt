package com.velipekkasalminen.glese


import android.graphics.Bitmap
import android.opengl.GLES20
import android.opengl.GLUtils
import com.velipekkasalminen.glese.utils.calculateMipmapLevels
import com.velipekkasalminen.glese.utils.isPowerOf2


/**
 * Texture2D represents an ordinary 2D texture living on the gpu.
 * It is possible to modify the texture after initialization,
 * but not recommended due to affecting the performance adversely.
 *
 */
class Texture2D(name: String) : Texture(name) {


    /**
     * Constructs a texture (buffer for the gpu) from the supplied bitmap and
     * assigns it to the given texture unit.
     * The bitmap can be released and garbage collected after this operation.
     *
     * @param[bitmap] The bitmap to use for the texture.
     * @param[textureUnit] The texture unit number where the texture is 'installed'. This number is also the index number for the texture uniform. The first texture unit is at position 0.
     * @param[generateMipmapsHint] Hints that mipmaps should be generated for the texture.
     * @param[mipmapFiltering] Sets the mipmap minification filtering.
     * There are six to choose from: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST,
     * GL_LINEAR_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, which is the default and GL_LINEAR_MIPMAP_LINEAR.
     *
     * @return Zero on success, otherwise returns one of the TEXTURE_* error codes or GL_* error codes.
     */
    fun construct(
        bitmap: Bitmap,
        @GLESIntHandle textureUnit: Int = 0,
        generateMipmapsHint: Boolean = false,
        mipmapFiltering: Int = GLES20.GL_NEAREST_MIPMAP_LINEAR
    ): Int {
        if (_textureInitialized) return TEXTURE_ALREADY_INITIALIZED

        // NOTE: texture units 0..7 are guaranteed to exist, others must be checked if they exist or not

        when {
            textureUnit < 0 -> return TEXTURE_INVALID_TEXTURE_UNIT
            textureUnit >= 8 && textureUnit >= Engine.textureUnitCount -> return TEXTURE_INVALID_TEXTURE_UNIT
        }

        val usage = GLES20.GL_TEXTURE_2D
        val isPOW2 = isPowerOf2(bitmap) // <--- false means NPOT texture

        // generate texture object name (=handle):
        var tmpArr = intArrayOf(0)
        GLES20.glGenTextures(1, tmpArr, 0)
        _txHandle = tmpArr[0]

        // set the active texture unit to modify:
        _txUnit = GLES20.GL_TEXTURE0 + textureUnit
        GLES20.glActiveTexture(_txUnit)
        _txUnif = textureUnit

        // bind texture to active texture unit:
        GLES20.glBindTexture(usage, _txHandle)

        // did the binding cause errors?
        var glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError

        // use GLUtils for convenience:
        GLUtils.texImage2D(usage, 0, bitmap, 0)

        // did the texImage2D from the GLUtils cause errors?
        glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError

        // at this point we set this?
        _textureInitialized = true

        var errret = 0    // <--- no errors
        _mipmapLevels = 1

        if (isPOW2) {
            if (generateMipmapsHint) {
                // generate mipmaps and set default minification filter for mipmap enabled texture:
                // TODO: see:  https://www.khronos.org/opengl/wiki/Texture#Mipmap_completeness
                GLES20.glGenerateMipmap(usage)
                GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_MIN_FILTER, mipmapFiltering)
                _mipmapLevels = calculateMipmapLevels(bitmap)
            } else {
                // no mipmaps:
                // TODO: see:  https://www.khronos.org/opengl/wiki/Common_Mistakes#Creating_a_complete_texture
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                //    GLES32.glTexParameteri(usage, GLES32.GL_TEXTURE_BASE_LEVEL, 0)
                //    GLES32.glTexParameteri(usage, GLES32.GL_TEXTURE_MAX_LEVEL, 0)
                //}
                // set filter to linear:
                GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)
            }
            // enable uv/st repeating: (these are defaults)
            GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT)
            GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT)
        } else {
            if (generateMipmapsHint) errret = TEXTURE_WARNING_NPOT
            // do not generate mipmaps if the texture is NPOT
            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            //    GLES32.glTexParameteri(usage, GLES32.GL_TEXTURE_BASE_LEVEL, 0)
            //    GLES32.glTexParameteri(usage, GLES32.GL_TEXTURE_MAX_LEVEL, 0)
            //}
            GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)
            // disable uv/st repeating for NPOT texture :(
            GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
            GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
            GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)
        }

        // mipmapped or not, set magnification filter to linear (only two options: linear or nearest)
        GLES20.glTexParameteri(usage, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        // what are the correct pack alignments?
        // these are the defaults:
        //GLES32.glPixelStorei(GLES32.GL_PACK_ALIGNMENT, 4)
        //GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 4)

        glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError

        // stop bondage-play with the texture (bind back to zero):
        GLES20.glBindTexture(usage, 0)

        return errret
    }

    /**
     * Modifies a subset of the texture.
     *
     * @param[subBitmap] The bitmap that replaces a portion of the texture. The underlying implementation may not support non power of two images.
     * @param[x] The x offset (in pixels) on the texture where the [subBitmap] is placed.
     * @param[y] The y offset (in pixels) on the texture where the [subBitmap] is placed.
     * @param[mipmapLevel] the mipmap level index that is being modified. Zero refers to the main mipmap level.
     *
     * @return Zero or on success, otherwise returns TEXTURE_NOT_INITIALIZED error code or one of the GL_* error codes.
     */
    fun modify(subBitmap: Bitmap, x: Int = 0, y: Int = 0, mipmapLevel: Int = 0): Int {
        if (!_textureInitialized) return TEXTURE_NOT_INITIALIZED

        GLES20.glActiveTexture(_txUnit)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, _txHandle)

        GLUtils.texSubImage2D(GLES20.GL_TEXTURE_2D, mipmapLevel, x, y, subBitmap)

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0)

        val glError = GLES20.glGetError()
        return if (glError != GLES20.GL_NO_ERROR) glError
        else 0
    }


}

