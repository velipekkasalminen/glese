@file:JvmName("utils")

package com.velipekkasalminen.glese.utils


import android.graphics.Bitmap
import android.graphics.PointF
import android.os.Build
import android.util.SizeF
import androidx.annotation.FloatRange
import androidx.annotation.RequiresApi
import com.velipekkasalminen.glese.near
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.Charset
import kotlin.math.*


/*
    This file contains some useful functions and other objects.
    These utilities can be accessed in Java the same way they are accessed in Kotlin.
*/


/**
 * Returns true if the given (positive) integer value [i] is a power-of-two value.
 * This is mostly used to check the dimension requirements of bitmaps when
 * creating textures.
 */
fun isPowerOf2(i: Int): Boolean = (i and (i-1)) == 0

/** True if the dimensions of the bitmap are a power-of-two */
fun isPowerOf2(bitmap: Bitmap) = isPowerOf2(bitmap.width) && isPowerOf2(bitmap.height)

/** Calculates the number of possible mipmap levels for a bitmap. */
fun calculateMipmapLevels(bitmap: Bitmap): Int = (1 + floor( log2( max(bitmap.width.toFloat(), bitmap.height.toFloat()) ) )).toInt()


/**
 * Linearly interpolates between [a] and [b] by [t].
 * When t=0 returned value equals a, when t=1 returned value equals b.
 * In GLSL this function is known as 'mix'
 */
fun lerp(a: Float, b:Float, @FloatRange(from=0.0, to=1.0, fromInclusive=true, toInclusive=true) t: Float): Float = (1f - t) * a + t * b

/**
 * Spherical linear interpolation between [a] and [b] by [t].
 * When t=0 returned value equals a, when t=1 returned value equals b.
 */
fun slerp(a: Float, b: Float, @FloatRange(from=0.0, to=1.0, fromInclusive=true, toInclusive=true) t: Float): Float = a * 0.5f * (1f - cos((1f - t) * 3.141592654f)) + b * 0.5f * (1f - cos(t * 3.141592654f))

/** Unnormalized sinc function, returns one when x is near zero. */
fun sincu(x: Float): Float = if (x near 0f) 1f else sin(x) / x

/** Normalized sinc function, returns one when x is near zero. */
fun sincn(x: Float): Float = if (x near 0f) 1f else sin(3.1415926536f * x) / (3.1415926536f * x)

/**
 * Smoothstep function, similar to AMD implementation.
 * The value [f] is interpolated between [edgeLeft] and [edgeRight],
 * and a value between zero and one is returned.
 *
 */
fun smoothstep(f: Float, edgeLeft: Float, edgeRight: Float): Float {
    val x = (f - edgeLeft) / (edgeRight - edgeLeft)
    return when {
        x in 0f..1f -> x * x * (3f - 2f * x)
        x < 0f -> 0f
        else -> 1f
    }
}

/**
 * Improved version of the [smoothstep] function; polynomial by Kenneth Perlin.
 */
fun smootherstep(f: Float, edgeLeft: Float, edgeRight: Float): Float {
    val x = (f - edgeLeft) / (edgeRight - edgeLeft)
    return when {
        x in 0f..1f -> x * x * x * (x * (x * 6f - 15f) + 10f)
        x < 0f -> 0f
        else -> 1f
    }
}

/** Inverse smoothstep function, similar to GLSL implementation. */
fun inverseSmoothstep(f: Float): Float = 0.5f - sin(asin(1f - 2f * f) / 3f)

/** Lerp function based on smoothstep, similar to cubic interpolation. */
fun smoothlerp(a: Float, b: Float, @FloatRange(from=0.0, to=1.0, fromInclusive=true, toInclusive=true) t:Float): Float = a + (t * t * (3f - 2f * t)) * (b - a)

/** Cubic interpolation; interpolates values [f1], [f2], [f3] and [f4] based on weight [t]. */
fun cubic(f1: Float, f2: Float, f3: Float, f4: Float, t: Float): Float {
    val x = f4 - f3 - f1 + f2
    val y = f1 - f2 - x
    return x * t * t * t + y * t * t + (f3 - f1) * t + f2
}


/**
 * Converts a pixel position of [x] and [y] to a view position range of [-1,1] for x and [-1,1] for y.
 * The [width] and [height] are given in pixels and determine the total size of the view area to
 * convert from; for example width and height could be 1080 by 1920 for hd-fullscreen applications
 * in the portrait mode.
 */
fun toGLViewPosition(x: Int, y: Int, width: Int, height: Int): PointF = PointF(x / (0.5f * width) - 1f, 1f - y / (0.5f * height))

/**
 * Converts a pixel position (x and y) to a view position range of [-1,1] for x and [-1,1] for y.
 * @param[size] The size of the view area in pixels, this could be 1080 by 1920 for hd-fullscreen applications in the portrait mode.
 */
fun PointF.toGLViewPosition(size: PointF) {
    this.x = this.x / (0.5f * size.x) - 1f
    this.y = 1f - this.y / (0.5f * size.y)
}

/**
 * Converts a pixel position (x and y) to a view position range of [-1,1] for x and [-1,1] for y.
 * @param[size] The size of the view area in pixels, this could be 1080 by 1920 for hd-fullscreen applications in the portrait mode.
 */
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun PointF.toGLViewPosition(size: SizeF) {
        this.x = this.x / (0.5f * size.width) - 1f
        this.y = 1f - this.y / (0.5f * size.height)
    }


/**
 * Creates a 'fake' InputStream from a string.
 * Can be used with the VertexData.loadFromPly -method for example.
 * Input stream should be closed when done.
 */
fun asInputStream(str: String): InputStream = ByteArrayInputStream(str.toByteArray())

/**
 * Counterpart for the [asInputStream] function; reads text from an input stream, the closes it.
 */
fun asString(inp: InputStream, charset: Charset = Charsets.UTF_8): String {
    return inp.bufferedReader(charset).use { it.readText() }
}


// some tuple helpers:
// NOTE: These tuple classes could be replaced with just Float3 and Float4

/**
 * Tuple3
 *
 */
data class Tuple3<T1, T2, T3>(val t1: T1, val t2: T2, val t3: T3) {
    override fun toString(): String = "($t1, $t2, $t3)"

    /**
     * Creates a Tuple4 from this Tuple3, the fourth element [t4] is null by default.
     */
    fun toTuple4(t4: Any? = null) : Tuple4<T1, T2, T3, Any?> {
        return Tuple4(t1, t2, t3, t4)
    }
}

/**
 * Conversion from Tuple3 to Tuple4,
 * the fourth element is set to null.
 *
 */
fun Tuple3<*, *, *>.toTuple4(): Tuple4<*, *, *, *> = Tuple4(t1, t2, t3, null)


/**
 * Tuple4
 *
 */
data class Tuple4<T1, T2, T3, T4>(val t1: T1, val t2: T2, val t3: T3, val t4: T4) {
    override fun toString(): String = "($t1, $t2, $t3, $t4)"
}

/**
 * Conversion from Tuple4 to Tuple3,
 * the fourth element is ignored.
 *
 */
fun Tuple4<*, *, *, *>.toTuple3(): Tuple3<*, *, *> = Tuple3(t1, t2, t3)


