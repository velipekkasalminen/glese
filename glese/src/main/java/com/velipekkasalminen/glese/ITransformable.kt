package com.velipekkasalminen.glese



interface ITransformable {

    /**
     * The local position in cartesian coordinates.
     */
    var position: Position

    /**
     * The scale of this object.
     */
    var scale: Vector4

    /**
     * The rotation (orientation) represented by a quaternion.
     */
    var rotation: Quaternion

    /**
     * The 'forward' direction vector
     * obtained from the local transform matrix.
     */
    val forward: Vector4

    /**
     * The 'up' direction vector
     * obtained from the local transform matrix.
     */
    val up: Vector4

    /**
     * The 'right' direction vector
     * obtained from the local transform matrix.
     */
    val right: Vector4

    /**
     * The local transform matrix.
     */
    val matrix: Matrix4x4

    /**
     * True, if this represents a stationary transform.
     * For example: static geometry.
     */
    var isStationary: Boolean

    /**
     * Does the calculations for the matrix transformations.
     */
    fun calculateTransformations()

}

