package com.velipekkasalminen.glese


import com.velipekkasalminen.glese.utils.Tuple3
import com.velipekkasalminen.glese.utils.Tuple4
import android.renderscript.Float3
import android.renderscript.Float4
import java.text.DecimalFormat
import kotlin.math.acos
import kotlin.math.sqrt


/**
 * Vector4 represents a vector in the homogeneous coordinate space with four components: x, y, z and w.
 * Each component is a float value and can be set to any valid numerical value.
 * The w component is typically set to 1, this way the homogeneous coordinates of the vector
 * can be directly mapped to the cartesian coordinate system.
 * To transform a vector from the homogeneous coordinate system (x,y,z,w) to the cartesian (x,y,z,0)
 * representation, you may use the [toCartesian] method of the vector instance.
 * The Vector4 object can also be made to represent a [Quaternion].
 */
open class Vector4 {

    var x: Float = 0.0f
    var y: Float = 0.0f
    var z: Float = 0.0f
    var w: Float = 0.0f

    // components for destructuring,
    // xyz and xyzw setters and getters do about the same...
    operator fun component1() = x
    operator fun component2() = y
    operator fun component3() = z
    operator fun component4() = w

    operator fun unaryMinus() = Vector4(-x, -y, -z, -w)
    operator fun plus(v: Vector4) = Vector4(x+v.x, y+v.y, z+v.z, w+v.w)
    operator fun minus(v: Vector4) = Vector4(x-v.x, y-v.y, z-v.z, w-v.w)
    operator fun times(i: Int) = Vector4(x*i, y*i, z*i, w*i)
    operator fun times(f: Float) = Vector4(x*f, y*f, z*f, w*f)

    operator fun plusAssign(v: Vector4) = add(v)
    operator fun minusAssign(v: Vector4) = subtract(v)
    operator fun timesAssign(f: Float) = multiply(f)
    operator fun timesAssign(i: Int) = multiply(i.toFloat())

    override fun equals(other: Any?): Boolean {
        when(other) {
            is Vector4 -> return x near other.x && y near other.y && z near other.z && w near other.w
            is Float4  -> return x near other.x && y near other.y && z near other.z && w near other.w
            is Tuple4<*, *, *, *> -> {
                return if (other.t1 is Float && other.t2 is Float && other.t3 is Float && other.t4 is Float)
                    x near other.t1 && y near other.t2 && z near other.t3 && w near other.t4
                else false
            }
            is FloatArray -> {
                return if (other.size == 4)
                    x near other[0] && y near other[1] && z near other[2] && w near other[3]
                else false
            }
        }
        return false
    }

    /**
     * Simple swizzling for the x, y, and z, the w is ignored.
     * Example usage: var (x, y, z) = myVector4.xyz
     */
    var xyz
        get() = Tuple3(x, y, z)
        set(value) {
            x = value.t1
            y = value.t2
            z = value.t3
            //w = value.t4
        }

    /**
     * Simple swizzling for the x, y, z, and w.
     * Example usage: var (x, y, z, w) = myVector4.xyzw
     */
    var xyzw
        get() = Tuple4(x, y, z, w)
        set(value) {
            x = value.t1
            y = value.t2
            z = value.t3
            w = value.t4
        }

    /**
     * The squared length/magnitude of this vector.
     */
    val lengthSq: Float
        get() = x * x + y * y + z * z + w * w

    /**
     * The length/magnitude of this vector.
     */
    val length: Float
        get() = sqrt(x * x + y * y + z * z + w * w)


    /**
     * True, if this Vector4 instance is in the cartesian coordinate system.
     */
    val isCartesian: Boolean
        get() =  w near 0f //= (w > -1e-5f && w < 1e-5f)


    /**
     * True, if this vector can be considered as a normalized vector.
     */
    val isNormalized: Boolean
        get() = lengthSq near 1f


    /**
     * True, if all of the components (x, y, z and w) are zero.
     */
    val isZero: Boolean
        get() = (x + y + z + w) near 0f


    fun set(x: Float, y:Float, z:Float, w:Float) {
        this.x = x
        this.y = y
        this.z = z
        this.w = w
    }

    fun set(v: Vector4) {
        x = v.x
        y = v.y
        z = v.z
        w = v.w
    }

    /**
     * Creates a new zero vector instance.
     */
    constructor() // dummy constructor

    /**
     * Creates a new Vector4 instance from a renderscript Float3 structure.
     */
    constructor(f: Float3) { x = f.x; y = f.y; z = f.z }

    /**
     * Creates a new Vector4 instance from a renderscript Float4 structure.
     */
    constructor(f: Float4) { x = f.x; y = f.y; z = f.z; w = f.z }

    /**
     * Creates a new Vector4 instance by copying values from another Vector4.
     */
    constructor(v: Vector4) { x = v.x; y = v.y; z = v.z; w = v.w }

    /**
     * Creates a new Vector4 instance with specified [x], [y], [z] and [w] values.
     */
    constructor(x: Float, y: Float, z: Float, w: Float = 0.0f) {
        this.x = x
        this.y = y
        this.z = z
        this.w = w
    }

    /**
     * Converts this vector instance from a homogeneous coordinate
     * space to a cartesian coordinate space by
     * dividing each of the x, y and z components with the w component,
     * the w component is then set to zero.<br />
     * No value is returned by this method, the conversion result overrides
     * the coordinate values of the vector. If the vector instance is already
     * a cartesian vector (the w is zero), no action is taken.
     *
     */
    fun toCartesian() {
        when {
            //w > -1e-5f && w < 1e-5f -> return
            w near 0f -> return
            //w > 0.99999f && w < 1.00001f -> { w = 0f; return }
            w near 1f -> { w = 0f; return }
        }
        w = 1f / w
        x *= w;
        y *= w;
        z *= w;
        w = 0f
    }

    /**
     * Calculates the cross product of this and another Vector4 object, then stores the
     * result back to this instance.<br />
     * Both vectors should be in the cartesian coordinate system, since the w component
     * is ignored.
     *
     */
    fun crossWith(v: Vector4) {
        tmp1 = y * v.z - z * v.y
        tmp2 = z * v.x - x * v.z
        this.z = x * v.y - y * v.x
        this.y = tmp2
        this.x = tmp1
    }

    /**
     * Normalizes this instance: the length (magnitude) will become 1.<br />
     * TIP: To normalize a vector in the cartesian coordinate system,
     * first use the [toCartesian] method, and then use the normalize -method.<br />
     * Note that zero vectors can not be normalized, use [isZero] to detect this condition.
     */
    fun normalize() {
        tmp3 = x * x + y * y + z * z + w * w
        if (tmp3 near 0f) /*(tmp3 > -1e-5f && tmp3 < 1e-5f)*/ {
            // we may have a zero vector!
            // TODO: log error (can not normalize a zero vector)
            return
        }
        multiply( 1.0f / sqrt(tmp3) )
    }

    /**
     * Adds coordinate values from another Vector4 to this instance.
     *
     */
    fun add(v: Vector4) { x += v.x; y += v.y; z += v.z; w += v.w }

    /**
     * Subtracts coordinate values of this instance by values from another Vector4.
     *
     */
    fun subtract(v: Vector4) { x -= v.x; y -= v.y; z -= v.z; w -= v.w }

    /**
     * Multiplies the coordinate values of this instance by scalar value [n].
     * This operation lengthens the vector.
     *
     */
    fun multiply(n: Float) { x *= n; y *= n; z *= n; w *= n }

    /**
     * Element-wise multiplication with another vector [v].
     * This operation is also known as Hadamard product or Schur product.
     *
     */
    fun multiply(v: Vector4) { x *= v.x; y *= v.y; z *= v.z; w *= v.w }

    /**
     * Multiplies matrix [m] with this vector.
     * The result is stored back to this vector instance:<br />
     * thisVector = matrix * thisVector
     *
     */
    fun multiply(m: Matrix4x4) {
        /*
        The idea here:
        tmp1 = dot(this, m.column1)
        tmp2 = dot(this, m.column2)
        tmp3 = dot(this, m.column3)
        w = dot(this, m.column4)
        x = tmp1
        y = tmp2
        z = tmp3
        */

        tmp1 = x * m[0,0] + y * m[0,1] + z * m[0,2] + w * m[0,3]
        tmp2 = x * m[1,0] + y * m[1,1] + z * m[1,2] + w * m[1,3]
        tmp3 = x * m[2,0] + y * m[2,1] + z * m[2,2] + w * m[2,3]
        w = x * m[3,0] + y * m[3,1] + z * m[3,2] + w * m[3,3]
        x = tmp1
        y = tmp2
        z = tmp3
    }

    /**
     * Rotates a cartesian vector by using a quaternion.
     * If the current vector instance is not in the cartesian coordinate system, the [toCartesian]
     * -method is called automatically.
     */
    fun rotate(q: Quaternion) {
        if (!isCartesian) toCartesian()
        tmp1 = x*(q.x*q.x+q.w*q.w-q.y*q.y-q.z*q.z)+y*(2f*q.x*q.y-2f*q.w*q.z)+z*(2f*q.x*q.z+2f*q.w*q.y)
        tmp2 = x*(2f*q.w*q.z+2f*q.x*q.y)+y*(q.w*q.w-q.x*q.x+q.y*q.y-q.z*q.z)+z*(-2f*q.w*q.x+2f*q.y*q.z)
        z = x*(-2f*q.w*q.y+2f*q.x*q.z)+y*(2f*q.w*q.x+2f*q.y*q.z)+z*(q.w*q.w-q.x*q.x-q.y*q.y+q.z*q.z)
        x = tmp1
        y = tmp2
    }

    /**
     * Returns the values of this Vector4 as a FloatArray with four floats
     *
     */
    fun toFloatArray(): FloatArray = floatArrayOf(x,y,z,w)

    /**
     * Returns the values of this Vector4 as a renderscript Float3 structure,
     * the w component is ignored
     */
    fun toFloat3(): Float3 = Float3(x,y,z)

    /**
     * Returns the values of this Vector4 as a renderscript Float4 structure
     */
    fun toFloat4(): Float4 = Float4(x,y,z,w)


    companion object {

        /** Calculates the dot-product of two Vector4 instances. */
        fun dot(a: Vector4, b: Vector4): Float = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w

        /**
         * Calculates the cross-product of two Vector4 instances.
         * The vector instances should be in the cartesian coordinate system, because
         * the w coordinate is ignored in the calculation since the cross-product only has
         * the orthogonality property in three and seven dimensional spaces.
         * This function creates a new Vector4 instance as a result of the calculation.
         *
         */
        fun cross(a: Vector4, b: Vector4): Vector4 {
            var retval = Vector4(a)
            retval.crossWith(b)
            return retval
        }

        /**
         * Calculates the angle (in radians) between two vectors.
         * If any of the two vectors is a zero-vector, zero is returned as the angle.
         *
         */
        fun angle(a: Vector4, b: Vector4): Float {
            tmp2 = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w
            tmp3 = a.lengthSq * b.lengthSq
            if (tmp3 near 0f) /*(tmp3 > -1e-5f && tmp3 < 1e-5f)*/ {
                // we may have zero vectors!
                // TODO: log error (zero vector can not be used to calculate an angle)
                return 0.0f
            }
            return acos( tmp2 / sqrt(tmp3) )
        }

        /** Calculates the squared distance from position [a] to position [b] */
        fun distanceSq(a: Position, b: Position): Float {
            return (b.x - a.x) * (b.x - a.x) +
                   (b.y - a.y) * (b.y - a.y) +
                   (b.z - a.z) * (b.z - a.z) +
                   (b.w - a.w) * (b.w - a.w)
        }

        /** Calculates the distance from position [a] to position [b] */
        fun distance(a: Position, b: Position): Float {
            return sqrt(
                (b.x - a.x) * (b.x - a.x) +
                   (b.y - a.y) * (b.y - a.y) +
                   (b.z - a.z) * (b.z - a.z) +
                   (b.w - a.w) * (b.w - a.w))
        }

        /** A Vector4 with all of the component values at zero. */
        val zero by lazy { Vector4(0.0f, 0.0f, 0.0f, 0.0f) }

        /** Unit vector along the X axis. */
        val xUnit by lazy { Vector4( 1.0f, 0.0f, 0.0f, 0.0f) }

        /** Unit vector along the Y axis. */
        val yUnit by lazy { Vector4( 0.0f, 1.0f, 0.0f, 0.0f) }

        /** Unit vector along the Z axis. */
        val zUnit by lazy { Vector4( 0.0f, 0.0f, 1.0f, 0.0f) }

        // some static helpers:
        protected var tmp1: Float = 0.0f
        protected var tmp2: Float = 0.0f
        protected var tmp3: Float = 0.0f
        protected var tmp4: Float = 0.0f

        // The following should be replaced/removed:
        protected val _df = DecimalFormat("#0.0000")
    }

    override fun toString(): String = "Vector4(${_df.format(x)}, ${_df.format(y)}, ${_df.format(z)}, ${_df.format(w)})"

    // NOTE: this generated function is unsafe to use with collections that use hash codes as keys
    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        result = 31 * result + z.hashCode()
        result = 31 * result + w.hashCode()
        return result
    }

}

// some extensions:
operator fun Int.times(v: Vector4) = Vector4(v.x * this, v.y * this, v.z * this, v.w * this)
operator fun Float.times(v: Vector4) = Vector4(v.x * this, v.y * this, v.z * this, v.w * this)

// just to not to confuse 3D positions with directions
typealias Position = Vector4
