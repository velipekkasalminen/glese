package com.velipekkasalminen.glese


import android.opengl.GLES20
import java.nio.*


// NOTE: 'Mesh' is just a fancy wrapper for the (vertex and index) buffer objects


/**
 * Mesh is a container for the buffer object handles
 * that are used to render models.
 *
 * The buffers are in 'static draw' mode, and should
 * not be modified after initialization via the [construct]
 * method.
 *
 */
class Mesh(val name: String): IDeletable {

    private var _vertexCount: Int = 0
    private var _triangleCount: Int = 0

    // element size, in bytes, of the index buffer:
    private var _iboBytesPerElement: Int = 0
    // allowed values for 'BytesPerElement': 1 (BYTE), 2 (SHORT), 4 (INT)
    // now using only 2 or 4

    // is this mesh ready to be used?
    private var _buffersInitialized: Boolean = false

    /*
        from Khronos OpenGL-Refpages:

        "Buffer object names are unsigned integers. The value zero is reserved, but there is no
        default buffer object for each buffer object target. Instead, buffer set to zero
        effectively unbinds any buffer object previously bound, and restores client memory usage
        for that buffer object target. Buffer object names and the corresponding buffer object
        contents are local to the shared object space of the current GL rendering context."

    */

    // Thus we init buffer object handles to zero (= pointer to null):
    // vertex buffer handle (vx, vy, vz): VBO type
    @GLESIntHandle private var _vbHandle: Int = 0
    // index buffer handle (triangles from vertices): IBO type
    @GLESIntHandle private var _ibHandle: Int = 0
    // normal buffer handle (nx, ny, nz): VBO type
    @GLESIntHandle private var _nbHandle: Int = 0
    // vertex color buffer handle (r, g, b, a): VBO type
    @GLESIntHandle private var _cbHandle: Int = 0
    // texture coord. buffer handle ('u, v' or 's, t'): VBO type
    @GLESIntHandle private var _tbHandle: Int = 0


    // simple (read only) getters for the private fields:

    /** The number of vertices this mesh has. */
    val vertexCount get() = _vertexCount

    /** The number of triangles this mesh has. */
    val triangleCount get() = _triangleCount

    /** The size, in bytes, of a single element in the index buffer. */
    val bytesPerIndexElement get() = _iboBytesPerElement

    /** True, if the mesh has been initialized and is ready to be used. */
    override val isInitialized get() = _buffersInitialized

    @GLESIntHandle val vertexBufferHandle: Int get() = _vbHandle
    @GLESIntHandle val indexBufferHandle: Int get() = _ibHandle
    @GLESIntHandle val normalBufferHandle: Int get() = _nbHandle
    @GLESIntHandle val colorBufferHandle: Int get() = _cbHandle
    @GLESIntHandle val textureCoordinateBufferHandle: Int get() = _tbHandle

    private fun setFloatBufferData(@GLESIntHandle bufferHandle: Int, bufferType: Int, bufferUsage: Int, sizeInBytes: Int, dataArr: FloatArray): Boolean {
        if (sizeInBytes < 1) return false
        var bbuff = ByteBuffer.allocateDirect(sizeInBytes)
        bbuff.order(ByteOrder.nativeOrder()) // <- use the native order of the hardware!

        var fbuff = bbuff.asFloatBuffer()
        fbuff.put(dataArr)
        fbuff.position(0)

        GLES20.glBindBuffer(bufferType, bufferHandle)
        GLES20.glBufferData(bufferType, sizeInBytes, fbuff, bufferUsage)
        return true
    }

    /**
     * Constructs the mesh (buffers for the gpu) from the supplied arrays.
     * The arrays can be garbage collected after this operation, since the buffers exist on the
     * gpu side.
     *
     * @param[vertexArray] An array of vertices, three floats per vertex (x, y and z, in this order).
     * @param[indexArray] An array of non-negative integers referencing the vertices (three indices form a triangle).
     * @param[allowShortIndices] Set to true by default: allows the use 16 bit integers, instead of 32 bit integers, for the index buffer.
     * @param[normalArray] An array of vertex normals, the size must correspond with the vertex array. There are three floats per normal (x, y and z, in this order).
     * @param[texCoordArray] An array of texture coordinates for the vertices. There are two texture coordinates per vertex ((u, v) or (s, t) coordinates).
     * @param[vertexColorArray] An array of colors for the vertices. There are four color channels per vertex (r, g, b and a, in this order).
     *
     * @return Zero on success, otherwise returns one of the MESH_* error codes or GL_* error codes.
     */
    fun construct(
        vertexArray: FloatArray,
        indexArray: IntArray,
        allowShortIndices: Boolean = true,
        normalArray: FloatArray? = null,
        texCoordArray: FloatArray? = null,
        vertexColorArray: FloatArray? = null
    ):Int {
        if (_buffersInitialized) return MESH_ALREADY_INITIALIZED

        // simple integrity checks:
        // we must have at least one triangle:
        _vertexCount = if (vertexArray.size % 3 == 0 && vertexArray.size >= 9)
            vertexArray.size / 3
        else return MESH_INVALID_VERTEX_ARRAY_SIZE

        // TODO: check that the vertexArray does not contain stupid values, such as NaN or Infinity
        // ---> also could calculate the minimum bounding box...

        // only triangles are allowed, quads or others are not:
        _triangleCount = if (indexArray.size % 3 == 0 && indexArray.size >= 3)
            indexArray.size / 3
        else return MESH_INVALID_INDEX_ARRAY_SIZE


        // TODO: check that the indexArray values do not exceed (vertexCount-1)
        // ---> if one of them does exceed, we have a bad index that points outside the vertexArray!

        // we must have vertex and index array!
        var buffCount = 2
        var nextBuffIndex = 2

        // do we have a normal array?
        if (normalArray != null) {
            buffCount++
            // each vertex must have a normal:
            if (normalArray.size != vertexArray.size) return MESH_INVALID_NORMAL_ARRAY_SIZE
        }

        // do we have an 'uv' or 'st' array?
        if (texCoordArray != null) {
            buffCount++
            // there must be two floats (uv/st) per vertex:
            if (texCoordArray.size != 2*_vertexCount) return MESH_INVALID_TEXCOORD_ARRAY_SIZE
        }

        // do we have colors for the vertices?
        if (vertexColorArray != null) {
            buffCount++
            // there must be four floats (rgba) per vertex:
            if (vertexColorArray.size != 4*_vertexCount) return MESH_INVALID_COLOR_ARRAY_SIZE
        }

        var tmpHandleArr = IntArray(buffCount)

        // generate buffer object names (=handles):
        GLES20.glGenBuffers(buffCount, tmpHandleArr, 0)

        // set buffer object handles:
        _vbHandle = tmpHandleArr[0]
        _ibHandle = tmpHandleArr[1]

        if (_vbHandle == 0 || _ibHandle == 0) {
            // if the buffer handles point to zero, something has gone HORRIBLY WRONG!
            delete()
            return MESH_ERRONEOUS_BUFFER_HANDLE
        }

        if (normalArray != null) {
            _nbHandle = tmpHandleArr[nextBuffIndex]
            if (_nbHandle == 0) {
                delete()
                return MESH_ERRONEOUS_BUFFER_HANDLE
            }
            nextBuffIndex++
        }

        if (texCoordArray != null) {
            _tbHandle = tmpHandleArr[nextBuffIndex]
            if (_tbHandle == 0) {
                delete()
                return MESH_ERRONEOUS_BUFFER_HANDLE
            }
            nextBuffIndex++
        }
        if (vertexColorArray != null) {
            _cbHandle = tmpHandleArr[nextBuffIndex]
            if (_cbHandle == 0) {
                delete()
                return MESH_ERRONEOUS_BUFFER_HANDLE
            }
        }

        // At this point, everything seems to be OK
        // --> Start binding the buffers.

        val buffType = GLES20.GL_ARRAY_BUFFER
        val buffUsage = GLES20.GL_STATIC_DRAW

        var sizeInBytes = vertexArray.size * 4

        // bind and init vertex buffer:
        if (!setFloatBufferData(_vbHandle, buffType, buffUsage, sizeInBytes, vertexArray)) return MESH_INVALID_VERTEX_ARRAY_SIZE

        // vertex buffer at position 0 has 3 floats per element (un-normalized) with a stride of 0 (tightly packed, same as setting '12' or MESH_VERTEX_STRIDE) and an offset of 0
        GLES20.glVertexAttribPointer(VERTEX_ATTRIBUTE_POSITIONS, MESH_COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0)

        // TODO: check if we succeeded?
        // glGetBufferParameteriv(GLES20.GL_ARRAY_BUFFER, GLES20.GL_ARRAY_SIZE, someIntBuffer, 0)
        // someIntBuffer[0] must be same as sizeInBytes
        // or: if glGetError() returns no error

        // normal buffer:
        normalArray?.let {
            // sizeInBytes is the same with the vertexArray
            if (!setFloatBufferData(_nbHandle, buffType, buffUsage, sizeInBytes, it)) return MESH_INVALID_INDEX_ARRAY_SIZE

            // normal buffer has 3 floats per element (already normalized) with a stride of 0 (tightly packed) and an offset of 0
            GLES20.glVertexAttribPointer(VERTEX_ATTRIBUTE_NORMALS, MESH_COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0)
        }

        // texCoord buffer:
        texCoordArray?.let {
            sizeInBytes = it.size * 4
            if (!setFloatBufferData(_tbHandle, buffType, buffUsage, sizeInBytes, it)) return MESH_INVALID_TEXCOORD_ARRAY_SIZE

            // texCoord buffer has 2 floats per element with a stride of 0 (tightly packed) and an offset of 0
            GLES20.glVertexAttribPointer(VERTEX_ATTRIBUTE_TEXTURE_COORDS, MESH_TEX_COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0)
        }

        // vertex color buffer:
        vertexColorArray?.let {
            sizeInBytes = it.size * 4
            if (!setFloatBufferData(_cbHandle, buffType, buffUsage, sizeInBytes, it)) return MESH_INVALID_COLOR_ARRAY_SIZE

            // color buffer has 4 floats per element with a stride of 0 (tightly packed) and an offset of 0
            GLES20.glVertexAttribPointer(VERTEX_ATTRIBUTE_COLORS, MESH_COLOR_CHANNELS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0)
        }

        // stop bondage play with the float buffs:
        GLES20.glBindBuffer(buffType, 0)


        // bind and init index buffer:
        if (allowShortIndices && indexArray.size < 32767) { // <-- using SIGNED short here :(
            // make a ShortBuffer for smaller meshes:
            val shortIndexArray = (indexArray.map { it.toShort() }).toShortArray()

            _iboBytesPerElement = 2

            sizeInBytes = shortIndexArray.size * _iboBytesPerElement

            var bbuff = ByteBuffer.allocateDirect(sizeInBytes)
            bbuff.order(ByteOrder.nativeOrder())

            var sbuff = bbuff.asShortBuffer() // <- no UNSIGNED short buffers !?
            sbuff.put(shortIndexArray)
            sbuff.position(0)

            GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, _ibHandle)
            GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, sizeInBytes, sbuff, buffUsage)
        } else {
            // just use an IntBuffer for large meshes:

            _iboBytesPerElement = 4

            sizeInBytes = indexArray.size * _iboBytesPerElement

            var bbuff = ByteBuffer.allocateDirect(sizeInBytes)
            bbuff.order(ByteOrder.nativeOrder())

            var ibuff = bbuff.asIntBuffer() // <- no UNSIGNED int buffers...
            ibuff.put(indexArray)
            ibuff.position(0)

            GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, _ibHandle)
            GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, sizeInBytes, ibuff, buffUsage)
        }

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0)

        // glError is returned only once:
        val glError = GLES20.glGetError()
        if (glError != GLES20.GL_NO_ERROR) return glError

        _buffersInitialized = true

        return 0
    }

    /**
     * Called just before this mesh is to be used for rendering
     *
     */
    fun onAboutToRender() {
        // enable the use of attribute arrays:

        // TODO: use glGetAttribLocation when constructing the mesh (and/or glBindAttribLocation in shader) instead of hardcoded values!

        // vertex positions at position 0 (MESH_ATTRIBUTE_VERTEX_POSITIONS) should already be enabled!
        if (_vbHandle != 0) {
            GLES20.glEnableVertexAttribArray(VERTEX_ATTRIBUTE_POSITIONS)
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, _vbHandle)
            GLES20.glVertexAttribPointer(VERTEX_ATTRIBUTE_POSITIONS, MESH_COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0)
        }
        else GLES20.glDisableVertexAttribArray(VERTEX_ATTRIBUTE_POSITIONS)

        // the normals:
        if (_nbHandle != 0) {
            GLES20.glEnableVertexAttribArray(VERTEX_ATTRIBUTE_NORMALS)
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, _nbHandle)
            GLES20.glVertexAttribPointer(VERTEX_ATTRIBUTE_NORMALS, MESH_COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0)
        }
        else GLES20.glDisableVertexAttribArray(VERTEX_ATTRIBUTE_NORMALS)

        // texture coordinates (uv or st):
        if (_tbHandle != 0) {
            GLES20.glEnableVertexAttribArray(VERTEX_ATTRIBUTE_TEXTURE_COORDS)
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, _tbHandle)
            GLES20.glVertexAttribPointer(VERTEX_ATTRIBUTE_TEXTURE_COORDS, MESH_TEX_COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0)
        }
        else GLES20.glDisableVertexAttribArray(VERTEX_ATTRIBUTE_TEXTURE_COORDS)

        // vertex colors:
        if (_cbHandle != 0) {
            GLES20.glEnableVertexAttribArray(VERTEX_ATTRIBUTE_COLORS)
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, _cbHandle)
            GLES20.glVertexAttribPointer(VERTEX_ATTRIBUTE_COLORS, MESH_COLOR_CHANNELS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0)
        }
        else GLES20.glDisableVertexAttribArray(VERTEX_ATTRIBUTE_COLORS)

        // bind the index array!
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, _ibHandle)

    }

    fun render() {

        // notice the GL_ELEMENT_ARRAY_BUFFER bind above,
        // also note that only GL_UNSIGNED_* is allowed here
        // ALSO note that the 'element count' means the number of element array buffer indices to draw (triangle count * 3)
        when (_iboBytesPerElement) {
            1 -> GLES20.glDrawElements(GLES20.GL_TRIANGLES, _triangleCount * 3, GLES20.GL_UNSIGNED_BYTE, 0)
            2 -> GLES20.glDrawElements(GLES20.GL_TRIANGLES, _triangleCount * 3, GLES20.GL_UNSIGNED_SHORT, 0)
            4 -> GLES20.glDrawElements(GLES20.GL_TRIANGLES, _triangleCount * 3, GLES20.GL_UNSIGNED_INT, 0)
        }

    }

    /**
     * Tells OpenGL ES to delete the associated buffers via handles.
     * The handles are then reset to point to nothing.
     */
    override fun delete() {

        // relying on the 'zero is ignored' rule of glDeleteBuffers:
        GLES20.glDeleteBuffers(5, intArrayOf(_vbHandle, _ibHandle, _nbHandle, _tbHandle, _cbHandle) ,0)

        _vbHandle = 0
        _ibHandle = 0
        _nbHandle = 0
        _tbHandle = 0
        _cbHandle = 0

        _vertexCount = 0
        _triangleCount = 0
        _iboBytesPerElement = 0
        _buffersInitialized = false
    }


    override fun toString(): String =
        "${name}(vertexCount:${_vertexCount}, triangleCount:${_triangleCount}, vertexBuffer:${_vbHandle!=0}, indexBuffer:${_ibHandle!=0}, shortIndices:${_iboBytesPerElement==2}, normalBuffer:${_nbHandle!=0}, colorBuffer:${_cbHandle!=0}, textureCoordinateBuffer:${_tbHandle!=0})"

}

