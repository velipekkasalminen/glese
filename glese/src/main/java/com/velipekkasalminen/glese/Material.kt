package com.velipekkasalminen.glese


import android.opengl.GLES20


/**
 * Material consists of textures, colors and other properties. Non-null reference to
 * an initialized [ShaderProgram] is required. Material is usually used to render a [Model3D]
 *
 */
class Material(val name: String, val shaderProgram: ShaderProgram): IDeletable {

    private var _initialized: Boolean

    // Contains all of the 'default' properties and attributes:
    private val _dProp: MaterialProperties

    // Contains the state changing functions called at the beginning of the use of this material.
    private val _mFunc: MutableMap<OpenGLStateFunction, Any> = mutableMapOf()

    // Uniform locations for the matrices:
    private var _mvpMatrixUniform: Int
    private var _normalMatrixUniform: Int

    init {
        if (shaderProgram.isInitialized) {
            _initialized = true
            _mvpMatrixUniform = GLES20.glGetUniformLocation(shaderProgram.programHandle, "mvpMatrix")
            _normalMatrixUniform = GLES20.glGetUniformLocation(shaderProgram.programHandle, "normalMatrix")
            // TODO: search the shader for more default uniforms, such as 'mvMatrix' and others
        } else {
            //Log.d("MATERIAL","Shader is not initialized!")
            _initialized = false
            _mvpMatrixUniform = -1
            _normalMatrixUniform = -1
            throw Exception("The supplied shader program has not been initialized. Call the construct -method to initialize the program.")
        }
        _dProp = MaterialProperties(shaderProgram)
    }


    /**
     * The default material properties.
     *
     */
    val defaultProperties get() = _dProp

    /**
     * The model-view-projection matrix uniform location on the vertex shader.
     * The value will be -1 if the shader does not have the matrix uniform.
     */
    val mvpMatrixUniform get() = _mvpMatrixUniform

    /**
     * The normal matrix (that is an inverted transposed 'model matrix') uniform location on the vertex shader.
     * The value will be -1 if the shader does not have the matrix uniform.
     */
    val normalMatrixUniform get() = _normalMatrixUniform

    /**
     * The render queue used for rendering objects with this material;
     * one of [RenderQueueLocation.SOLIDGEOMETRY], [RenderQueueLocation.ALPHATESTED],
     * [RenderQueueLocation.SCREENSPACE] or [RenderQueueLocation.ADDITIVE].
     *
     */
    var renderQueueLocation: RenderQueueLocation = RenderQueueLocation.UNKNOWN

    override val isInitialized get() = _initialized

    /**
     * Clears the [defaultProperties] and the list of OpenGL state changes on this material.
     * This will not delete the shader program or any textures associated with this material.
     */
    override fun delete() {
        // not much here...
        // Do not delete shaders or any other stuff here.
        _initialized = false
        _dProp.clear()
        _mFunc.clear()
    }

    /**
     * Adds an OpenGL state change to this material. The state changes are enacted when
     * rendering with this material begins. Note that the validity of [param] is not checked
     * by this method, nor the compatibility of the state change with the shader program.
     */
    fun addStateChange(func: OpenGLStateFunction, param: Any): Boolean {
        if (!_initialized || _mFunc.containsKey(func)) return false
        _mFunc[func] = param
        return true
    }


    /**
     * Begins using this material for the renderable objects.
     */
    fun begin(): Int {
        if (!_initialized) return MATERIAL_NOT_INITIALIZED

        if (_mFunc.isNotEmpty()) {
            _mFunc.forEach {
                Engine.changeState(it.key, it.value)
            }
        }

        shaderProgram.setActive()
        return 0
    }

    /**
     * Ends the use of this material, a companion for the [begin] function.
     * @param[next] The next material to be used, or null.
     */
    fun end(next: Material? = null): Int {
        if (!_initialized) return MATERIAL_NOT_INITIALIZED
        if (_mFunc.isNotEmpty()) {
            if (next != null) {
                // reset the state only if the next material property map does not contain the
                // same OpenGlStateFunction, since the next material will change states anyway.
                _mFunc.forEach {
                    if (!next._mFunc.containsKey(it.key)) Engine.resetState(it.key)
                }
            } else {
            // just reset all states that were changed:
                _mFunc.forEach {
                    Engine.resetState(it.key)
                }
            }
        }
        return 0
    }

    // see the return value of MaterialProperties.sendUniformDataToShader()
    fun onAboutToDraw(customizedMaterialProperties: MaterialProperties? = null): Int {
        if (!_initialized) return MATERIAL_NOT_INITIALIZED

        // NOTE: setActive() must be called first on the shader program before calling this method,
        //       this is done in the begin() function.

        // use the default props if the customized props are null:
        return customizedMaterialProperties?.sendUniformDataToShader() ?: _dProp.sendUniformDataToShader()
    }

    override fun toString(): String = "${name}(shaderProgram:${shaderProgram.name}, propertiesCount:${_dProp.count})"

}

