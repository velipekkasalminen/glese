package com.velipekkasalminen.glese


/**
 * Scene class is responsible for managing, rendering and updating the contained Object3D objects.
 * Each Scene has a map of objects it contains; objects can be added to this map via the [addObject]
 *  -method. To transfer an object to another scene, use the [transferObject] -method.
 * Objects that are no longer needed should be removed via the [removeObject] -method.
 * HINT: The inheritors of the Scene class should implement custom object pools.
 */
open class Scene(val name: String) {

    /** The camera that is used with rendering the scene. */
    var activeCamera = Camera3D(name + "_mainCamera")

    // used to sort the objects in _alphaTestedList by distance to camera:
    private data class FloatSortable(var f: Float, val objKey: Int)
    private fun floatSortableSelector(fs: FloatSortable): Float = fs.f

    // simple map of objects: ( key = Object3D.name.hashCode() )
    private val _objects: MutableMap<Int, Object3D> = mutableMapOf<Int, Object3D>()

    // rendering for solid geometry: ( <Material.name.hashCode, list of Object3D.name.hashCodes > )
    private val _solidGeometryList: MutableMap<Int, MutableList<Int>> = mutableMapOf<Int, MutableList<Int>>()
    // rendering for alpha tested and sorted objects:
    private val _alphaTestedList: MutableMap<Int, MutableList<FloatSortable>> = mutableMapOf<Int, MutableList<FloatSortable>>()
    // rendering for additively rendered objects:
    private val _additiveList: MutableMap<Int, MutableList<Int>> = mutableMapOf<Int, MutableList<Int>>()
    // rendering for screen space objects:
    private val _screenSpaceList: MutableMap<Int, MutableList<Int>> = mutableMapOf<Int, MutableList<Int>>()

    /** The number of Object3D objects added to this scene. */
    val objectCount: Int get() = _objects.count()


    /**
     * Adds an Object3D object to this scene to be automatically updated and rendered.
     * This method determines the 'render queue location' for the object, thus the object
     * should have defined a non-null sharedModel, and that shared model should have
     * a sharedMaterial defined. Otherwise the object will not be rendered.
     * To change the sharedModel (or the material of it), the object should be first
     * removed from the scene with [removeObject] and add it back in when the change
     * is successfully done.
     */
    fun addObject(obj: Object3D): Boolean {
        val h = obj.name.hashCode()
        if (_objects.containsKey(h)) return false

        _objects[h] = obj

        // determine the rendering list to add to:
        if (obj.sharedModel?.sharedMaterial != null) {
            val loc = obj.sharedModel!!.sharedMaterial!!.renderQueueLocation
            val matHash = obj.sharedModel!!.sharedMaterial!!.name.hashCode()
            when (loc) {
                RenderQueueLocation.SOLIDGEOMETRY -> {
                    if (_solidGeometryList[matHash] == null)
                        _solidGeometryList[matHash] = mutableListOf<Int>()
                    _solidGeometryList[matHash]!!.add(h)
                }
                RenderQueueLocation.ADDITIVE -> {
                    if (_additiveList[matHash] == null)
                        _additiveList[matHash] = mutableListOf<Int>()
                    _additiveList[matHash]!!.add(h)
                }
                RenderQueueLocation.ALPHATESTED -> {
                    if (_alphaTestedList[matHash] == null)
                        _alphaTestedList[matHash] = mutableListOf<FloatSortable>()
                    _alphaTestedList[matHash]!!.add(FloatSortable(
                        Vector4.distanceSq(activeCamera.position, obj.position),h))
                }
                RenderQueueLocation.SCREENSPACE -> {
                    if (_screenSpaceList[matHash] == null)
                        _screenSpaceList[matHash] = mutableListOf<Int>()
                    _screenSpaceList[matHash]!!.add(h)
                }
                else -> { /* Nothing */ }
            }
        }
        // else case: the object is not rendered at all!

        return true
    }

    /** Returns true if this scene contains the [obj] */
    operator fun contains(obj: Object3D) = _objects.contains(obj.name.hashCode())

    /** Returns the named object or null if the object is not found. */
    fun getObject(name: String): Object3D? {
        val h = name.hashCode()
        if (!_objects.containsKey(h)) return null
        return _objects[h]
    }

    /**
     * Transfers a named object from this scene to the named scene.
     * @param[object3DName] The name of the object on this scene to be transferred.
     * @param[sceneName] The name of the receiving scene.
     * @return True if the operation succeeded, false otherwise.
     */
    fun transferObject(object3DName: String, sceneName: String): Boolean {
        if (name == sceneName) return false
        val h = object3DName.hashCode()
        if (!_objects.containsKey(h)) return false
        if (!Engine.scenes.contains(sceneName)) return false
        val sc = Engine.scenes[sceneName]
        if (!_objects[h]?.let { sc.addObject(it) }!!) return false
        return removeObject(object3DName)
    }

    /** Removes the named object from the scene. */
    fun removeObject(name: String): Boolean {
        val h = name.hashCode()
        if (!_objects.containsKey(h)) return false
        _objects.remove(h)

        // remove from the rendering lists:
        _solidGeometryList.forEach { (_, list) -> if ( list.remove(h) ) return true }
        _additiveList.forEach { (_, list) -> if ( list.remove(h) ) return true }
        _screenSpaceList.forEach { (_, list) -> if ( list.remove(h) ) return true }
        _alphaTestedList.forEach { (_, list) ->
            list.forEachIndexed { i: Int, s: FloatSortable ->
                if (s.objKey == h) {
                    list.removeAt(i)
                    return true
                }
            }
        }

        // Here object was not found in the lists?

        return true
    }

    /** Removes the object from the scene. */
    fun removeObject(obj: Object3D) = removeObject(obj.name)

    fun removeAllObjects() {
        _objects.clear()
        _solidGeometryList.clear()
        _additiveList.clear()
        _alphaTestedList.clear()
        _screenSpaceList.clear()
    }

    open fun onCreate() {
        // called when this scene is added via Engine.scenes.add
        // pre-load/calculate things for this scene here
    }

    open fun onStart() {
        // called when Engine.scenes.load is called on this object
    }

    open fun onUnload() {
        // called when this scene is being 'unloaded' (before another scene is loaded)
    }

    open fun onDelete() {
        // called when this scene is being deleted
        // release the resources that will no longer be used here
    }

    open fun onUpdate(frameTime: Float) {
        // called before this scene is updated
    }

    fun update(frameTime: Float) {
        _objects.forEach { (_ , o) ->
            if (o.updateEnabled) o.update(frameTime)
            if (!o.isStationary) o.calculateTransformations()
        }
        if (!activeCamera.isStationary) activeCamera.calculateTransformations()

        // calculate the distance (squared distance is faster!) to the camera, then sort the list:
        _alphaTestedList.forEach { (_, s) ->
            s.forEach {
                it.f = Vector4.distanceSq(activeCamera.position, _objects[it.objKey]!!.position)
            }
            s.sortByDescending { floatSortableSelector(it) }
        }

    }

    open fun onPause() {
        // called when the surface view onPause is called
    }

    open fun onResume() {
        // called when the surface view onResume is called
    }

    open fun onRender() {
        // called before this scene is rendered
    }

    private fun renderList(l: MutableMap<Int, MutableList<Int>>) {
        val size = l.count()

        // current material 'm' (and the next material is 'val n')
        var m: Material? = null

        for (i in 0 until size) {
            // material key:
            val mKey = l.keys.elementAt(i)

            // possible next material key:
            val nKey = l.keys.elementAtOrElse(i+1) {-1}

            // figure out what material to use, and then render with it:
            if (l[mKey]!!.isNotEmpty()) {
                val thisList = l[mKey]

                // HACK: just take the material from the first object:
                m = _objects[ thisList!![0] ]?.sharedModel?.sharedMaterial

                // render with the material (if we have it, and can render with it):
                if (m != null && m.begin() == 0) {
                    thisList.forEach {
                        if (_objects[it]!!.renderEnabled)
                            _objects[it]!!.render(activeCamera)
                    }
                } else continue // <- material is null or broken, skip rendering with this material.

                // end the rendering with this material (with or without a ref to the next material):
                if (nKey != -1 && l[nKey]!!.isNotEmpty()) {
                    val n = _objects[ l[nKey]!!.get(0) ]?.sharedModel?.sharedMaterial
                    m.end(n)
                } else m.end()
            }

        }

    }

    fun render() {

        // render with _solidGeometryList:
        if (_solidGeometryList.isNotEmpty())
            renderList(_solidGeometryList)

        // render with _additiveList:
        if (_additiveList.isNotEmpty())
            renderList(_additiveList)

        // render with _alphaTestedList (modified renderList function):
        if (_alphaTestedList.isNotEmpty()) {
            val size = _alphaTestedList.count()
            var m: Material? = null
            for (i in 0 until size) {
                val mKey = _alphaTestedList.keys.elementAt(i)
                val nKey = _alphaTestedList.keys.elementAtOrElse(i+1) {-1}
                if (_alphaTestedList[mKey]!!.isNotEmpty()) {
                    val thisList = _alphaTestedList[mKey]
                    m = _objects[ thisList!![0].objKey ]?.sharedModel?.sharedMaterial
                    if (m != null && m.begin() == 0) {
                        thisList.forEach {
                            if (_objects[it.objKey]!!.renderEnabled)
                                _objects[it.objKey]!!.render(activeCamera)
                        }
                    } else continue
                    if (nKey != -1 && _alphaTestedList[nKey]!!.isNotEmpty()) {
                        val n = _objects[ _alphaTestedList[nKey]!!.get(0).objKey ]?.sharedModel?.sharedMaterial
                        m.end(n)
                    } else m.end()
                }
            }
        }

        // finally, render with _screenSpaceList:
        if (_screenSpaceList.isNotEmpty())
            renderList(_screenSpaceList)

    }

}

