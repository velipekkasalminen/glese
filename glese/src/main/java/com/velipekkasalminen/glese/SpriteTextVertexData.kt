package com.velipekkasalminen.glese


import android.graphics.RectF
import androidx.annotation.IntRange


/**
 * SpriteTextVertexData is a simple 'sprite' text VertexData factory object intended to help create
 * textured and colored flat meshes for world space texts. The glyphs in the texture must be organized
 * in a specific way to be useful; The glyph grid is 25x15 grid with a strip at the bottom for
 * other symbols. For example: a 1024x1024 font texture has glyph size of 40x64 with one pixel
 * between each glyph, and a strip of twenty 48x48 slots for other symbols.
 * Note that the generated text mesh is immutable, and the text can not be changed.
 */
object SpriteTextVertexData {

    // The following glyphMap is the default hard-coded unicode glyph map for
    // extended latin and basic greek.
    // Map format: <utf16_code: Int, glyph_index: Int>

    /**
     * A map of unicode code points and indexed glyph positions in a texture.
     * The default glyphMap contains glyph mappings for Basic Latin, Latin-1 Supplement and
     * Latin Extended-A unicode blocks and support for Basic Greek.
     * Note that this map must contain an entry for glyphs that are not
     * present in the texture; GLYPH_UNKNOWN (-1) is used as the unicode code point for these glyphs.
     */
    var glyphMap: MutableMap<Int, Int> = mutableMapOf<Int, Int>(
      -1  to 106,   33  to 0,     34  to 1,     35  to 2,     36  to 3,     37  to 4,     38  to 5,     39  to 6,
      40  to 7,     41  to 8,     42  to 9,     43  to 10,    44  to 11,    45  to 12,    46  to 13,    47  to 14,
      48  to 15,    49  to 16,    50  to 17,    51  to 18,    52  to 19,    53  to 20,    54  to 21,    55  to 22,
      56  to 23,    57  to 24,    58  to 25,    59  to 26,    60  to 27,    61  to 28,    62  to 29,    63  to 30,
      64  to 31,    65  to 32,    66  to 33,    67  to 34,    68  to 35,    69  to 36,    70  to 37,    71  to 38,
      72  to 39,    73  to 40,    74  to 41,    75  to 42,    76  to 43,    77  to 44,    78  to 45,    79  to 46,
      80  to 47,    81  to 48,    82  to 49,    83  to 50,    84  to 51,    85  to 52,    86  to 53,    87  to 54,
      88  to 55,    89  to 56,    90  to 57,    91  to 58,    92  to 59,    93  to 60,    94  to 61,    95  to 62,
      96  to 63,    97  to 64,    98  to 65,    99  to 66,    100 to 67,    101 to 68,    102 to 69,    103 to 70,
      104 to 71,    105 to 72,    106 to 73,    107 to 74,    108 to 75,    109 to 76,    110 to 77,    111 to 78,
      112 to 79,    113 to 80,    114 to 81,    115 to 82,    116 to 83,    117 to 84,    118 to 85,    119 to 86,
      120 to 87,    121 to 88,    122 to 89,    123 to 90,    124 to 91,    125 to 92,    126 to 93,    161 to 94,
      162 to 95,    163 to 96,    164 to 97,    165 to 98,    166 to 99,    167 to 100,   168 to 101,   169 to 102,
      170 to 103,   171 to 104,   172 to 105,   174 to 107,   175 to 108,   176 to 109,   177 to 110,   178 to 111,
      179 to 112,   180 to 113,   181 to 114,   182 to 115,   183 to 116,   184 to 117,   185 to 118,   186 to 119,
      187 to 120,   188 to 121,   189 to 122,   190 to 123,   191 to 124,   192 to 125,   193 to 126,   194 to 127,
      195 to 128,   196 to 129,   197 to 130,   198 to 131,   199 to 132,   200 to 133,   201 to 134,   202 to 135,
      203 to 136,   204 to 137,   205 to 138,   206 to 139,   207 to 140,   208 to 141,   209 to 142,   210 to 143,
      211 to 144,   212 to 145,   213 to 146,   214 to 147,   215 to 148,   216 to 149,   217 to 150,   218 to 151,
      219 to 152,   220 to 153,   221 to 154,   222 to 155,   223 to 156,   224 to 157,   225 to 158,   226 to 159,
      227 to 160,   228 to 161,   229 to 162,   230 to 163,   231 to 164,   232 to 165,   233 to 166,   234 to 167,
      235 to 168,   236 to 169,   237 to 170,   238 to 171,   239 to 172,   240 to 173,   241 to 174,   242 to 175,
      243 to 176,   244 to 177,   245 to 178,   246 to 179,   247 to 180,   248 to 181,   249 to 182,   250 to 183,
      251 to 184,   252 to 185,   253 to 186,   254 to 187,   255 to 188,   256 to 189,   257 to 190,   258 to 191,
      259 to 192,   260 to 193,   261 to 194,   262 to 195,   263 to 196,   264 to 197,   265 to 198,   266 to 199,
      267 to 200,   268 to 201,   269 to 202,   270 to 203,   271 to 204,   272 to 141,   273 to 206,   274 to 207,
      275 to 208,   276 to 209,   277 to 210,   278 to 211,   279 to 212,   280 to 213,   281 to 214,   282 to 215,
      283 to 216,   284 to 217,   285 to 218,   286 to 219,   287 to 220,   288 to 221,   289 to 222,   290 to 223,
      291 to 224,   292 to 225,   293 to 226,   294 to 227,   295 to 228,   296 to 229,   297 to 230,   298 to 231,
      299 to 232,   300 to 233,   301 to 234,   302 to 235,   303 to 236,   304 to 237,   305 to 238,   306 to 239,
      307 to 240,   308 to 241,   309 to 242,   310 to 243,   311 to 244,   312 to 345,   313 to 246,   314 to 247,
      315 to 248,   316 to 249,   317 to 250,   318 to 251,   319 to 252,   320 to 253,   321 to 254,   322 to 255,
      323 to 256,   324 to 257,   325 to 258,   326 to 259,   327 to 260,   328 to 261,   329 to 262,   330 to 263,
      331 to 264,   332 to 265,   333 to 266,   334 to 267,   335 to 268,   336 to 269,   337 to 270,   338 to 271,
      339 to 272,   340 to 273,   341 to 274,   342 to 275,   343 to 276,   344 to 277,   345 to 278,   346 to 279,
      347 to 280,   348 to 281,   349 to 282,   350 to 283,   351 to 284,   352 to 285,   353 to 286,   354 to 287,
      355 to 288,   356 to 289,   357 to 290,   358 to 291,   359 to 292,   360 to 293,   361 to 294,   362 to 295,
      363 to 296,   364 to 297,   365 to 298,   366 to 299,   367 to 300,   368 to 301,   369 to 302,   370 to 303,
      371 to 304,   372 to 305,   373 to 306,   374 to 307,   375 to 308,   376 to 309,   377 to 310,   378 to 311,
      379 to 312,   380 to 313,   381 to 314,   382 to 315,   383 to 316,   8208 to 12,   8209 to 12,   8210 to 12,
      8211 to 12,   8212 to 12,   8213 to 12,   8216 to 63,   8217 to 113,  8220 to 1,    8221 to 1,    8223 to 1,
      8260 to 14,   8722 to 12,   8725 to 14,   8727 to 9,    8729 to 106,  8739 to 91,   8764 to 93,   10005 to 148,
      10006 to 148, 10088 to 7,   10090 to 7,   10098 to 7,   10089 to 8,   10091 to 8,   10099 to 8,   10100 to 90,
      10101 to 92,  8383 to 317,  8363 to 318,  8369 to 319,  8377 to 320,  8361 to 321,  8730 to 322,  8734 to 323,
      8776 to 324,  8800 to 325,  894 to 26,    915 to 326,   916 to 327,   920 to 328,   923 to 329,   926 to 330,
      928 to 331,   931 to 332,   934 to 333,   936 to 334,   937 to 335,   945 to 336,   946 to 337,   947 to 338,
      948 to 339,   949 to 340,   950 to 341,   951 to 342,   952 to 343,   953 to 344,   954 to 345,   955 to 346,
      957 to 347,   958 to 348,   960 to 349,   961 to 350,   963 to 351,   964 to 352,   965 to 353,   966 to 354,
      967 to 355,   968 to 356,   969 to 357,   8381 to 358,  8372 to 359,  8378 to 360,  399 to 205,   601 to 245,
      7498 to 245,  8340 to 245,  962 to 361,   977 to 362,   978 to 363,   982 to 364,   439 to 365,   658 to 366,
      494 to 367,   495 to 368,   913 to 32,    914 to 33,    917 to 36,    918 to 57,    919 to 39,    921 to 40,
      922 to 42,    924 to 44,    925 to 45,    927 to 46,    929 to 47,    932 to 52,    933 to 56,    935 to 55,
      956 to 114,   959 to 78,    1015 to 155,  1016 to 187,  1018 to 44)

    // TODO: add glyphWidthMap (key=glyphIndex:Int, value=width:Float) for non-monospace fonts,
    //       for monospace fonts: width=1f.  Note that final wsize = 0.625f * glyphWidthMap[glyphIndex]

    /** The color of the top left corner of each glyph. */
    var colorTopLeft = ColorRGBA(1f,1f,1f,1f)

    /** The color of the top right corner of each glyph. */
    var colorTopRight = ColorRGBA(1f,1f,1f,1f)

    /** The color of the bottom left corner of each glyph. */
    var colorBottomLeft = ColorRGBA(1f,1f,1f,1f)

    /** The color of the bottom right corner of each glyph. */
    var colorBottomRight = ColorRGBA(1f,1f,1f,1f)

    /**
     * Sets the color of each glyph.
     * Overwrites [colorTopLeft], [colorTopRight], [colorBottomLeft] and [colorBottomRight]
     * with the same color [c].
     */
    fun setColor(c: ColorRGBA) {
        colorTopLeft = c
        colorTopRight = c
        colorBottomLeft = c
        colorBottomRight = c
    }

    /**
     * Sets the top and bottom colors of each glyph.
     * Overwrites [colorTopLeft] and [colorTopRight] with [topColor], and
     * [colorBottomLeft] and [colorBottomRight] with [bottomColor].
     */
    fun setColors(topColor: ColorRGBA, bottomColor: ColorRGBA) {
        colorTopLeft = topColor
        colorTopRight = topColor
        colorBottomLeft = bottomColor
        colorBottomRight = bottomColor
    }

    /**
     * Creates a VertexData object based on the given [text] string.
     * Remember to set the colors before calling this function.
     *
     * @param[text] Unicode (utf16) text string. This string should not be empty or blank.
     * @param[glyphSize] The size (height) of the glyphs in world space. For screen/view space the glyph size should be around 0.05
     * @param[positionOffset] The offset of the starting position, this is (0,0) by default.
     * @return VertexData object that can be used to create a Mesh.
     */
    fun create(text: String, glyphSize: Float= 1f, positionOffset: Position = Position()): VertexData {
        if(text.isBlank()) throw IllegalArgumentException("Can not create vertex data from an empty or blank text.")
        if (glyphSize < 0.0001f) throw IllegalArgumentException("Can not create vertex data from too small glyphs.")
        val vertices = mutableListOf<Float>() // (x, y, z)
        val normals = mutableListOf<Float>() // (nx, ny, nz)
        val tCoords = mutableListOf<Float>() // (s, t)
        val colors = mutableListOf<Float>() // (r, g, b, a)
        val indices = mutableListOf<Int>() // vertex indices per triangle
        var i = 0
        val pos = Position(positionOffset.x, positionOffset.y, positionOffset.z)

        // monospace glyph width is 62.5% of height (size):
        val wsize = 0.625f * glyphSize
        val chars = text.toCharArray()

        // helper function to add the actual glyph quad:
        val addQuad:(Position,Int) -> Unit = {
             position: Position, glyphIndex: Int ->

            // top left:
            vertices.add(position.x)
            vertices.add(position.y)
            vertices.add(position.z)
            tCoords.add(((glyphIndex % 25) * 41) * 9.765625e-4f)
            tCoords.add((1024 - (glyphIndex / 25) * 65) * 9.765625e-4f)
            colors.add(colorTopLeft.r)
            colors.add(colorTopLeft.g)
            colors.add(colorTopLeft.b)
            colors.add(colorTopLeft.a)
            // here:  i = vertices.size / 3

            // top right:
            vertices.add(position.x + wsize)
            vertices.add(position.y)
            vertices.add(position.z)
            tCoords.add(((glyphIndex % 25) * 41) * 9.765625e-4f + 3.90625e-2f)
            tCoords.add((1024 - (glyphIndex / 25) * 65) * 9.765625e-4f)
            colors.add(colorTopRight.r)
            colors.add(colorTopRight.g)
            colors.add(colorTopRight.b)
            colors.add(colorTopRight.a)

            // bottom left:
            vertices.add(position.x)
            vertices.add(position.y + glyphSize)
            vertices.add(position.z)
            tCoords.add(((glyphIndex % 25) * 41) * 9.765625e-4f)
            tCoords.add((1024 - (glyphIndex / 25) * 65) * 9.765625e-4f - 6.25e-2f)
            colors.add(colorBottomLeft.r)
            colors.add(colorBottomLeft.g)
            colors.add(colorBottomLeft.b)
            colors.add(colorBottomLeft.a)

            // bottom right:
            vertices.add(position.x + wsize)
            vertices.add(position.y + glyphSize)
            vertices.add(position.z)
            tCoords.add(((glyphIndex % 25) * 41) * 9.765625e-4f + 3.90625e-2f)
            tCoords.add((1024 - (glyphIndex / 25) * 65) * 9.765625e-4f - 6.25e-2f)
            colors.add(colorBottomRight.r)
            colors.add(colorBottomRight.g)
            colors.add(colorBottomRight.b)
            colors.add(colorBottomRight.a)

            normals.addAll(listOf(0f, 0f, -1f, 0f, 0f, -1f, 0f, 0f, -1f, 0f, 0f, -1f))

            // two triangles:
            indices.addAll(listOf(i, i+1, i+3, i, i+3, i+2))

            i+=4
        }

        for (c in chars) {
            val g = getGlyphIndex(c.toInt())
            when (g) {
                GLYPH_UNKNOWN, GLYPH_SPACE -> pos.x += wsize
                GLYPH_HORIZONTAL_TAB -> pos.x += 4 * wsize
                GLYPH_NEW_LINE -> {
                    pos.y += glyphSize
                    pos.x = positionOffset.x
                }
                GLYPH_VERTICAL_TAB -> {
                    pos.y += 2 * glyphSize
                    pos.x = positionOffset.x
                }
                // GLYPH_CARRIAGE_RETURN -> do nothing
                else -> {
                    addQuad(pos,g)
                    pos.x += wsize
                }
            }
        }

        // the vertices list stores vertex positions in values of three (x, y, z)
        val vertexCount = vertices.count() / 3

        // assert that  indices.count() / 3 == vertexCount / 2

        val retval = VertexData(vertexCount,vertexCount / 2)

        if (!retval.setVertices(vertices.toFloatArray())) throw Exception("VertexData.setVertices failed in SpriteTextVertexData.create")
        if (!retval.setIndices(indices.toIntArray())) throw Exception("VertexData.setIndices failed in SpriteTextVertexData.create")
        if (!retval.setTextureCoordinates(tCoords.toFloatArray())) throw Exception("VertexData.setTextureCoordinates failed in SpriteTextVertexData.create")
        if (!retval.setNormals(normals.toFloatArray())) throw Exception("VertexData.setNormals failed in SpriteTextVertexData.create")
        if (!retval.setVertexColors(colors.toFloatArray())) throw Exception("VertexData.setVertexColors failed in SpriteTextVertexData.create")

        // this may not be necessary, but let's do it anyway:
        vertices.clear()
        normals.clear()
        tCoords.clear()
        colors.clear()
        indices.clear()

        // all done:
        return retval
    }

    /**
     * Returns a glyph position index that corresponds with the given [utf16code] integer value.
     * If the glyph map does not contain the given unicode then one of the following is returned:
     * GLYPH_UNKNOWN, GLYPH_SPACE, GLYPH_CARRIAGE_RETURN, GLYPH_HORIZONTAL_TAB, GLYPH_NEW_LINE,
     * or GLYPH_VERTICAL_TAB
     */
    fun getGlyphIndex(utf16code: Int): Int {

        // do we have this utf16code within our map?
        if (glyphMap.containsKey(utf16code))
            return glyphMap[utf16code] ?: GLYPH_UNKNOWN

        // is the utf16code a space or a new line?
        when (utf16code) {
            9 -> return GLYPH_HORIZONTAL_TAB
            // The different kinds of spaces:
            32, 160, 8192 , 8193 , 8194 , 8195, 8196, 8197, 8198, 8199, 8200, 8201, 8202, 8239, 8287, 12288 -> return GLYPH_SPACE
            10, 12, 133, 8232 -> return GLYPH_NEW_LINE
            13 -> return GLYPH_CARRIAGE_RETURN
            // The 8233 should not be a vertical tab, but we map it as such:
            11, 8233 -> return GLYPH_VERTICAL_TAB
        }

        // if none of the above returned, return glyphMap[-1]
        // (note that the glyph map should have the '-1' entry for any unsupported characters):
        return glyphMap[-1] ?: GLYPH_UNKNOWN
    }

    /**
     * Returns texture coordinates based on the glyph [index].
     * Note that the index must be in a range from 0 to 374.
     */
    fun getGlyphTextureCoords( @IntRange(from=0, to=374) index: Int): RectF =
        RectF(((index % 25) * 41) * 9.765625e-4f,
            (1024 - (index / 25) * 65) * 9.765625e-4f,
            ((index % 25) * 41) * 9.765625e-4f + 3.90625e-2f,
            (1024 - (index / 25) * 65) * 9.765625e-4f - 6.25e-2f
        )


    // Any of these will not generate quads by default:

    /** A glyph that is not found on the map */
    const val GLYPH_UNKNOWN = -1

    /** Empty space character. */
    const val GLYPH_SPACE = -2

    /** Carriage return. */
    const val GLYPH_CARRIAGE_RETURN = -3

    /** Normal text tabulation, understood as four spaces. */
    const val GLYPH_HORIZONTAL_TAB = -4

    /** New line. */
    const val GLYPH_NEW_LINE = -5

    /** Vertical tabulation, or a change of paragraph. Understood as two new lines. */
    const val GLYPH_VERTICAL_TAB = -6

}
