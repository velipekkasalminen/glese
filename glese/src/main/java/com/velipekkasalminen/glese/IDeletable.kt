package com.velipekkasalminen.glese


/**
 * Simple interface for objects managing gpu/hardware resources
 */
interface IDeletable {

    val isInitialized: Boolean

    fun delete()

}
