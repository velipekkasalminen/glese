package com.velipekkasalminen.glese


/*

OpenGL ES functions mostly use floats, and the precision does not need to be at a high level
(shaders might use 'half floats' for example).

Here are some infix functions that check if a (float) value is 'near' another value.
For example, the Vector4 normalization uses these.

*/

// Here these 'near' functions are intended for small numbers,
// a better solution would be:  ((this >= f * 0.999999f) && (this <= f * 1.000001f))
// or something else proportional to the input value 'f'

/** Returns true if this Float value is 'near' (almost equal to) another Float value */
infix fun Float.near(f: Float): Boolean = ((this >= f-1e-5f) && (this <= f+1e-5f))

/** Returns true if this Float value is 'near' (almost equal to) an integer value */
infix fun Float.near(i: Int): Boolean = ((this >= i-1e-5f) && (this <= i+1e-5f))

/** Returns true if this Int value is 'near' the given Float value */
infix fun Int.near(f: Float): Boolean = ((this >= f-1e-5f) && (this <= f+1e-5f))



/**
 * Returns a vector from position a to position b:<br />
 * val vec = a to b<br />
 * To know the distance from a to b:<br />
 * val distance = vec.length<br />
 * or:<br />
 * val distance = Position.distance(a,b)
 */
infix fun Position.to(p: Position): Vector4 = Vector4(p - this)
