package com.velipekkasalminen.glese


/**
 * Base class for classes managing objects that manage a gpu/hardware resource.
 * The BaseResMap accepts only subtypes of IDeletable.
 */
open class BaseResMap<T: IDeletable> {

    protected val _resMap = mutableMapOf<Int, T>()

    operator fun get(name: String): T {
        return _resMap[name.hashCode()] ?: throw IllegalArgumentException("Invalid use of indexed access: $name does not exist.")
    }

    operator fun set(name: String, v: T) {
        val h = name.hashCode()
        if (_resMap.containsKey(h)) throw IllegalArgumentException("Invalid use of indexed access: $name already exists and can not be changed.")
        _resMap[h] = v
    }

    /** Returns true, if the map contains the named object. */
    operator fun contains(name: String) = _resMap.contains(name.hashCode())

    /** The number of objects in the map. */
    val count get() = _resMap.count()

    /**
     * Calls delete on the named resource and removes it from the map.
     */
    open fun delete(name: String): Boolean {
        val h = name.hashCode()
        if(!_resMap.containsKey(h)) return false

        _resMap[h]?.delete()

        _resMap.remove(h)

        // TODO: could the above two lines be replaced with just:
        //       _resMap[h].remove(h)?.delete()

        return true
    }

    /**
     * Calls delete on every resource, then clears the map.
     */
    open fun deleteAll() {
        for (m in _resMap.values) {
            m.delete()
        }
        _resMap.clear()
    }

    /**
     * Calls toString on the named resource object.
     * @return An empty string if the [name] does not exist.
     */
    open fun invokeToString(name: String): String {
        val h = name.hashCode()
        return if (!_resMap.containsKey(h)) ""
        else _resMap[h].toString()
    }

}
