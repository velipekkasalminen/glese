package com.velipekkasalminen.glese


import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.os.SystemClock
import java.lang.Thread.sleep
import java.util.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10



// NOTE: this renderer is not the same as android.graphics.HardwareRenderer
// https://developer.android.com/reference/kotlin/android/graphics/HardwareRenderer

// The renderer runs on the rendering -thread (DUH!)
class GleseRenderer: GLSurfaceView.Renderer {

    // the buffer clear color is set initially to black
    private var _clearColor: ColorRGBA = ColorRGBA(0f,0f,0f,1f)

    private var _threadId: Long = Long.MAX_VALUE

    /** The Id of the rendering thread. */
    val threadId: Long get() = _threadId

    /**
     * The default color that the color buffer is set to at the beginning of each frame.
     *
     */
    var clearColor: ColorRGBA
        get() = _clearColor
        set(value) {
            _clearColor.r = value.r
            _clearColor.g = value.g
            _clearColor.b = value.b
            //m_clearColor.a = 1.0f
            //GLES20.glClearColor(_clearColor.r, _clearColor.g, _clearColor.b, _clearColor.a)
        }

    /**
     * The currently achieved frames per second, measured from the update cycle.
     * This value is averaged over multiple frames and is not available on the first few update cycles.
     * Note that the display uses a fixed frame rate, and the Engine.surfaceView.display.mode.refreshRate
     * is only available on API level 23 and above.
     */
    val currentFPS: Float get() = _currentFPS

    /**
     * The target frame rate per second for the update cycle, default is 60.
     * The target frame rate can not be set below 10 frames per second,
     * and should not exceed a value over 1000 fps.
     */
    var targetFPS: Float = 60f
        set(value) {
            field = if (value < 10f) 10f else value
            _targetFrameTime = 1f / field
        }

    /**
     * This will set the [targetFPS] to 'maximum' value, note that
     * the [targetFPS] may report an incorrect value after this method call.
     * Calling this method may have side-effects, such as the frameTime
     * parameter of the update method of [Object3D] being zero or close
     * to zero.
     */
    fun maximumFPS() {
        targetFPS = Float.MAX_VALUE
        _targetFrameTime = 0f
    }


    // private variables:
    private var _currentFrameTime: Float = 0f
    private var _currentFPS: Float = 0f
    private var _targetFrameTime: Float = 0.016666f // <--- default is 60 FPS, see: targetFPS
    private var _frameTimer: Float = 0f
    private var _frameTimeTen: Float = 0f
    private var _frameCounter = 0
    private var _startSceneStarted = false

    fun runSceneDeque() {
        var s = _sceneBouncerQueue.poll()
        while (s != null) {
            Engine.scenes.add(s)
            s = _sceneBouncerQueue.poll()
        }
        if (!_startSceneStarted) {
            // load the staring scene:
            if (GleseSurfaceView.config.startingSceneName != null) {
                if (!Engine.scenes.load(GleseSurfaceView.config.startingSceneName!!))
                    throw IllegalArgumentException("Attempted to start scene \"${GleseSurfaceView.config.startingSceneName}\" but it was not found on the Engine.scenes")
                else _startSceneStarted = true
            } // else ?? Warning: no starting scene name set ??
        }
    }

    // this function runs on the rendering thread:
    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {
        _threadId = Thread.currentThread().id
        GLES20.glClearColor(_clearColor.r, _clearColor.g, _clearColor.b, _clearColor.a)
        GLES20.glEnable(GLES20.GL_DEPTH_TEST)
        GLES20.glEnable(GLES20.GL_BLEND)
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
        GLES20.glBlendEquation(GLES20.GL_FUNC_ADD) // <- this is the default
        GLES20.glEnable(GLES20.GL_CULL_FACE)
        GLES20.glCullFace(GLES20.GL_BACK) // <- this is the default


        // TODO: set the thread name to something consistent:
        //       Thread.currentThread().name = "GleseRenderingThread"


        // Now that the rendering thread is running,
        // add all of the scenes that are waiting in the queue:
        //if (_sceneBouncerQueue.isNotEmpty()) runSceneDeque()

        // load the staring scene:
        //if (GleseSurfaceView.config.startingSceneName != null) {
        //    if (!Engine.scenes.load(GleseSurfaceView.config.startingSceneName!!))
        //        throw IllegalArgumentException("Attempted to start scene \"${GleseSurfaceView.config.startingSceneName}\" but it was not found on the Engine.scenes")
        //} // else ?? Warning: no starting scene name set ??
    }

    // this function runs on the rendering thread:
    override fun onDrawFrame(unused: GL10) {
        // call the callback if we have it:
        Engine.onDrawFrameCallback?.let { it() }

        // clear the color and depth buffers (begins a new frame):
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)

        _frameTimer = (SystemClock.elapsedRealtime().toFloat()) * 1e-3f

        // add all of the scenes that are waiting in the queue:
        if (_sceneBouncerQueue.isNotEmpty()) runSceneDeque()

        // render the active scene:
        Engine.scenes.activeScene?.onRender()
        Engine.scenes.activeScene?.render()

        // update the active scene:
        Engine.scenes.activeScene?.onUpdate(_currentFrameTime)
        Engine.scenes.activeScene?.update(_currentFrameTime)

        _currentFrameTime = (SystemClock.elapsedRealtime().toFloat()) * 1e-3f - _frameTimer

        // Hard Limit of minimum frame rate is 10 FPS,
        // this will 'slow down' the frameTime of the update cycle
        // if the system is under heavy stress.
        if (_currentFrameTime > 0.1f) _currentFrameTime = 0.1f
        else if (_currentFrameTime < _targetFrameTime) {
            // SLOW DOWN, YOU ARE GOING TOO FAST!
            sleep( (999f * (_targetFrameTime - _currentFrameTime)).toLong() )
            _currentFrameTime = _targetFrameTime
        }

        // calculate fps (using the time average over ten frames):
        _frameTimeTen += _currentFrameTime
        _frameCounter++
        if (_frameCounter % 10 == 0) {
            _currentFPS = 1f / (0.1f * _frameTimeTen)
            _frameTimeTen = 0f
            _frameCounter = 0
        }

    }

    // this function runs on the rendering thread:
    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {
        if (Engine.onSurfaceChangedCallback != null) Engine.onSurfaceChangedCallback?.let { it(width, height) }
        else GLES20.glViewport(0, 0, width, height)
    }


    companion object {

        private val _anyLock = Any()

        private val _sceneBouncerQueue: Queue<Scene> = ArrayDeque<Scene>()


        /**
         * Adds a Scene to the waiting queue to be later added to the [Engine.scenes] on the running rendering thread.
         * This is usually used outside the rendering thread by the add method of [Engine.scenes], even before
         * the renderer thread has started.
         */
        fun addToSceneBouncerQueue(scene: Scene) = synchronized(_anyLock) {
            _sceneBouncerQueue.add(scene)
        } // the add here returns a boolean, but it should be ignored (remnant of the Collection.add interface)

        // Note to self: The 'sceneBouncerQueue' mechanism (which is a bit stupid) is designed to be
        // used before the rendering thread starts, to add scenes that need their 'onCreate' to be called.
        // Please, replace the mechanism with something better (or at least improve the thread safety).

    }

}

