package com.velipekkasalminen.glese


import androidx.annotation.FloatRange


/**
 * Camera3D is designed to be used as a perspective camera by using the perspective projection
 * matrix to create the perspective effect; use the [setPerspectiveProjection] -method to achieve this.
 * Do not confuse this camera class with [android.graphics.Camera], which is usually used with the
 * [android.graphics.Canvas] class.
 */
class Camera3D(val name: String): ITransformable {

    /**
     * The local position of this camera.
     */
    override var position = Position()

    /**
     * The rotation (orientation) of this camera, represented by a quaternion.
     */
    override var rotation = Quaternion()

    override var scale = Vector4()

    // TODO: change lookAtTarget to Object3D ref
    var lookAtTarget = Position()

    /**
     * True if this camera is looking at the [lookAtTarget].
     */
    var isLookingAtTarget = false

    /**
     * True if this camera is using the global 'up' vector (the y-axis) for
     * setting the rotation (orientation) while tracking the
     * [lookAtTarget].
     */
    var isUsingGlobalUp = true

    /**
     * The local view transform matrix.
     */
    var viewMatrix = Matrix4x4()

    /**
     * The projection matrix.
     * Use the [setPerspectiveProjection] method to set the camera as a perspective camera.
     * To set the camera to an orthographic projection, you can use one of the
     * [setOrthographicProjection] -methods.
     */
    var projectionMatrix = Matrix4x4()

    private var _right = Vector4()
    private var _forward = Vector4()
    private var _up = Vector4()
    private var _isStationary: Boolean = false
    private var _xFow = 1.74533f
    private var _yFow = 1.74533f

    /**
     * The horizontal field-of-view of the perspective mode camera in radians.
     * Orthographic mode will report 1.7533 as the fov.
     */
    val horizontalFowPerspective: Float get() = _xFow

    /**
     * The vertical field-of-view of the perspective mode camera in radians.
     * Orthographic mode will report 1.7533 as the fov.
     */
    val verticalFowPerspective: Float get() = _yFow

    /**
     * The 'forward' direction vector of this camera,
     * obtained from the local view transform matrix.
     */
    override val forward: Vector4 get() = _forward

    /**
     * The 'up' direction vector of this camera,
     * obtained from the local view transform matrix.
     */
    override val up: Vector4 get() = _up

    /**
     * The 'right' direction vector of this camera,
     * obtained from the local view transform matrix.
     */
    override val right: Vector4 get() = _right

    /**
     * The local view transform matrix.
     * @see viewMatrix
     */
    override val matrix: Matrix4x4 get() = viewMatrix

    /**
     * Set to true to set this camera stationary.
     * Manipulating position or rotation will have no effect
     * as the automatic calls to the [calculateTransformations] -method are disabled.
     */
    override var isStationary: Boolean
        get() = _isStationary
        set(value) {
            // do the calculation one last time before going stationary:
            if (value) calculateTransformations()
            _isStationary = value
        }

    /**
     * True, if the position [p] is in (or nearly in) the field-of-view of the camera.
     * A perspective camera is assumed.
     */
    fun isInFov(p: Position): Boolean {
        // NOTE: posVec = p - position
        when {
            _xFow > _yFow -> if (0.55f * _xFow > Vector4.angle(forward, p - position)) return true
            else -> if (0.55f * _yFow > Vector4.angle(forward, p - position)) return true
        }
        return false
    }

    /**
     * Sets the projection matrix as a perspective projection matrix for this camera.
     * @param[fov] The vertical field-of-view in degrees. This is usually set to 90.
     * @param[aspect] The aspect ratio of the view, this determines the horizontal field-of-view. The value is usually screen or view width divided by height.
     * @param[near] The distance (from the camera) to the near plane. Usually a small value, such as 0.1
     * @param[far] The distance to the far plane. Must be greater than the distance to the near plane.
     */
    fun setPerspectiveProjection(
        @FloatRange(from = 0.0, to = 180.0, fromInclusive = false, toInclusive = false) fov: Float,
        aspect: Float, near: Float = 0.1f, far: Float = 1000f) {
        projectionMatrix.setProjection(fov, aspect, near, far)
        _yFow = fov * 1.745329252e-2f
        _xFow = _yFow * aspect
    }

    fun setOrthographicProjection() {
        projectionMatrix.setIdentity()
        _yFow = 1.74533f
        _xFow = 1.74533f
    }

    fun setOrthographicProjection(left: Float, right: Float,
                                  bottom: Float, top: Float,
                                  near: Float, far: Float) {
        projectionMatrix.setOrthographic(left, right, bottom, top, near, far)
        _yFow = 1.74533f
        _xFow = 1.74533f
    }


    override fun calculateTransformations() {
        // NOTE: isStationary is checked in the Scene.update method

        // reset:
        viewMatrix.setIdentity()

        // rotate:
        if (isLookingAtTarget) {
            // look at the target:
            if (isUsingGlobalUp) {
                viewMatrix.setLookAt(position, lookAtTarget)
                // NOTE: the _up should be (0f, 1f, 0f) here:
                _up.x = viewMatrix[0, 1]
                _up.y = viewMatrix[1, 1]
                _up.z = viewMatrix[2, 1]
            } else {
                viewMatrix.setLookAt(position, lookAtTarget, _up)
                // here _up is _up, no need to get it from the matrix.
            }

            // FIXME: test that the camera rotation from the view matrix works!
            rotation.set(viewMatrix)

        } else {
            // set rotation:
            viewMatrix.setRotation(rotation)
            // translate:
            viewMatrix.translate(position)

            _up.x = viewMatrix[0, 1]
            _up.y = viewMatrix[1, 1]
            _up.z = viewMatrix[2, 1]
        }

        _forward.x = viewMatrix[0, 2]
        _forward.y = viewMatrix[1, 2]
        _forward.z = viewMatrix[2, 2]
        _right.x = matrix[0, 0]
        _right.y = matrix[1, 0]
        _right.z = matrix[2, 0]

        // TODO: use scale (inverse of) to scale the view matrix of the camera

    }

}

