uniform mat4 mvpMatrix;
uniform vec3 cameraPosition;
attribute vec3 vertexPosition;
attribute vec3 vertexNormal;
varying vec3 v_normal;
varying vec3 v_viewVector;

void main() {
gl_Position = mvpMatrix * vec4(vertexPosition, 1.0);
v_normal = vertexNormal;
v_viewVector = cameraPosition - gl_Position.xyz;
}
