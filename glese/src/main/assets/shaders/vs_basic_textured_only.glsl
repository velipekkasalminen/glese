uniform mat4 mvpMatrix;
attribute vec3 vertexPosition;
attribute vec2 textureCoord;
varying vec2 v_textureCoord;

void main() {
gl_Position = mvpMatrix * vec4(vertexPosition, 1.0);
v_textureCoord = textureCoord;
}
