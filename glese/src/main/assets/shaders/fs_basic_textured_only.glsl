precision mediump float;
uniform sampler2D mainTexture;
varying vec2 v_textureCoord;

void main() {
gl_FragColor = texture2D(mainTexture, v_textureCoord);
}
