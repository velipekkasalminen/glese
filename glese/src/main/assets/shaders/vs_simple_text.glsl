uniform mat4 mvpMatrix;
uniform vec3 cameraPosition;
attribute vec3 vertexPosition;
attribute vec2 textureCoord;
attribute vec3 vertexNormal;
attribute vec4 vertexColor;
varying vec3 v_normal;
varying vec2 v_textureCoord;
varying vec4 v_vertexColor;
varying vec3 v_viewVector;

void main() {
gl_Position = mvpMatrix * vec4(vertexPosition, 1.0);
v_textureCoord.s = textureCoord.s;
v_textureCoord.t = 1.0 - textureCoord.t;
v_vertexColor = vertexColor;
v_normal = -vertexNormal;
v_viewVector = cameraPosition - gl_Position.xyz;
}
