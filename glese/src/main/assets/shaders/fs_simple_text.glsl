precision mediump float;
uniform sampler2D mainTexture;
varying vec3 v_normal;
varying vec2 v_textureCoord;
varying vec4 v_vertexColor;
varying vec3 v_viewVector;

void main() {
vec4 textureColor = texture2D(mainTexture, v_textureCoord);
float angleMult = 0.5 * (1.0 + dot(normalize(v_viewVector), v_normal));
gl_FragColor.rgb = max(angleMult, 0.4) * v_vertexColor.rgb * textureColor.rgb;
gl_FragColor.a = textureColor.r * textureColor.g * textureColor.b * textureColor.a;
}
