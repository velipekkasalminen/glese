precision mediump float;
uniform vec4 tintColor;
varying vec3 v_normal;
varying vec3 v_viewVector;

void main() {
gl_FragColor.rgb = 0.5 * (1.0 + dot(normalize(v_viewVector), v_normal)) * tintColor.rgb;
gl_FragColor.a = tintColor.a;
}
