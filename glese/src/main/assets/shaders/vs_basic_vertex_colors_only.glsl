uniform mat4 mvpMatrix;
attribute vec3 vertexPosition;
attribute vec4 vertexColor;
varying vec4 v_vertexColor;

void main() {
gl_Position = mvpMatrix * vec4(vertexPosition, 1.0);
v_vertexColor = vertexColor;
}
