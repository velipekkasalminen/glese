![Glese](images/GLESE.png)

<br>

<br>

## About

This project contains the sources for the `Glese` library.

The library is intended for 3D application development on the Android platfom.

Pre-built `aar` library package is not available at the moment.

<br>

## Copyright notice

Copyright © 2020 Veli-Pekka Salminen,

Licensed under the EUPL-1.2

See the [LICENSE](LICENSE) file for the full license text.

<br>

## Disclaimer

This library is in the prototype phase, and should not be used in release application development.

There is no warranty, expressed or implied.

You use this library at your own risk.

